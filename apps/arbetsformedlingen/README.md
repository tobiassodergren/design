# Arbetsförmedlingen

Digi är Arbetsförmedlingens och Skolverkets gemensamma designsystem. Här finns bland annat komponentbibliotek, design tokens, funktionalitet, api:er och annat som hjälper dig att bygga digitala tjänster som följer våra gemensamma riktlinjer. Repot är ett monorepo genererat med [Nx](https://nx.dev). Besök [Arbetsförmedlingens dokumentationssida](https://designsystem.arbetsformedlingen.se/) för att se vilka komponenter som ingår.

## Kom igång

### Förutsättningar

- [node (och npm)](https://nodejs.org/en/) i din utvecklingsmiljö. Aktuell nodeversion finns i `.nvmrc`

### Installera

- `nvm use` om du använder nvm. Annars får byta till rätt nodeversion på annat sätt.
- `npm i` i roten av projektet.

### Arbetsförmedlingen

Arbetsförmedlingen bygger på @digi/core som är den centrala delen i designsystemet. I @digi/core skapas alla komponenter som delas mellan de olika projekten, arbetsförmedlingen, skolverket etc.

#### Börja jobba i Arbetsförmedlingen dokumentationsappen

För att bygga dokumentationsappen.

- kör `npm run build arbetsformedlingen-docs`.

När du bygger projektet så läggs den i `dist/apps/arbetsformedlingen/docs`.

För att starta dokumentationen.

- kör `npm run start arbetsformedlingen-docs`.

#### Skapa en ny komponent

För att skapa din fösta komponent så behöver du följa beskrivningen nedan.
- Kör kommandot `npm run generate-component` i ett terminalfönster.
- Välj ett namn för din komponent t.ex. `button`.
- Välj sedan en kategori t.ex. `form, navigation etc`.
- Välj sedan en variation eller flera variation, separera med komma t.ex. `primary, secondary, small, medium`
- Ange `arbetsformedlingen` när den frågar vilket projekt du vill använda.
- En komponent ska nu ha skapats under `libs\arbetsformedlingen\package\src\components`.

#### Skapa ny sida i dokumentationen

- Kör kommandot `npm run generate-page:docs`
- Ange sedan namnet på den nya sidan likt `min-sida`.
- Ange `arbetsformedlingen` när den frågar vilket projekt du vill använda.
- En sida ska nu ha skapats under `apps\arbetsformedlingen\docs\src\pages`.
- Lägg till en route i switchen i `digi-docs.tsx`.

#### Skapa en dokumentationsida för en komponent

För att skapa en dokumentationssida för en komponent så behöver du följa beskrivningen nedan.

- Kör kommandot `npm run generate-component-page:docs`.
- Skriv namnet på komponenten du ska dokumentera, t.ex. `digi-button`.
- Ange `arbetsformedlingen` när den frågar vilket projekt du vill använda.
- En dokumentationssida för komponenten ska nu ha skapats under `apps\arbetsformedlingen\docs\src\details\core`.
- Lägg till en ny rad i `apps\arbetsformedlingen\docs\src\data\categoryData.ts` för att lägga till komponenten i menyn.

#### Skapa ny angular playground

- Kör kommandot `npx nx generate @nx/angular:app arbetsformedlingen/playground/angular-app` och ange sedan namnet på den nya sidan likt `angular-app`.
- kör kommandot `npm run build arbetsformedlingen-angular` för att importerar arbetsförmedlingens komponenter in till angular.
- kör kommandot `npm run start arbetsformedlingen-playground-angular-app` för att starta appen.
- kör kommandot `npx nx generate @nx/angular:component components/<page-name> --project arbetsformedlingen-playground-angular` för att skapa en sida.
- Lägg till sökvägen för sidan i `home.component.html` och `app.module.ts`.

#### Skapa ny react playground

- Kör kommandot `npx nx generate @nx/react:application arbetsformedlingen/playground/react-app` och ange sedan namnet på den nya sidan likt `react-app`.
- kör kommandot `npm run build arbetsformedlingen-react` för att importerar arbetsförmedlingens componenter.
- kör kommandot `npm run start arbetsformedlingen-playground-react-app` för att starta appen.

[Arbetsförmedlingen libs](../../libs/arbetsformedlingen/README.md)
