import { newSpecPage } from '@stencil/core/testing';
import { TokenCopy } from './digi-docs-token-copy';

describe('digi-docs-token-copy', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [TokenCopy],
			html: '<digi-docs-token-copy></digi-docs-token-copy>'
		});
		expect(root).toEqualHtml(`
      <digi-docs-token-copy>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-token-copy>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [TokenCopy],
			html: `<digi-docs-token-copy first="Stencil" last="'Don't call me a framework' JS"></digi-docs-token-copy>`
		});
		expect(root).toEqualHtml(`
      <digi-docs-token-copy first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-token-copy>
    `);
	});
});
