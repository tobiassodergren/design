import { Component, h, Host } from '@stencil/core';

@Component({
	tag: 'digi-docs-grid-list-item',
	styleUrl: 'digi-docs-grid-list-item.scss',
	scoped: true
})
export class GridListItem {

	render() {
		return (
			<Host class={{"digi-docs-grid-list-item": true}}>
				<div class="digi-docs-grid-list-item__image">
					<slot name="image"></slot>
				</div>
				<div class="digi-docs-grid-list-item__content">
					<slot name="content"></slot>
				</div>
				<div class="digi-docs-grid-list-item__footer">
					<slot name="footer"></slot>
				</div>
			</Host>
		);
	}
}
