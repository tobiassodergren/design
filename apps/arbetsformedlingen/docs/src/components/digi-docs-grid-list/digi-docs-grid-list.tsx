import { Component, h } from '@stencil/core';

@Component({
	tag: 'digi-docs-grid-list',
	styleUrl: 'digi-docs-grid-list.scss',
	scoped: true
})
export class GridList {

	render() {
		return (
			<host class="digi-docs-grid-list">
				<slot></slot>
			</host>
		);
	}
}
