import { newE2EPage } from '@stencil/core/testing';

describe('digi-docs-issue-collector', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent(
      '<digi-docs-issue-collector></digi-docs-issue-collector>'
    );

    const element = await page.find('digi-docs-issue-collector');
    expect(element).toHaveClass('hydrated');
  });

  it('contains a "Profile Page" button', async () => {
    const page = await newE2EPage();
    await page.setContent(
      '<digi-docs-issue-collector></digi-docs-issue-collector>'
    );

    const element = await page.find('digi-docs-issue-collector >>> button');
    expect(element.textContent).toEqual('Profile page');
  });
});
