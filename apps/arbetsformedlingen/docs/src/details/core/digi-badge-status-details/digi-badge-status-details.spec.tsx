import { newSpecPage } from '@stencil/core/testing';
import { DigiBadgeStatus } from './digi-badge-status-details';

describe('digi-badge-status-details', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [DigiBadgeStatus],
			html: '<digi-badge-status-details></digi-badge-status-details>'
		});
		expect(root).toEqualHtml(`
      <digi-badge-status-details>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-badge-status-details>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [DigiBadgeStatus],
			html: `<digi-badge-status-details first="Stencil" last="'Don't call me a framework' JS"></digi-badge-status-details>`
		});
		expect(root).toEqualHtml(`
      <digi-badge-status-details first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-badge-status-details>
    `);
	});
});
