import { Component, h, Prop, State } from '@stencil/core';
import {
	CodeExampleLanguage,
	BadgeStatusType,
	BadgeStatusVariation
} from '@digi/arbetsformedlingen';

@Component({
	tag: 'digi-badge-status-details',
	styleUrl: 'digi-badge-status-details.scss'
})
export class DigiBadgeStatus {
	@Prop() component: string;
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;

	@State() badgeStatusType: BadgeStatusType = BadgeStatusType.APPROVED;
	@State() afVariation: BadgeStatusVariation = BadgeStatusVariation.PRIMARY;

	get badgeStatusCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-badge-status
	af-type="${this.badgeStatusType}" 
	af-variation="${this.afVariation}" 
	af-text="${this.afText}"
>
</digi-badge-status>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-badge-status
	[attr.af-type]="BadgeStatusType.${this.badgeStatusType}" 
	[attr.af-variation]="BadgeStatusVariation.${this.afVariation}" 
	[attr.af-text]="'${this.afText}'"
>
</digi-badge-status>`,
			[CodeExampleLanguage.REACT]: `\
<DigiBadgeStatus
	afType={BadgeStatusType.${this.badgeStatusType}} 
	afVariation={BadgeStatusVariation.${this.afVariation}} 
	afText="${this.afText}"
>
</DigiBadgeStatus>`
		};
	}

	get afText() {
		let text = '';

		switch (this.badgeStatusType) {
			case BadgeStatusType.APPROVED:
				text = 'Godkänd';
				break;
			case BadgeStatusType.DENIED:
				text = 'Ej godkänd';
				break;
			case BadgeStatusType.MISSING:
				text = 'Saknas';
				break;
			case BadgeStatusType.PROMPT:
				text = 'Info';
				break;
			case BadgeStatusType.BETA:
				text = 'Beta';
				break;
			case BadgeStatusType.NEUTRAL:
				text = 'Neutral';
				break;
			default:
				text = 'Unknown Status';
				break;
		}

		return text;
	}
	render() {
		return (
			<div class="digi-badge-status-details">
				<div class="block-spacer">
					<digi-typography>
						{!this.afShowOnlyExample && (
							<digi-typography-preamble>
								Används för att informera användare om ärendestatus eller om en åtgärd
								som har utförts. Vanligtvis används de i tabelldata för att ange status.
							</digi-typography-preamble>
						)}
					</digi-typography>
				</div>
				<digi-layout-container af-no-gutter af-margin-bottom af-margin-top>
					{!this.afShowOnlyExample && <h2>Exempel</h2>}
					<digi-code-example
						af-code={JSON.stringify(this.badgeStatusCode)}
						af-hide-controls={this.afHideControls ? 'true' : 'false'}
						af-hide-code={this.afHideCode ? 'true' : 'false'}
					>
						<div class="slot__controls" slot="controls">
							<digi-form-fieldset
								afName="Statustyp"
								afLegend="Statustyp"
								onChange={(e) => (this.badgeStatusType = (e.target as any).value)}
							>
								<digi-form-radiobutton
									afName="Statustyp"
									afLabel="Approved"
									afValue={BadgeStatusType.APPROVED}
									afChecked={this.badgeStatusType === BadgeStatusType.APPROVED}
								/>
								<digi-form-radiobutton
									afName="Statustyp"
									afLabel="Denied"
									afValue={BadgeStatusType.DENIED}
									afChecked={this.badgeStatusType === BadgeStatusType.DENIED}
								/>
								<digi-form-radiobutton
									afName="Statustyp"
									afLabel="Missing"
									afValue={BadgeStatusType.MISSING}
									afChecked={this.badgeStatusType === BadgeStatusType.MISSING}
								/>
								<digi-form-radiobutton
									afName="Statustyp"
									afLabel="Prompt"
									afValue={BadgeStatusType.PROMPT}
									afChecked={this.badgeStatusType === BadgeStatusType.PROMPT}
								/>
								<digi-form-radiobutton
									afName="Statustyp"
									afLabel="Neutral"
									afValue={BadgeStatusType.NEUTRAL}
									afChecked={this.badgeStatusType === BadgeStatusType.NEUTRAL}
								/>
								<digi-form-radiobutton
									afName="Statustyp"
									afLabel="Beta"
									afValue={BadgeStatusType.BETA}
									afChecked={this.badgeStatusType === BadgeStatusType.BETA}
								/>
							</digi-form-fieldset>
							<digi-form-fieldset
								afName="Varianter"
								afLegend="Varianter"
								onChange={(e) => (this.afVariation = (e.target as any).value)}
							>
								<digi-form-radiobutton
									afName="Primär"
									afLabel="Primär"
									afValue={BadgeStatusVariation.PRIMARY}
									afChecked={this.afVariation === BadgeStatusVariation.PRIMARY}
								/>
								<digi-form-radiobutton
									afName="Sekundär"
									afLabel="Sekundär"
									afValue={BadgeStatusVariation.SECONDARY}
									afChecked={this.afVariation === BadgeStatusVariation.SECONDARY}
								/>
							</digi-form-fieldset>
						</div>
						<digi-badge-status
							afType={this.badgeStatusType}
							af-variation={this.afVariation}
							afText={this.afText}
						></digi-badge-status>
					</digi-code-example>
				</digi-layout-container>
				<digi-typography>
					{!this.afShowOnlyExample && (
						<digi-layout-container af-no-gutter af-margin-bottom>
							<h2>Beskrivning</h2>
							<h3>Varianter</h3>
							<p>
								Status indikeror finns som primär- och sekundärvariant, vilket skickas
								in via följande prop: <digi-code af-code="af-variation" />.
							</p>
							<p>
								Typen av badge ställs in med hjälp av <digi-code af-code="af-type" />,
								komponent har olika typer nekad, saknas, godkänd, prompt, neutral och
								beta.
							</p>
							<p>
								Texten anges hjälp av <digi-code af-code="af-text" />
							</p>
						</digi-layout-container>
					)}
				</digi-typography>
			</div>
		);
	}
}
