import { newSpecPage } from '@stencil/core/testing';
import { DigiCalendarDatepicker } from './digi-calendar-datepicker-details';

describe('digi-calendar-datepicker-details', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [DigiCalendarDatepicker],
			html:
				'<digi-calendar-datepicker-details></digi-calendar-datepicker-details>'
		});
		expect(root).toEqualHtml(`
      <digi-calendar-datepicker-details>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-calendar-datepicker-details>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [DigiCalendarDatepicker],
			html: `<digi-calendar-datepicker-details first="Stencil" last="'Don't call me a framework' JS"></digi-calendar-datepicker-details>`
		});
		expect(root).toEqualHtml(`
      <digi-calendar-datepicker-details first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-calendar-datepicker-details>
    `);
	});
});
