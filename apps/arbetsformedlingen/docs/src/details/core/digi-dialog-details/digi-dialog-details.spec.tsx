import { newSpecPage } from '@stencil/core/testing';
import { DigiDialog } from './digi-dialog-details';

describe('digi-dialog-details', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [DigiDialog],
			html: '<digi-dialog-details></digi-dialog-details>'
		});
		expect(root).toEqualHtml(`
      <digi-dialog-details>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-dialog-details>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [DigiDialog],
			html: `<digi-dialog-details first="Stencil" last="'Don't call me a framework' JS"></digi-dialog-details>`
		});
		expect(root).toEqualHtml(`
      <digi-dialog-details first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-dialog-details>
    `);
	});
});
