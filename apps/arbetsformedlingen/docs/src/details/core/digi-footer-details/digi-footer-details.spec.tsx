import { newSpecPage } from '@stencil/core/testing';
import { DigiFooter } from './digi-footer-details';

describe('digi-footer-details', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [DigiFooter],
			html: '<digi-footer-details></digi-footer-details>'
		});
		expect(root).toEqualHtml(`
      <digi-footer-details>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-footer-details>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [DigiFooter],
			html: `<digi-footer-details first="Stencil" last="'Don't call me a framework' JS"></digi-footer-details>`
		});
		expect(root).toEqualHtml(`
      <digi-footer-details first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-footer-details>
    `);
	});
});
