import { Component, Prop, h } from '@stencil/core';
import { CodeExampleLanguage } from '@digi/arbetsformedlingen';

@Component({
	tag: 'digi-form-filter-details',
	styleUrl: 'digi-form-filter-details.scss',
})
export class DigiFormFilterDetails {
	@Prop() component: string;
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	get filterCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-form-filter
	af-filter-button-text="Yrkesområde"
	af-submit-button-text="Filtrera">
	<digi-form-checkbox af-label="Val 1"/>
	<digi-form-checkbox af-label="Val 2"/>
	<digi-form-checkbox af-label="Val 3"/>
</digi-form-filter>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-form-filter
	[attr.af-filter-button-text]="'Yrkesområde'"
	[attr.af-submit-button-text]="'Filtrera'"> 
	<digi-form-checkbox afLabel="Val 1"/>
	<digi-form-checkbox afLabel="Val 2"/>
	<digi-form-checkbox afLabel="Val 3"/>
</digi-form-filter>`,
			[CodeExampleLanguage.REACT]: `\
<DigiFormFilter
	afFilterButtonText="Yrkesområde"
	afSubmitButtonText="Filtrera">
	<DigiFormCheckbox afLabel="Val 1"/>
	<DigiFormCheckbox afLabel="Val 2"/>
	<DigiFormCheckbox afLabel="Val 3"/>
</DigiFormFilter>`,
		};
	}
	render() {
		return (
			<div class="digi-form-filter-details">

				<digi-typography>
					{!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Multifilterkomponenten används för att filtrera resultat med hjälp av andra formulärkomponenter, till exempel radioknappar och kryssrutor.
            </digi-typography-preamble>
					)}
					<digi-layout-container af-no-gutter af-margin-bottom>
						{!this.afShowOnlyExample && (<h2>Exempel</h2>)}
						<digi-code-example af-code={JSON.stringify(this.filterCode)}
							af-hide-controls={this.afHideControls ? 'true' : 'false'} af-hide-code={this.afHideCode ? 'true' : 'false'}
						>
							<div class="slot__controls" slot="controls">
							</div>
							<div style={{ height: '250px' }}>
								<digi-form-filter
									afFilterButtonText="Yrkesområde"
									afSubmitButtonText="Filtrera"
								>
									<digi-form-checkbox
										afLabel="Val 1"
										af-label="Val 1"
									/>
									<digi-form-checkbox
										afLabel="Val 2"
										af-label="Val 2"
									/>
									<digi-form-checkbox
										afLabel="Val 3"
										af-label="Val 3"
									/>
								</digi-form-filter>
							</div>
						</digi-code-example>

					</digi-layout-container>

				</digi-typography>

			</div>
		);
	}
}
