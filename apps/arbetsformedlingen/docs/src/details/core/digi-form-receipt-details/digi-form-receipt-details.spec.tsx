import { newSpecPage } from '@stencil/core/testing';
import { DigiFormReceipt } from './digi-form-receipt-details';

describe('digi-form-receipt-details', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [DigiFormReceipt],
			html: '<digi-form-receipt-details></digi-form-receipt-details>'
		});
		expect(root).toEqualHtml(`
      <digi-form-receipt-details>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-form-receipt-details>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [DigiFormReceipt],
			html: `<digi-form-receipt-details first="Stencil" last="'Don't call me a framework' JS"></digi-form-receipt-details>`
		});
		expect(root).toEqualHtml(`
      <digi-form-receipt-details first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-form-receipt-details>
    `);
	});
});
