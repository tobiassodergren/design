import { newSpecPage } from '@stencil/core/testing';
import { DigiFormSelectFilter } from './digi-form-select-filter-details';

describe('digi-form-select-filter-details', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [DigiFormSelectFilter],
			html: '<digi-form-select-filter-details></digi-form-select-filter-details>'
		});
		expect(root).toEqualHtml(`
      <digi-form-select-filter-details>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-form-select-filter-details>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [DigiFormSelectFilter],
			html: `<digi-form-select-filter-details first="Stencil" last="'Don't call me a framework' JS"></digi-form-select-filter-details>`
		});
		expect(root).toEqualHtml(`
      <digi-form-select-filter-details first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-form-select-filter-details>
    `);
	});
});
