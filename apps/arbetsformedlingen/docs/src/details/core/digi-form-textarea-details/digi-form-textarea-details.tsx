import { Component, Prop, h, State } from '@stencil/core';
import { CodeExampleLanguage, FormTextareaValidation, FormTextareaVariation } from '@digi/arbetsformedlingen';

@Component({
	tag: 'digi-form-textarea-details',
	styleUrl: 'digi-form-textarea-details.scss',
})
export class DigiFormTextareaDetails {
	@Prop() component: string;
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() formTextareaVariation: FormTextareaVariation = FormTextareaVariation.MEDIUM;
	@State() formTextareaValidation: FormTextareaValidation = FormTextareaValidation.NEUTRAL;
	@State() hasDescription: boolean = false;

	get textareaCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-form-textarea
	af-label="Etikett"
	af-variation="${this.formTextareaVariation}"
	af-validation="${this.formTextareaValidation}"\
	${this.hasDescription ? '\n\taf-label-description="Beskrivande text"' : ''}\
	${this.formTextareaValidation !== FormTextareaValidation.NEUTRAL ? '\n\taf-validation-text="Det här är ett valideringsmeddelande"\n>' : '\n>'}
</digi-form-textarea>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-form-textarea
	[attr.af-label]="'Etikett'"
	[attr.af-variation]="FormTextareaVariation.${Object.keys(FormTextareaVariation).find((key) => FormTextareaVariation[key] === this.formTextareaVariation)}"
	[attr.af-validation]="FormTextareaValidation.${Object.keys(FormTextareaValidation).find((key) => FormTextareaValidation[key] === this.formTextareaValidation)}"\
	${this.hasDescription ? `\n\t[attr.af-label-description]="'Beskrivande text'"\t` : ''}\
	${this.formTextareaValidation !== FormTextareaValidation.NEUTRAL ? `\n\t[attr.af-validation-text]="'Det här är ett valideringsmeddelande'"\n>` : '\n>'}
</digi-form-textarea>`,
			[CodeExampleLanguage.REACT]: `\
<DigiFormTextarea
	afLabel="Etikett"
	afVariation={FormTextareaVariation.${Object.keys(FormTextareaVariation).find((key) => FormTextareaVariation[key] === this.formTextareaVariation)}}
	afValidation={FormTextareaValidation.${Object.keys(FormTextareaValidation).find((key) => FormTextareaValidation[key] === this.formTextareaValidation)}}\
	${this.hasDescription ? `\n\tafLabelDescription="Beskrivande text"\t ` : ''}\
	${this.formTextareaValidation !== FormTextareaValidation.NEUTRAL ? `\n\tafValidationText="Det här är ett valideringsmeddelande"\n>` : '\n>'}
</DigiFormTextarea>`
		};
	}
	render() {
		return (
			<div class="digi-form-textarea-details">

				<digi-typography>
					{!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Textarean används för att låta användare ange en mängd text som är längre än en rad.
            </digi-typography-preamble>
					)}
					<digi-layout-container af-no-gutter af-margin-bottom>
						{!this.afShowOnlyExample && (<h2>Exempel</h2>)}
						<digi-code-example af-code={JSON.stringify(this.textareaCode)}
							af-hide-controls={this.afHideControls ? 'true' : 'false'} af-hide-code={this.afHideCode ? 'true' : 'false'}>
							<div style={{ width: '400px' }}>
								{this.formTextareaValidation !== FormTextareaValidation.NEUTRAL && this.hasDescription && (<digi-form-textarea
									afLabel="Etikett"
									af-validation-text="Det här är ett valideringsmeddelande"
									af-variation={this.formTextareaVariation}
									af-validation={this.formTextareaValidation}
									af-label-description="Beskrivande text"
								></digi-form-textarea>)}
								{this.formTextareaValidation == FormTextareaValidation.NEUTRAL && this.hasDescription && (<digi-form-textarea
									afLabel="Etikett"
									af-variation={this.formTextareaVariation}
									af-validation={this.formTextareaValidation}
									af-label-description="Beskrivande text"
								></digi-form-textarea>)}
								{this.formTextareaValidation !== FormTextareaValidation.NEUTRAL && !this.hasDescription && (<digi-form-textarea
									afLabel="Etikett"
									af-validation-text="Det här är ett valideringsmeddelande"
									af-variation={this.formTextareaVariation}
									af-validation={this.formTextareaValidation}
								></digi-form-textarea>)}
								{this.formTextareaValidation == FormTextareaValidation.NEUTRAL && !this.hasDescription && (<digi-form-textarea
									afLabel="Etikett"
									af-variation={this.formTextareaVariation}
									af-validation={this.formTextareaValidation}
								></digi-form-textarea>)}
							</div>

							<div class="slot__controls" slot="controls">

								<digi-form-select
									afLabel="Storlek"
									onAfOnChange={(e) =>
										(this.formTextareaVariation = (e.target as any).value)
									}
									af-variation="small"
									af-value='medium'
								>
									<option value="small">Liten</option>
									<option value="medium">Mellan</option>
									<option value="large">Stor</option>
									<option value="auto">Auto</option>
								</digi-form-select>
								<digi-form-fieldset afLegend="Beskrivande text">
									<digi-form-checkbox
										afLabel="Ja"
										afChecked={this.hasDescription}
										onAfOnChange={() =>
											this.hasDescription
												? (this.hasDescription = false)
												: (this.hasDescription = true)
										}
									></digi-form-checkbox>
								</digi-form-fieldset>
								<digi-form-select
									afLabel="Validering"
									onAfOnChange={(e) =>
										(this.formTextareaValidation = (e.target as any).value)
									}
									af-variation="small"
								>
									<option value="neutral">Ingen</option>
									<option value="error">Felaktig</option>
									<option value="success">Godkänd</option>
									<option value="warning">Varning</option>
								</digi-form-select>
							</div>
						</digi-code-example>
					</digi-layout-container>

					{!this.afShowOnlyExample && (
						<digi-layout-container af-no-gutter af-margin-bottom>
							<h2>Beskrivning</h2>
							<p>
								Textarean innehåller en obligatorisk etikett med en valfri beskrivning och validering i olika varianter.

								Med attributet {' '} <digi-code af-code="af-label" />{' '}  sätts etiketten på textarean och med

								{' '} <digi-code af-code="af-label-description" />{' '}  går det att sätta en beskrivningstexten.
							</p>
						</digi-layout-container>
					)}
					{!this.afShowOnlyExample && (
						<digi-layout-container af-no-gutter af-margin-bottom>
							<h3>Storlekar</h3>
							<p>
								Textarean finns i storlekarna liten, mellan (förvald), stor och auto. Den sätts med attributet {' '} <digi-code af-code="af-variation" />{' '}.
							</p>
							<h3>Validering</h3>
							<p>
								Textarean finns i fyra olika valideringstyper: ingen (förvald), felaktig, godkänd, varning.

								{' '} Den ändras med attributet <digi-code af-code="af-validation" />{' '} och

								{' '}  <digi-code af-code="af-validation-text" /> {' '} ger textarean en valideringsmeddelandetext.
								Meddelandetexten visas enbart vid elementets första inmatning (onInput) eller oskärpa (onBlur).
							</p>
						</digi-layout-container>
					)}
				</digi-typography>

			</div>
		);
	}
}
