import { newSpecPage } from '@stencil/core/testing';
import { DigiHeaderAvatar } from './digi-header-avatar-details';

describe('digi-header-avatar-details', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [DigiHeaderAvatar],
			html: '<digi-header-avatar-details></digi-header-avatar-details>'
		});
		expect(root).toEqualHtml(`
      <digi-header-avatar-details>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-header-avatar-details>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [DigiHeaderAvatar],
			html: `<digi-header-avatar-details first="Stencil" last="'Don't call me a framework' JS"></digi-header-avatar-details>`
		});
		expect(root).toEqualHtml(`
      <digi-header-avatar-details first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-header-avatar-details>
    `);
	});
});
