import { newSpecPage } from '@stencil/core/testing';
import { DigiHeader } from './digi-header-details';

describe('digi-header-details', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [DigiHeader],
			html: '<digi-header-details></digi-header-details>'
		});
		expect(root).toEqualHtml(`
      <digi-header-details>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-header-details>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [DigiHeader],
			html: `<digi-header-details first="Stencil" last="'Don't call me a framework' JS"></digi-header-details>`
		});
		expect(root).toEqualHtml(`
      <digi-header-details first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-header-details>
    `);
	});
});
