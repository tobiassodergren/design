import { newSpecPage } from '@stencil/core/testing';
import { DigiHeaderNavigation } from './digi-header-navigation-details';

describe('digi-header-navigation-details', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [DigiHeaderNavigation],
			html: '<digi-header-navigation-details></digi-header-navigation-details>'
		});
		expect(root).toEqualHtml(`
      <digi-header-navigation-details>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-header-navigation-details>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [DigiHeaderNavigation],
			html: `<digi-header-navigation-details first="Stencil" last="'Don't call me a framework' JS"></digi-header-navigation-details>`
		});
		expect(root).toEqualHtml(`
      <digi-header-navigation-details first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-header-navigation-details>
    `);
	});
});
