import { newSpecPage } from '@stencil/core/testing';
import { DigiHeaderNavigationItem } from './digi-header-navigation-item-details';

describe('digi-header-navigation-item-details', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [DigiHeaderNavigationItem],
			html:
				'<digi-header-navigation-item-details></digi-header-navigation-item-details>'
		});
		expect(root).toEqualHtml(`
      <digi-header-navigation-item-details>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-header-navigation-item-details>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [DigiHeaderNavigationItem],
			html: `<digi-header-navigation-item-details first="Stencil" last="'Don't call me a framework' JS"></digi-header-navigation-item-details>`
		});
		expect(root).toEqualHtml(`
      <digi-header-navigation-item-details first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-header-navigation-item-details>
    `);
	});
});
