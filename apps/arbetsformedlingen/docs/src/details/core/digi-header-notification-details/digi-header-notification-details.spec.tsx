import { newSpecPage } from '@stencil/core/testing';
import { DigiHeaderNotification } from './digi-header-notification-details';

describe('digi-header-notification-details', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [DigiHeaderNotification],
			html: '<digi-header-notification-details></digi-header-notification-details>'
		});
		expect(root).toEqualHtml(`
      <digi-header-notification-details>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-header-notification-details>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [DigiHeaderNotification],
			html: `<digi-header-notification-details first="Stencil" last="'Don't call me a framework' JS"></digi-header-notification-details>`
		});
		expect(root).toEqualHtml(`
      <digi-header-notification-details first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-header-notification-details>
    `);
	});
});
