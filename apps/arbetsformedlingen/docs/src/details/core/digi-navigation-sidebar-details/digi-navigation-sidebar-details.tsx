import { Component, Prop, h } from '@stencil/core';
import { CodeExampleLanguage } from '@digi/arbetsformedlingen';

@Component({
	tag: 'digi-navigation-sidebar-details',
	styleUrl: 'digi-navigation-sidebar-details.scss'
})
export class DigiNavigationSidebarDetails {
	@Prop() component: string;
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;

	get navigationSidebar() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-navigation-sidebar
	af-active="true"
	af-sticky-header="true"
	af-backdrop="true"
	af-position="start"
	af-variation="over"
	af-close-button-text="Stäng"
>
	<digi-navigation-vertical-menu>
		...
	</digi-navigation-vertical-menu>
</digi-navigation-sidebar>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-navigation-sidebar
	[attr.af-active]="true"
	[attr.af-sticky-header]="true"
	[attr.af-backdrop]="true"
	[attr.af-position]="NavigationSidebarPosition.START"
	[attr.af-variation]="NavigationSidebarVariation.OVER"
	[attr.af-close-button-text]="Stäng"
>
	<digi-navigation-vertical-menu>
		...
	</digi-navigation-vertical-menu>
</digi-navigation-sidebar>`,
			[CodeExampleLanguage.REACT]: `\
<DigiNavigationSidebar
	afActive={true}
	afStickyHeader={true}
	afBackdrop={true}
	afPosition={NavigationSidebarPosition.START}
	afVariation={NavigationSidebarVariation.OVER}
	afCloseButtonText="Stäng"
>
	<DigiNavigationVerticalMenu>
		...
	</DigiNavigationVerticalMenu>
</DigiNavigationSidebar>`
		};
	}

	render() {
		return (
			<div class="digi-navigation-sidebar-details">

				<digi-typography>
					{!this.afShowOnlyExample && (
            <digi-typography-preamble>
              En layoutkomponent som är speciellt anpassad för <a href="/komponenter/digi-navigation-vertical-menu/oversikt"><digi-code afCode='<digi-navigation-vertical-menu>' /></a>.
            </digi-typography-preamble>
					)}
					<digi-layout-container af-no-gutter af-margin-bottom>
						{!this.afShowOnlyExample && (<h2>Exempel</h2>)}
						<digi-code-example
							af-code={JSON.stringify(this.navigationSidebar)}
							af-hide-controls={this.afHideControls ? 'true' : 'false'} af-hide-code={this.afHideCode ? 'true' : 'false'}
						>
							<digi-link-external
								af-variation="small"
								afTarget="_blank"
								afHref="https://digi-core.netlify.app/iframe.html?id=navigation-digi-navigation-sidebar--standard&viewMode=story"
							>
								Se ett exempel i ett nytt fönster
							</digi-link-external>
						</digi-code-example>
						<br />
						{!this.afShowOnlyExample && (
						<p>Läs mer om komponentens attribut under kodfliken.</p>
						)}
					</digi-layout-container>
				</digi-typography>

			</div>
		);
	}
}
