import { Component, Prop, h, Listen, State } from '@stencil/core';
import {
	CodeExampleLanguage,
	NavigationVerticalMenuVariation
} from '@digi/arbetsformedlingen';

@Component({
	tag: 'digi-navigation-vertical-menu-details',
	styleUrl: 'digi-navigation-vertical-menu-details.scss'
})
export class DigiNavigationVerticalMenuDetails {
	@Prop() component: string;
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() navigationVerticalMenuVariation: NavigationVerticalMenuVariation =
		NavigationVerticalMenuVariation.PRIMARY;

	get verticalMenuCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-navigation-vertical-menu 
	af-variation="${this.navigationVerticalMenuVariation}"
>
	<ul>
		<li>
			<digi-navigation-vertical-menu-item 
				af-text="Menyval 1" 
				af-active="true">
			</digi-navigation-vertical-menu-item>
		</li>
		<li>
			<digi-navigation-vertical-menu-item 
				af-text="Menyval 2" 
				af-active-subnav="true">
			</digi-navigation-vertical-menu-item>
			<ul>
				<li>
					<digi-navigation-vertical-menu-item 
						af-text="Undermenyval första nivå 1" 
						af-active-subnav="false">
					</digi-navigation-vertical-menu-item>
					<ul>
						<li>
							<digi-navigation-vertical-menu-item 
								af-href="/"
								af-text="Undermenyval andra nivå 1">
							</digi-navigation-vertical-menu-item>
						</li>
					</ul>
				</li>
			</ul>
		</li>
	</ul>
</digi-navigation-vertical-menu>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-navigation-vertical-menu
	[attr.af-variation]="NavigationVerticalMenuVariation.${Object.keys(NavigationVerticalMenuVariation).find((key) =>NavigationVerticalMenuVariation[key] ===this.navigationVerticalMenuVariation)}"
>
	<ul>
		<li>
			<digi-navigation-vertical-menu-item 
				[attr.af-text]="'Menyval 1'" 
				[attr.af-active]="true">
			</digi-navigation-vertical-menu-item>
		</li>
		<li>
			<digi-navigation-vertical-menu-item 
				[attr.af-text]="'Menyval 2'" 
				[attr.af-active-subnav]="true">
			</digi-navigation-vertical-menu-item>
			<ul>
				<li>
					<digi-navigation-vertical-menu-item 
						[attr.af-text]="'Undermenyval första nivå 1'" 
						[attr.af-active-subnav]="false">
					</digi-navigation-vertical-menu-item>
					<ul>
						<li>
							<digi-navigation-vertical-menu-item 
								[attr.af-href="'/'"]
								[attr.af-text]="'Undermenyval andra nivå 1'">
							</digi-navigation-vertical-menu-item>
						</li>
					</ul>
				</li>
			</ul>
		</li>
	</ul>
</digi-navigation-vertical-menu>`,
			[CodeExampleLanguage.REACT]: `\
<DigiNavigationVerticalMenu
	afVariation={NavigationVerticalMenuVariation.${Object.keys(NavigationVerticalMenuVariation).find((key) =>NavigationVerticalMenuVariation[key] ===this.navigationVerticalMenuVariation)}}
>
	<ul>
		<li>
			<DigiNavigationVerticalMenuItem 
				afText="Menyval 1" 
				afActive={true}>
			</DigiNavigationVerticalMenuItem>
		</li>
		<li>
			<DigiNavigationVerticalMenuItem
				afText="Menyval 2" 
				afActiveSubnav={true}>
			</DigiNavigationVerticalMenuItem>
			<ul>
				<li>
					<DigiNavigationVerticalMenuItem 
						afText="Undermenyval första nivå 1" 
						afActiveSubnav={false}>
					</DigiNavigationVerticalMenuItem>
					<ul>
						<li>
							<DigiNavigationVerticalMenuItem
								afHref="/"
								afText="Undermenyval andra nivå 1">
							</DigiNavigationVerticalMenuItem>
						</li>
					</ul>
				</li>
			</ul>
		</li>
	</ul>
</DigiNavigationVerticalMenu>`
		};
	}
	setActiveMenuItem(target) {
		const parent = target.closest('digi-navigation-vertical-menu');
		const menuItems = parent.querySelectorAll(
			'digi-navigation-vertical-menu-item[af-active="true"]'
		);
		if (menuItems) {
			menuItems.forEach((el) => el.setAttribute('af-active', 'false'));
		}
		if (target) {
			target.setAttribute('af-active', 'true');
		}
	}

	@Listen('afOnClick')
	clickHandler(e) {
		if (
			e.target.matches(
				'.digi-navigation-vertical-menu-details digi-navigation-vertical-menu-item:not([af-active-subnav])'
			)
		) {
			e.detail.preventDefault();
			this.setActiveMenuItem(e.target);
		}
	}

	render() {
		return (
			<div class="digi-navigation-vertical-menu-details">

				<digi-typography>
					{!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Den här komponenten används som bas tillsammans med menyvalskomponenten{' '}
              <a href="/komponenter/digi-navigation-vertical-menu-item/oversikt">
                <digi-code af-code="<digi-navigation-vertical-menu-item>"></digi-code>
              </a>{' '}
              för att skapa vertikala menyer, t.ex. rullgardinsmenyer. Det går att
              bädda in flera menyvalskomponenter i varandra för att skapa undermenyer.
            </digi-typography-preamble>
					)}
					<digi-layout-container af-no-gutter af-margin-bottom>
						<article>
							{!this.afShowOnlyExample && (<h2>Exempel</h2>)}
							<digi-code-example af-code={JSON.stringify(this.verticalMenuCode)}
								af-hide-controls={this.afHideControls ? 'true' : 'false'} af-hide-code={this.afHideCode ? 'true' : 'false'}>
								<digi-navigation-vertical-menu
									af-variation={this.navigationVerticalMenuVariation}
								>
									<ul>
										<li>
											<digi-navigation-vertical-menu-item
												af-text="Menyval 1"
												af-active="true"
											></digi-navigation-vertical-menu-item>
										</li>
										<li>
											<digi-navigation-vertical-menu-item
												af-text="Menyval 2"
												af-active-subnav="true"
											></digi-navigation-vertical-menu-item>
											<ul>
												<li>
													<digi-navigation-vertical-menu-item
														af-text="Undermenyval första nivå"
														af-active-subnav="false"
													></digi-navigation-vertical-menu-item>
													<ul>
														<li>
															<digi-navigation-vertical-menu-item af-href="/" af-text="Undermenyval andra nivå"></digi-navigation-vertical-menu-item>
														</li>
													</ul>
												</li>
											</ul>
										</li>
									</ul>
								</digi-navigation-vertical-menu>

								<div class="slot__controls" slot="controls">
									<digi-form-fieldset
										afName="Variation"
										afLegend="Variation"
										onChange={(e) =>
											(this.navigationVerticalMenuVariation = (e.target as any).value)
										}
									>
										<digi-form-radiobutton
											afName="Variation"
											afLabel="Primär"
											afValue={NavigationVerticalMenuVariation.PRIMARY}
											afChecked={
												this.navigationVerticalMenuVariation ===
												NavigationVerticalMenuVariation.PRIMARY
											}
										/>
										<digi-form-radiobutton
											afName="Variation"
											afLabel="Sekundär"
											afValue={NavigationVerticalMenuVariation.SECONDARY}
											afChecked={
												this.navigationVerticalMenuVariation ===
												NavigationVerticalMenuVariation.SECONDARY
											}
										/>
									</digi-form-fieldset>
								</div>
							</digi-code-example>
						</article>
					</digi-layout-container>
				</digi-typography>

			</div>
		);
	}
}
