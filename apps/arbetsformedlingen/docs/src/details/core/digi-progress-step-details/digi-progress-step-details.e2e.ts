import { newE2EPage } from '@stencil/core/testing';

describe('digi-progress-step', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent(
      '<digi-progress-step></digi-progress-step>'
    );

    const element = await page.find('digi-progress-step');
    expect(element).toHaveClass('hydrated');
  });

  it('displays the specified name', async () => {
    const page = await newE2EPage({ url: '/profile/joseph' });

    const profileElement = await page.find(
      'app-root >>> digi-progress-step'
    );
    const element = profileElement.shadowRoot.querySelector('div');
    expect(element.textContent).toContain('Hello! My name is Joseph.');
  });

  // it('includes a div with the class "digi-progress-step"', async () => {
  //   const page = await newE2EPage({ url: '/profile/joseph' });

  // I would like to use a selector like this above, but it does not seem to work
  //   const element = await page.find('app-root >>> digi-progress-step >>> div');
  //   expect(element).toHaveClass('digi-progress-step');
  // });
});
