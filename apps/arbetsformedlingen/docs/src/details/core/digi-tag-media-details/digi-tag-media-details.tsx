import { Component, h, Host, Prop } from '@stencil/core';

import { TagMediaIcon, CodeExampleLanguage } from '@digi/arbetsformedlingen';


@Component({
	tag: 'digi-tag-media-details',
	styleUrl: 'digi-tag-media-details.scss'
})
export class DigiTagMedia {
	@Prop() component: string;
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@Prop() tagMediaIcon: TagMediaIcon = TagMediaIcon.NEWS;

	get tagMediaCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-tag-media    
	af-text="Mediatagg"
	af-icon="${Object.keys(TagMediaIcon).find(key => TagMediaIcon[key] === this.tagMediaIcon).toLocaleLowerCase()}"
>
</digi-tag-media>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-tag-media 
	[attr.af-text]="Jag är en tagg"
	[attr.af-icon]="TagMediaIcon.${Object.keys(TagMediaIcon).find(key => TagMediaIcon[key] === this.tagMediaIcon)}"
>
</digi-tag-media>`,
			[CodeExampleLanguage.REACT]: `\
<DigiTagMedia
	afText="Jag är en tagg"
	afIcon={TagMediaIcon.${Object.keys(TagMediaIcon).find(key => TagMediaIcon[key] === this.tagMediaIcon)}}
>
</DigiTagMedia>`
		};
	}

	render() {
		return (
			<Host>
				<div class="digi-tag-media-details">
					<digi-typography>
						{!this.afShowOnlyExample && (
							<digi-typography-preamble>
								Mediataggar används för att visuellt representera nya eller kategoriserade innehållselement. Mediataggens text sätts via
								<digi-code af-code="af-text='Taggens text'"></digi-code>
							</digi-typography-preamble>
						)}
						<digi-layout-container af-no-gutter af-margin-bottom>
							{!this.afShowOnlyExample && (<h2>Exempel</h2>)}
							<digi-code-example
								af-code={JSON.stringify(this.tagMediaCode)}
								af-hide-controls={this.afHideControls ? 'true' : 'false'} af-hide-code={this.afHideCode ? 'true' : 'false'}
							>
								<div class="slot__controls" slot="controls">
									<digi-form-select
										afLabel="Ikoner"
										onAfOnChange={(e) =>
											(this.tagMediaIcon = (e.target as any).value)
										}
										af-variation="small"
										af-start-selected="0"
									>
										<option value="news">Nyheter</option>
										<option value="playlist">Spellista</option>
										<option value="podcast">Podcast</option>
										<option value="film">Film</option>
										<option value="webtv">Webb-Tv</option>
										<option value="webinar">Webbinar</option>
									</digi-form-select>
								</div>
								<digi-tag-media
									afText="Mediatagg"
									afIcon={this.tagMediaIcon}
								>
								</digi-tag-media>
							</digi-code-example>
						</digi-layout-container>
						{!this.afShowOnlyExample && (
							<digi-layout-container afNoGutter afMarginBottom>
								<h2>Beskrivning</h2>
								<h3>Ikon</h3>
								<p>
									Mediataggen har sex valbara ikoner, dessa sätts med något utav valen nedan.
								</p>
								<digi-list af-list-type="numbered">
									<li>
										<digi-code af-code="af-icon='news'"></digi-code>
									</li>
									<li>
										<digi-code af-code="af-icon='playlist'"></digi-code>
									</li>
									<li>
										<digi-code af-code="af-icon='podcast'"></digi-code>
									</li>
									<li>
										<digi-code af-code="af-icon='film'"></digi-code>
									</li>
									<li>
										<digi-code af-code="af-icon='webtv'"></digi-code>
									</li>
									<li>
										<digi-code af-code="af-icon='webinar'"></digi-code>
									</li>
								</digi-list>
							</digi-layout-container>
						)}
					</digi-typography>
				</div>
			</Host>
		);
	}
}
