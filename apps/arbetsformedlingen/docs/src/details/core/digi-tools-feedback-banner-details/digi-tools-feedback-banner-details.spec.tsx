import { newSpecPage } from '@stencil/core/testing';
import { DigiToolsFeedbackBanner } from './digi-tools-feedback-banner-details';

describe('digi-tools-feedback-banner-details', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [DigiToolsFeedbackBanner],
			html:
				'<digi-tools-feedback-banner-details></digi-tools-feedback-banner-details>'
		});
		expect(root).toEqualHtml(`
      <digi-tools-feedback-banner-details>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-tools-feedback-banner-details>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [DigiToolsFeedbackBanner],
			html: `<digi-tools-feedback-banner-details first="Stencil" last="'Don't call me a framework' JS"></digi-tools-feedback-banner-details>`
		});
		expect(root).toEqualHtml(`
      <digi-tools-feedback-banner-details first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-tools-feedback-banner-details>
    `);
	});
});
