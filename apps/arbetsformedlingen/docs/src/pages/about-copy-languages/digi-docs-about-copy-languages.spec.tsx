import { newSpecPage } from '@stencil/core/testing';
import { AboutCopyLanguages } from './digi-docs-about-copy-languages';

describe('digi-docs-about-copy-languages', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [AboutCopyLanguages],
			html: '<digi-docs-about-copy-languages></digi-docs-about-copy-languages>'
		});
		expect(root).toEqualHtml(`
      <digi-docs-about-copy-languages>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-about-copy-languages>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [AboutCopyLanguages],
			html: `<digi-docs-about-copy-languages first="Stencil" last="'Don't call me a framework' JS"></digi-docs-about-copy-languages>`
		});
		expect(root).toEqualHtml(`
      <digi-docs-about-copy-languages first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-about-copy-languages>
    `);
	});
});
