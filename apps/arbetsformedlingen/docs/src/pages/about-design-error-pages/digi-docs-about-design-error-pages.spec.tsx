import { newSpecPage } from '@stencil/core/testing';
import { AboutDesignErrorPages } from './digi-docs-about-design-error-pages';

describe('digi-docs-about-design-error-pages', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [AboutDesignErrorPages],
			html:
				'<digi-docs-about-design-error-pages></digi-docs-about-design-error-pages>'
		});
		expect(root).toEqualHtml(`
      <digi-docs-about-design-error-pages>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-about-design-error-pages>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [AboutDesignErrorPages],
			html: `<digi-docs-about-design-error-pages first="Stencil" last="'Don't call me a framework' JS"></digi-docs-about-design-error-pages>`
		});
		expect(root).toEqualHtml(`
      <digi-docs-about-design-error-pages first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-about-design-error-pages>
    `);
	});
});
