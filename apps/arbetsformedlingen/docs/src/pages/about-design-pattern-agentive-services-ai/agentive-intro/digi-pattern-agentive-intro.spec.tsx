import { newSpecPage } from '@stencil/core/testing';
import { AgentiveIntro } from './digi-pattern-agentive-intro';

describe('digi-pattern-agentive-intro', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [AgentiveIntro],
			html: '<digi-pattern-agentive-intro></digi-pattern-agentive-intro>'
		});
		expect(root).toEqualHtml(`
      <digi-pattern-agentive-intro>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-pattern-agentive-intro>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [AgentiveIntro],
			html: `<digi-pattern-agentive-intro first="Stencil" last="'Don't call me a framework' JS"></digi-pattern-agentive-intro>`
		});
		expect(root).toEqualHtml(`
      <digi-pattern-agentive-intro first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-pattern-agentive-intro>
    `);
	});
});
