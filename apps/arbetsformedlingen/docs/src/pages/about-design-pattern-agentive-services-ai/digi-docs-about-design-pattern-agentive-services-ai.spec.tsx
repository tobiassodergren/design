import { newSpecPage } from '@stencil/core/testing';
import { AboutDesignPatternAgentive } from './digi-docs-about-design-pattern-agentive-services-ai';

describe('digi-docs-about-design-pattern-agentive-services-ai', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [AboutDesignPatternAgentive],
			html:
				'<digi-docs-about-design-pattern-forms></digi-docs-about-design-pattern-agentive-services-ai>'
		});
		expect(root).toEqualHtml(`
      <digi-docs-about-design-pattern-agentive-services-ai>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-about-design-pattern-agentive-services-ai>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [AboutDesignPatternAgentive],
			html: `<digi-docs-about-design-pattern-agentive-services-ai first="Stencil" last="'Don't call me a framework' JS"></digi-docs-about-design-pattern-agentive-services-ai>`
		});
		expect(root).toEqualHtml(`
      <digi-docs-about-design-pattern-agentive-services-ai first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-about-design-pattern-agentive-services-ai>
    `);
	});
});
