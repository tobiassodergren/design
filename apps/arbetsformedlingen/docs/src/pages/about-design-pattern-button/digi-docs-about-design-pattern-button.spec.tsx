import { newSpecPage } from '@stencil/core/testing';
import { AboutDesignPatternButton } from './digi-docs-about-design-pattern-button';

describe('digi-docs-about-design-pattern-button', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [AboutDesignPatternButton],
			html:
				'<digi-docs-about-design-pattern-button></digi-docs-about-design-pattern-button>'
		});
		expect(root).toEqualHtml(`
      <digi-docs-about-design-pattern-button>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-about-design-pattern-button>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [AboutDesignPatternButton],
			html: `<digi-docs-about-design-pattern-button first="Stencil" last="'Don't call me a framework' JS"></digi-docs-about-design-pattern-button>`
		});
		expect(root).toEqualHtml(`
      <digi-docs-about-design-pattern-button first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-about-design-pattern-button>
    `);
	});
});
