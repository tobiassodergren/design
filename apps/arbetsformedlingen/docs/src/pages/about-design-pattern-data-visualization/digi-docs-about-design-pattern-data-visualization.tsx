import { Component, h, Prop } from '@stencil/core';
import { router } from '../../global/router';
import Helmet from '@stencil/helmet';

@Component({
	tag: 'digi-docs-about-design-pattern-data-visualization',
	styleUrl: 'digi-docs-about-design-pattern-data-visualization.scss',
	scoped: true
})
export class AboutDesignPatternDataVisualization {
	Router = router;
	@Prop() pageName = 'Datavisualisering';
	linkClickHandler(e) {
		e.detail.preventDefault();
		this.Router.push(e.target.afHref);
	}

	render() {
		return (
			<host>
				<digi-docs-page-layout
					af-page-heading={this.pageName}
					af-edit-href="pages/about-design-pattern-data-visualization/digi-docs-about-design-pattern-data-visualization.tsx"
				>
					<Helmet>
						<meta
							property="rek:pubdate"
							content="Fri Jan 13 2023 15:26:55 GMT+0100 (centraleuropeisk normaltid)"
						/>
					</Helmet>
					<span slot="preamble">
						Datavisualisering är en grupp komponenter som ger användaren möjlighet att
						.....
					</span>
					<br />

					<digi-layout-block
						af-variation="secondary"
						afVerticalPadding
						af-no-gutter
						afMarginTop
					>
						<br />
						<digi-layout-columns af-variation="two">
							<div>
								<h2>Datavisualisering och andra komponenter </h2>
								<p>
									Datavisualisering, .... m.fl, är till för att våra användare ska kunna
									skicka in data till oss med minimal insats.
								</p>
								<br />
								<h2>Datavisualisering </h2>
								<p>.......</p>
								<br />
								<h2>....</h2>
								<p>.....</p>
								<br />
								<h2>Datavisualisering</h2>
								<p>Använd Datavisualisering .....</p>
								<br />
							</div>

							<div>
								<digi-layout-block af-variation="primary" afVerticalPadding>
									<div class="pattern"></div>
									<br />
									<div class="pattern"></div>
								</digi-layout-block>
								<br />
								<digi-layout-block
									af-variation="primary"
									afVerticalPadding
									afMarginBottom
								>
									<div class="pattern"></div>
								</digi-layout-block>

								<digi-layout-block af-variation="primary" afVerticalPadding>
									<div class="pattern"></div>
								</digi-layout-block>
								<br />
								<digi-layout-block af-variation="primary" afVerticalPadding>
									<div class="pattern">
										<br />
									</div>
								</digi-layout-block>
							</div>
						</digi-layout-columns>
						<br />
						<hr></hr>
						<br />
						<br />
						<digi-layout-columns af-variation="two">
							<digi-layout-block af-variation="primary" afVerticalPadding>
								<div class="pattern">
									<br />
									<br />
								</div>
							</digi-layout-block>

							<digi-layout-container>
								<h2>Layout</h2>
								<p></p>
							</digi-layout-container>
						</digi-layout-columns>
						<br />
						<br />
						<hr></hr>
						<br />
						<br />
						<digi-layout-columns af-variation="two">
							<div>
								<h4>Grid och brytpunkter</h4>
								<p>Se relaterade designmönster och komponenter</p>
								<digi-docs-related-links>
									<digi-link-button
										afHref="/designmonster/grid-och-brytpunkter"
										af-target="_blank"
										af-size="medium"
										af-variation="secondary"
										onAfOnClick={(e) => this.linkClickHandler(e)}
									>
										Grid och brytpunkter
									</digi-link-button>
								</digi-docs-related-links>
							</div>
							<div>
								<h4>Spacing</h4>
								<p>Se relaterade designmönster och komponenter</p>
								<digi-docs-related-links>
									<digi-link-button
										afHref="/designmonster/spacing"
										af-target="_blank"
										af-size="medium"
										af-variation="secondary"
										onAfOnClick={(e) => this.linkClickHandler(e)}
									>
										Spacing
									</digi-link-button>
								</digi-docs-related-links>
							</div>
						</digi-layout-columns>
					</digi-layout-block>
				</digi-docs-page-layout>
			</host>
		);
	}
}
