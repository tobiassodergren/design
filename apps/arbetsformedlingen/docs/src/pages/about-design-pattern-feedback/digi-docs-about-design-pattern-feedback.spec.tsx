import { newSpecPage } from '@stencil/core/testing';
import { AboutDesignPatternFeedback } from './digi-docs-about-design-pattern-feedback';

describe('digi-docs-about-design-pattern-feedback', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [AboutDesignPatternFeedback],
			html:
				'<digi-docs-about-design-pattern-feedback></digi-docs-about-design-pattern-feedback>'
		});
		expect(root).toEqualHtml(`
      <digi-docs-about-design-pattern-feedback>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-about-design-pattern-feedback>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [AboutDesignPatternFeedback],
			html: `<digi-docs-about-design-pattern-feedback first="Stencil" last="'Don't call me a framework' JS"></digi-docs-about-design-pattern-feedback>`
		});
		expect(root).toEqualHtml(`
      <digi-docs-about-design-pattern-feedback first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-about-design-pattern-feedback>
    `);
	});
});
