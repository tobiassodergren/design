import { newSpecPage } from '@stencil/core/testing';
import { AboutDesignPatternFormsValidation } from './digi-docs-about-design-pattern-forms-validation';

describe('digi-docs-about-design-pattern-forms-validation', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [AboutDesignPatternFormsValidation],
			html:
				'<digi-docs-about-design-pattern-forms-validation></digi-docs-about-design-pattern-forms-validation>'
		});
		expect(root).toEqualHtml(`
      <digi-docs-about-design-pattern-forms-validation>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-about-design-pattern-forms-validation>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [AboutDesignPatternFormsValidation],
			html: `<digi-docs-about-design-pattern-forms-validation first="Stencil" last="'Don't call me a framework' JS"></digi-docs-about-design-pattern-forms-validation>`
		});
		expect(root).toEqualHtml(`
      <digi-docs-about-design-pattern-forms-validation first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-about-design-pattern-forms-validation>
    `);
	});
});
