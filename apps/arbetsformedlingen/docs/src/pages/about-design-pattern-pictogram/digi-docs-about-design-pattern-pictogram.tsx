import { Component, getAssetPath, h, Prop } from '@stencil/core';
import { ListType } from 'libs/arbetsformedlingen/package/src';

@Component({
	tag: 'digi-docs-about-design-pattern-pictogram',
	styleUrl: 'digi-docs-about-design-pattern-pictogram.scss',
	scoped: true
})
export class AboutDesignPatternPictogram {
	@Prop() pageName = 'Piktogram till kartläggningsarbete för service designers';

	render() {
		return (
			<host>
				<digi-docs-page-layout
					af-page-heading={this.pageName}
					af-edit-href="pages/about-design-pattern-pictogram/digi-docs-about-design-pattern-pictogram.tsx"
				>
					<digi-layout-container>
						<digi-typography-preamble
							af-margin-bottom
						>
							I det interna arbetet skapas mycket av innehållet tillsammans med intressenter och deltagare. För att underlätta arbetet har vi utvecklat färglösa piktogram som kan användas för att kartlägga sambandet mellan kundernas aktiviteter och interna processer och arbetsuppgifter. Syftet är att hjälpa grupper att få en gemensam förståelse genom enkla visuella representationer.
						</digi-typography-preamble>
					</digi-layout-container>
					<digi-layout-block
						af-variation="secondary"
						afVerticalPadding
						af-no-gutter
						afMarginTop
					>
						<h2>Hur kan piktogrammen användas?</h2>
						<digi-list
							afListType={ListType.BULLET}
						>
							<li>Vid kartläggning av flöden och aktiviteter i varje steg.</li>
							<li>I en kundresa för att identifiera vilka problem som kan uppstå.</li>
							<li>För att tydliggöra när olika aktörer måste samarbeta för att uppnå ett gemensamt mål.</li>
						</digi-list>
						<p>Om du använder färger för att skilja mellan olika aktörer, se till att dokumentera det i ditt arbetsmaterial. Det underlättar för intressenter och andra som ska ta del av materialet.</p>
						<h2>Enhetlig stil</h2>
						<p>Piktogrammen är enkla och färglösa. Det går bra att skapa egna piktogram efter behov, men det är viktigt att följa samma stil. Skicka gärna in nya piktogram till <a href="mailto:designsystem@arbetsformedlingen.se">designsystem@arbetsformedlingen.se</a> så att de kan läggas till i arbetsförmedlingens whiteboard-mall.</p>
						<h2>Vad är ett piktogram?</h2>
						<p>Ett piktogram är en enkel grafisk symbol som används för att kommunicera något så tydligt som möjligt. Till exempel används skyltar på flygplatser ofta piktogram för att visa var man hittar vatten, bagage eller toalett. Syftet är att beskriva något med en bild utan att använda ord.</p>
					</digi-layout-block>
					<digi-layout-block
						af-variation="primary"
						afVerticalPadding
						af-no-gutter
						afMarginTop
					>
						<article>
							<h2>Du hittar fördefinierade piktogram i Digital whiteboard</h2>
							<p><b>Standardisera betydelser</b><br/>
							Se till att ditt team och dina intressenter förstår betydelsen av varje piktogram. Skapa en gemensam förståelse genom att använda standardiserade symboler och beskriva vad varje symbol representerar.</p>
							<digi-media-image
								class="digi-design-pattern--pictogram__collection"
								afUnlazy
								afSrc={getAssetPath('/assets/images/pattern/pictogram/piktogram-collection.svg')}
								afAlt="Tiotal olika piktogram."
							></digi-media-image>
							<digi-link-external
								afHref=''
							>Piktogram och startdokument i Digital whitebord</digi-link-external>
						</article>
					</digi-layout-block>
					<digi-layout-block
						af-variation="secondary"
						afVerticalPadding
						af-no-gutter
						afMarginTop
					>
						<h2>Digital whiteboard</h2>
						<p>Med Digital whiteboard kan du visualisera ditt arbete med digitala post-it lappar, figurer och bilder. Bjud in till dina tavlor och samarbeta i realtid.</p>
						<p>Läs mer om Arbetsförmedlingens nya samarbetsverktyg - Digital whiteboard</p>
						<digi-link-external
							afHref='https://start.arbetsformedlingen.se/stod-i-arbetet/min-arbetsplats-och-mina-verktyg/systemstod/digital-whiteboard'
						>Mer om Digital whiteboard</digi-link-external>
					</digi-layout-block>
				</digi-docs-page-layout>
			</host>
		);
	}
}
