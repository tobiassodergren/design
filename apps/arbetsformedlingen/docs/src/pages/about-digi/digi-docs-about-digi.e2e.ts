import { newE2EPage } from '@stencil/core/testing';

describe('digi-docs-about-digi', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-docs-about-digi></digi-docs-about-digi>');

    const element = await page.find('digi-docs-about-digi');
    expect(element).toHaveClass('hydrated');
  });

  it('contains a "Profile Page" button', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-docs-about-digi></digi-docs-about-digi>');

    const element = await page.find('digi-docs-about-digi >>> button');
    expect(element.textContent).toEqual('Profile page');
  });
});
