import { Component, h, State, getAssetPath } from '@stencil/core';
import Helmet from '@stencil/helmet';
import state from '../../store/store';

@Component({
	tag: 'digi-docs-about-digital-accessibility',
	styleUrl: 'digi-docs-about-digital-accessibility.scss',
	scoped: true
})
export class DigiDocsAboutDigitalAccessibility {
	@State() pageName = 'Om digital tillgänglighet';

	render() {
		return (
			<div class="digi-docs-about-digital-accessibility">
				<Helmet>
					<meta
						property="rek:pubdate"
						content="Thu Nov 29 2022 08:06:16 GMT+0100 (CET)"
					/>
				</Helmet>
				<digi-typography>
					<digi-docs-page-layout
						// af-page-heading={this.pageName}
						af-edit-href="pages/digi-docs-about-digital-accessibility/digi-docs-about-digital-accessibility.tsx"
					>
						<digi-about-digital-accessibility-intro></digi-about-digital-accessibility-intro>
						<br />

						<digi-layout-block af-margin-bottom>
							<digi-layout-columns af-variation={state.responsiveThreeColumns}>
								<div>
									<h2>Tillgängligt för vem?</h2>
									<p>
										Våra digitala tjänster ska kunna användas av både kunder och
										medarbetare, vare sig mobil eller dator används. De ska vara
										tillgängliga oavsett om användaren har en funktionsnedsättning, är ny
										i Sverige eller saknar teknikvana.
									</p>
								</div>
								<div>
									<h3>Enkelt för våra kunder</h3>
									<p>
										Vi strävar efter att våra kunder i första hand ska använda våra
										digitala kanaler. Kunderna ska kunna utföra sina ärenden digitalt utan
										besvär, vare sig det är att hitta information, använda en tjänst eller
										fylla i ett formulär. Det avlastar också våra kontor och telefonsupport så vi kan hjälpa kunderna där snabbare.
									</p>
								</div>
								<div>
									<h3>En inkluderande arbetsmiljö</h3>
									<p>
										Arbetsförmedlingen ska spegla Sveriges mångfald. God digital tillgänglighet hjälper våra medarbetare att arbeta smidigt
										och effektivt. Det skapar en inkluderande arbetsmiljö som lockar
										och inspirerar kompetenta medarbetare.
									</p>
								</div>
							</digi-layout-columns>
						</digi-layout-block>

						<digi-layout-block
							af-variation="secondary"
							af-no-gutter
							af-vertical-padding
						>
							<h2>Designsystemet stöttar tillgänglighetsarbetet</h2>
							<p>
								Designsystemet samlar återanvändbara verktyg och riktlinjer. De hjälper
								oss att följa lagkrav och riktlinjer för digital tillgänglighet.
								Dessutom får allt som utvecklas med hjälp av designsystemet ett
								enhetligt utseende. Det gör våra tjänster lättare att känna igen och
								enklare att använda.
							</p>
							<digi-docs-grid-list>
								<digi-docs-grid-list-item>
									<div slot="image">
										<digi-media-image
											afUnlazy
											afWidth="455"
											afSrc={getAssetPath(
												'/assets/images/about-digital-accessibility/komponenter.svg'
											)}
											afAlt=""
										></digi-media-image>
									</div>
									<div slot="content">
										<h3>Komponenter och Digi Core</h3>
										<p>
											Digi Core är vårt komponentbibliotek för kod. De hjälper oss att
											snabbt och effektivt bygga digitala produkter.
										</p>
									</div>
									<div slot="footer">
										<digi-docs-related-links>
											<digi-link-button
												afHref="/komponenter/om-komponenter"
												af-size="medium"
												af-variation="secondary"
											>
												Komponenter
											</digi-link-button>
											<digi-link-button
												afHref="/om-designsystemet/digi-core"
												af-size="medium"
												af-variation="secondary"
											>
												Digi Core
											</digi-link-button>
										</digi-docs-related-links>
									</div>
								</digi-docs-grid-list-item>
								<digi-docs-grid-list-item>
									<div slot="image">
										<digi-media-image
											afUnlazy
											afWidth="455"
											afSrc={getAssetPath(
												'/assets/images/about-digital-accessibility/designmonster.svg'
											)}
											afAlt=""
										></digi-media-image>
									</div>
									<div slot="content">
										<h3>Designmönster och Digi UI Kit</h3>
										<p>
											Digi UI Kit samlar designkomponenter för för skisser och prototyper.
											Designmönster beskriver hur vi löser olika designproblem.
										</p>
									</div>
									<div slot="footer">
										<digi-docs-related-links>
											<digi-link-button
												afHref="/designmonster/introduktion"
												af-size="medium"
												af-variation="secondary"
											>
												Designmönster
											</digi-link-button>
											<digi-link-button
												afHref="/om-designsystemet/digi-ui-kit"
												af-size="medium"
												af-variation="secondary"
											>
												Digi UI Kit
											</digi-link-button>
										</digi-docs-related-links>
									</div>
								</digi-docs-grid-list-item>
								<digi-docs-grid-list-item>
									<div slot="image">
										<digi-media-image
											afUnlazy
											afWidth="455"
											afSrc={getAssetPath(
												'/assets/images/about-digital-accessibility/matrispuff.svg'
											)}
											afAlt=""
										></digi-media-image>
									</div>
									<div slot="content">
										<h3>Tillgänglighetsmatrisen</h3>
										<p>
											Tillgänglighetsmatrisen ger exempel på användargrupper med olika
											förutsättningar och behov.
										</p>
									</div>
									<div slot="footer">
										<digi-docs-related-links>
											<digi-link-button
												afHref="/tillganglighet-och-design/om-tillganglighetsmatrisen"
												af-size="medium"
												af-variation="secondary"
											>
												Tillgänglighetsmatrisen
											</digi-link-button>
										</digi-docs-related-links>
									</div>
								</digi-docs-grid-list-item>
								<digi-docs-grid-list-item>
									<div slot="image">
										<digi-media-image
											afUnlazy
											afWidth="455"
											afSrc={getAssetPath(
												'/assets/images/about-digital-accessibility/listpuff.svg'
											)}
											afAlt=""
										></digi-media-image>
									</div>
									<div slot="content">
										<h3>Tillgänglighetslistan</h3>
										<p>
											Tillgänglighetslistan är ett stöd både under utvecklingsprocessen och
											för att ta fram tillgänglighetsredogörelsen.
										</p>
									</div>
									<div slot="footer">
										<digi-docs-related-links>
											<digi-link-button
												afHref="/tillganglighet-och-design/tillganglighet-checklista"
												af-size="medium"
												af-variation="secondary"
											>
												Tillgänglighetslistan
											</digi-link-button>
										</digi-docs-related-links>
									</div>
								</digi-docs-grid-list-item>
								<digi-docs-grid-list-item>
									<div slot="image">
										<digi-media-image
											afUnlazy
											afWidth="455"
											afSrc={getAssetPath(
												'/assets/images/about-digital-accessibility/redogorpuff.svg'
											)}
											afAlt=""
										></digi-media-image>
									</div>
									<div slot="content">
										<h3>Tillgänglighetsredogörelse </h3>
										<p>
											Avsnittet “Process för tillgänglighetsredogörelse” samlar
											instruktioner och mallar för att skapa en tillgänglighetsredogörelse.
										</p>
									</div>
									<div slot="footer">
										<digi-docs-related-links>
											<digi-link-button
												afHref="/tillganglighet-och-design/tillganglighetsredogorelse"
												af-size="medium"
												af-variation="secondary"
											>
												Tillgänglighetsredogörelse
											</digi-link-button>
										</digi-docs-related-links>
									</div>
								</digi-docs-grid-list-item>
								<digi-docs-grid-list-item>
									<div slot="image">
										<digi-media-image
											class="cta-block__img"
											afUnlazy
											afWidth="455"
											afSrc={getAssetPath(
												'/assets/images/about-digital-accessibility/lagkravpuff.svg'
											)}
											afAlt=""
										></digi-media-image>
									</div>
									<div slot="content">
										<h3>Lagkrav och riktlinjer</h3>
										<p>
											Lagkrav och riktlinjer beskriver hur tillgänglighet är lagstadgat och
											länkar vidare till fördjupad information.
										</p>
									</div>
									<div slot="footer">
										<digi-docs-related-links>
											<digi-link-button
												afHref="/tillganglighet-och-design/lagkrav-och-riktlinjer"
												af-size="medium"
												af-variation="secondary"
											>
												Lagkrav och riktlinjer
											</digi-link-button>
										</digi-docs-related-links>
									</div>
								</digi-docs-grid-list-item>
							</digi-docs-grid-list>
						</digi-layout-block>

						<digi-layout-block af-no-gutter af-vertical-padding>
							<digi-layout-columns af-variation={state.responsiveTwoColumns}>
								<div>
									<h2>En levande process</h2>
									<digi-quote-single
										af-variation="primary"
										af-quote-author-name="Elisabeth Aguilera"
										af-quote-author-title="Chief Accessibility Officer"
									>
										Att arbeta med Inkluderande design och utveckling är en kontinuerlig
										process. Det kräver ett konstant åtagande att skapa en organisation
										som prioriterar tillgänglighet och inkludering i alla sina insatser.
									</digi-quote-single>
									<p>
										Designsystemet samlar inte bara verktyg. Det förenar människor i
										arbetet med att skapa användarcentrerad design och tillgänglighet.
									</p>
									<digi-list af-list-type="bullet">
										<li>Verktyg tas fram i samarbete med olika team.</li>
										<li>
											I användartester deltar testpersoner med olika förutsättningar.
										</li>
										<li>
											Verktygen uppdateras enligt ny forskning, samt när lagar och
											riktlinjer ändras.
										</li>
										<li>
											Vi samarbetar med en del andra myndigheter. Genom att utveckla en del
											design gemensamt förenklar vi kundernas kontakt med olika
											myndigheter.
										</li>
									</digi-list>
								</div>
								<div>
									<h2>Vår vision inkluderar alla</h2>
									<digi-quote-single
										af-variation="primary"
										af-quote-author-name="Arbetsförmedlingens vision"
									>
										Vi gör sverige rikare genom att få människor och företag att växa.
									</digi-quote-single>
									<p>
										I Sverige är andelen personer med någon typ av funktionsnedsättning
										15-20%. Vårt arbete med digital tillgänglighet är viktigt. Det
										inkluderar olika människor och skapar förutsättning för inspiration
										och innovation. Det hjälper oss att nå vår vision.
									</p>
								</div>
							</digi-layout-columns>
						</digi-layout-block>
					</digi-docs-page-layout>
				</digi-typography>
			</div>
		);
	}
}
