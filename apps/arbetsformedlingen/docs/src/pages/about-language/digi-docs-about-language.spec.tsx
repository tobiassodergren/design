import { newSpecPage } from '@stencil/core/testing';
import { DigiDocsAboutLanguage } from './digi-docs-about-language';

describe('digi-docs-about-language', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsAboutLanguage],
      html: '<digi-docs-about-language></digi-docs-about-language>'
    });
    expect(root).toEqualHtml(`
      <digi-docs-about-language>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-about-language>
    `);
  });

  it('renders with values', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsAboutLanguage],
      html: `<digi-docs-about-language first="Stencil" last="'Don't call me a framework' JS"></digi-docs-about-language>`
    });
    expect(root).toEqualHtml(`
      <digi-docs-about-language first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-about-language>
    `);
  });
});
