import { newSpecPage } from '@stencil/core/testing';
import { DigiDocsAccessibilityAndDesign } from './digi-docs-accessibility-and-design';

describe('digi-docs-accessibility-and-design', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsAccessibilityAndDesign],
      html: '<digi-docs-accessibility-and-design></digi-docs-accessibility-and-design>'
    });
    expect(root).toEqualHtml(`
      <digi-docs-accessibility-and-design>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-accessibility-and-design>
    `);
  });

  it('renders with values', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsAccessibilityAndDesign],
      html: `<digi-docs-accessibility-and-design first="Stencil" last="'Don't call me a framework' JS"></digi-docs-accessibility-and-design>`
    });
    expect(root).toEqualHtml(`
      <digi-docs-accessibility-and-design first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-accessibility-and-design>
    `);
  });
});
