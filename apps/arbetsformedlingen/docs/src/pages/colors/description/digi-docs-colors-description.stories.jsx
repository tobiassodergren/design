import { h } from '@stencil/core';

export default {
  title: 'DigiDocsColorsDescription',
  args: {
    first: 'John',
    middle: 'S',
    last: 'Doe'
  }
};

export const Primary = (args) => {
  return (
    <digi-docs-colors-description {...args}></digi-docs-colors-description>
  );
};
