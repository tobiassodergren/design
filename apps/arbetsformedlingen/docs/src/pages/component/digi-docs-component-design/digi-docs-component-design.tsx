import { Component, h, State } from '@stencil/core';
import state from '../../../store/store';
import { router } from '../../../global/router';

@Component({
  tag: 'digi-docs-component-design',
  styleUrl: 'digi-docs-component-design.scss',
})
export class DigiDocsComponentDesign {
  Router = router;

  @State() componentName: string;

  componentWillLoad() {
    // this.setActiveComponent();
  }

  componentWillUpdate() {
    // this.setActiveComponent();
  }

  // setActiveComponent() {
  //   if (this.match && this.match.params.name) {
  //     state.activeComponent = state.components.find(
  //       (c) => c.tag === this.match.params.name
  //     );

  //     this.componentName =
  //       state.activeComponent.docsTags?.find(
  //         (tag) => tag.name === 'swedishName'
  //       )?.text || state.activeComponent.tag;
  //   }
  // }

  toggleNavTabHandler(e) {
    const tag = e.target.dataset.tag;
    this.Router.push(`${state.routerRoot}komponenter/${this.componentName}/${tag}`);
  }

  render() {
    return (
      <div class="digi-docs-component-design">
        <digi-docs-page-layout >
          <digi-layout-block>
            <digi-typography>
              <h1 class="digi-docs-component-design__heading">
                {this.componentName}
              </h1>
            </digi-typography>
          </digi-layout-block>
          <digi-layout-block>
            <digi-navigation-tabs
              af-aria-label="Categories"
              af-init-active-tab="2"
            >
              <digi-navigation-tab
                onAfOnToggle={(e) => this.toggleNavTabHandler(e)}
                data-tag="oversikt"
                afAriaLabel="Översikt"
              ></digi-navigation-tab>
              <digi-navigation-tab
                onAfOnToggle={(e) => this.toggleNavTabHandler(e)}
                data-tag="api"
                afAriaLabel="Användning"
              ></digi-navigation-tab>
              <digi-navigation-tab
                onAfOnToggle={(e) => this.toggleNavTabHandler(e)}
                data-tag="design"
                afAriaLabel="Design"
              ></digi-navigation-tab>
            </digi-navigation-tabs>
          </digi-layout-block>
          <digi-layout-block>
            <digi-typography>
              <h2>Designdokumentation</h2>
            </digi-typography>
          </digi-layout-block>
        </digi-docs-page-layout>
      </div>
    );
  }
}
