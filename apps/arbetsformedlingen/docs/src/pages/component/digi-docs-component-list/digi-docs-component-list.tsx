import { Component, h, State } from '@stencil/core';
import state from '../../../store/store';

@Component({
	tag: 'digi-docs-component-list',
	styleUrl: 'digi-docs-component-list.scss',
	scoped: true
})
export class ComponentList {
	@State() pageName = 'Alla komponenter';
	@State() searchFilter: string = '';

	searchFilterChangeHandler(event) {
		const inputValue = event.detail.target.value;
		this.searchFilter = inputValue;
	}

	render() {
		return (
			<host>
				<span slot="preamble">
					Här listar vi alla våra komponenter utom ikoner
				</span>
				<div class="digi-docs-component-list__filters">
					<div class="digi-docs-component-list__search-filter">
						<digi-form-input-search
							af-label={`Filtrera komponenter`}
							afLabel={`Filtrera komponenter`}
							af-hide-button="true"
							onAfOnInput={(e) => this.searchFilterChangeHandler(e)}
						></digi-form-input-search>
					</div>
				</div>
				<div class="digi-docs-component-list__list">
					{state.components
						.filter((component) => !component.tag.includes('digi-icon-'))
						.filter((component) => !component.tag.includes('digi-util-'))
						.filter((component) => !component.tag.includes('digi-code-'))
						.filter((component) => {
							if (!component.docsTags?.find((tag) => tag.name === 'swedishName')?.text.toLowerCase().includes('under utveckling')) {
								return component;
							}
						})
						.filter((component) => {
							if (this.searchFilter !== '') {
								if (
									component.tag
										.toLowerCase()
										.includes(this.searchFilter.toLowerCase()) ||
									component.docsTags
										?.find((tag) => tag.name === 'swedishName')
										?.text.toLowerCase()
										.includes(this.searchFilter.toLowerCase())
								) {
									return true;
								}
								return false;
							}
							return true;
						})
						.map((component) => {
							const TagName = component.tag + '-details';
							const swedishName: string = component.docsTags?.find(
								(tag) => tag.name === 'swedishName'
							)?.text;
							return (
								<div>
									<digi-link afHref={`/komponenter/${component.tag}/oversikt`}>
										<h2 class="digi-docs-component-list__item-heading">
											{swedishName} <span>({component.tag})</span>
										</h2>
									</digi-link>
									<TagName afShowOnlyExample afHideControls afHideCode></TagName>
								</div>
							);
						})}
				</div>
			</host>
		);
	}
}
