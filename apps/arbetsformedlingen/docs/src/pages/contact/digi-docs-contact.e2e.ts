import { newE2EPage } from '@stencil/core/testing';

describe('digi-docs-contact', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-docs-contact></digi-docs-contact>');

    const element = await page.find('digi-docs-contact');
    expect(element).toHaveClass('hydrated');
  });

  it('contains a "Profile Page" button', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-docs-contact></digi-docs-contact>');

    const element = await page.find('digi-docs-contact >>> button');
    expect(element.textContent).toEqual('Profile page');
  });
});
