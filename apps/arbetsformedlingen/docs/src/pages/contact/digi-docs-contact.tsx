import { Component, h, State } from '@stencil/core';


@Component({
  tag: 'digi-docs-contact',
  styleUrl: 'digi-docs-contact.scss'
})
export class DigiDocsContact {
  @State() pageName = 'Kontakta Designsystem';

  async issueCollectorClickHandler() {
    const response = await fetch(
      'https://jira.arbetsformedlingen.se/s/03100f4077ec6b19fb2d024a447484ad-T/hsxbra/813005/b6b48b2829824b869586ac216d119363/4.0.4/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector-embededjs/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector-embededjs.js?locale=en-US&collectorId=8eb66a34'
    );
    const data = await response.json();
    console.log('data is', data);
  }

  render() {
    return (
      <div class="digi-docs-contact">
        <digi-docs-page-layout
          af-page-heading={this.pageName}
          af-edit-href="pages/about-digi/digi-docs-contact/digi-docs-contact.tsx"
        >
          <span slot="preamble">
            Designsystemet är en växande verktygslåda. Det består av ett
            teknikoberoende, användbart och tillgängligt kodbibliotek som följer
            myndighetens varumärke. Alla är välkomna att bidra och tillsammans
            ser vi till att designsystemet hela tiden blir bättre. 
          </span>
          <digi-layout-container af-margin-bottom af-margin-top>
            <article>
              <p>
                Du bidrar med din expertkunskap kring din produkt eller tjänst.
                Vi stöttar upp med riktlinjer, komponenter och en väletablerad
                samarbetsmodell. Tillsammans tar vi fram nya komponenter som
                hela organisationen har nytta av.
              </p>

              <digi-link
                af-variation="small"
                afHref={`mailto:designsystem@arbetsformedlingen.se?body=Beskriv%20ditt%20ärende%20så%20återkommer%20vi%20så%20snart%20vi%20kan`}
                class="digi-docs__accessibility-link"
              >
                <digi-icon-envelope slot="icon"></digi-icon-envelope>
                Mejla designsystem@arbetsformedlingen.se
              </digi-link>
            </article>
          </digi-layout-container>
          {/* <digi-link-external
              af-variation="small"
              afHref="https://bitbucket.arbetsformedlingen.se/projects/DIGI/repos/digi-monorepo/browse"
            >
              Designsystemets monorepo
            </digi-link-external>
            <h2>Skapa ett Jiraärende</h2>
            <p>
              För förfrågningar med tydliga mål (som till exempel buggar,
              utökad funktionalitet och så vidare), som du av någon anledning
              inte kan bidra med själv enligt ovan, så går det fint att skapa
              ett ärende på vår Jirabräda för Förslag och feedback
            </p>
            <digi-docs-issue-collector></digi-docs-issue-collector>
            <h2>För generella förfrågningar eller synpunkter</h2>
            <p>
              För mer generella synpunkter går det bra att skicka ett mail
              till vår frågelåda
            </p>
            
            <h2>Övriga sätt</h2>
            <p>
              Det går också att kontakta oss på Slack (#digi), vi försöker
              hålla koll där så mycket det går.
            </p>
            <digi-link-external
              af-variation="small"
              afHref="https://arbetsformedlingen-it.slack.com/archives/C31VB7CL8"
            >
              Digi på Slack
            </digi-link-external> */}
        </digi-docs-page-layout>
      </div>
    );
  }
}
