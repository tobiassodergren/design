import { newSpecPage } from '@stencil/core/testing';
import { CopyLanguages } from './digi-docs-copy-languages';

describe('digi-docs-copy-languages', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [CopyLanguages],
			html: '<digi-docs-copy-languages></digi-docs-copy-languages>'
		});
		expect(root).toEqualHtml(`
      <digi-docs-copy-languages>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-copy-languages>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [CopyLanguages],
			html: `<digi-docs-copy-languages first="Stencil" last="'Don't call me a framework' JS"></digi-docs-copy-languages>`
		});
		expect(root).toEqualHtml(`
      <digi-docs-copy-languages first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-copy-languages>
    `);
	});
});
