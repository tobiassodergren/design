import { newSpecPage } from '@stencil/core/testing';
import { DesignPattern } from './digi-docs-design-pattern';

describe('digi-docs-design-pattern', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [DesignPattern],
			html: '<digi-docs-design-pattern></digi-docs-design-pattern>'
		});
		expect(root).toEqualHtml(`
      <digi-docs-design-pattern>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-design-pattern>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [DesignPattern],
			html: `<digi-docs-design-pattern first="Stencil" last="'Don't call me a framework' JS"></digi-docs-design-pattern>`
		});
		expect(root).toEqualHtml(`
      <digi-docs-design-pattern first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-design-pattern>
    `);
	});
});
