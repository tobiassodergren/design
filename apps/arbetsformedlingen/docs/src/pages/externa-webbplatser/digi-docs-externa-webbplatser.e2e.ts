import { newE2EPage } from '@stencil/core/testing';

describe('digi-docs-externa-webbplatser', () => {
	it('renders', async () => {
		const page = await newE2EPage();

		await page.setContent(
			'<digi-docs-externa-webbplatser></digi-docs-externa-webbplatser>'
		);
		const element = await page.find('digi-docs-externa-webbplatser');
		expect(element).toHaveClass('hydrated');
	});

	it('renders changes to the name data', async () => {
		const page = await newE2EPage();

		await page.setContent(
			'<digi-docs-externa-webbplatser></digi-docs-externa-webbplatser>'
		);
		const component = await page.find('digi-docs-externa-webbplatser');
		const element = await page.find('digi-docs-externa-webbplatser >>> div');
		expect(element.textContent).toEqual(`Hello, World! I'm `);

		component.setProperty('first', 'James');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James`);

		component.setProperty('last', 'Quincy');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James Quincy`);

		component.setProperty('middle', 'Earl');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James Earl Quincy`);
	});
});
