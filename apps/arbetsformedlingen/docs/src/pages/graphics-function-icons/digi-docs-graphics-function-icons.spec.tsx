import { newSpecPage } from '@stencil/core/testing';
import { DigiDocsGraphicsFunctionIcons } from './digi-docs-graphics-function-icons';

describe('digi-docs-graphics-function-icons', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsGraphicsFunctionIcons],
      html: '<digi-docs-graphics-function-icons></digi-docs-graphics-function-icons>'
    });
    expect(root).toEqualHtml(`
      <digi-docs-graphics-function-icons>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-graphics-function-icons>
    `);
  });

  it('renders with values', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsGraphicsFunctionIcons],
      html: `<digi-docs-graphics-function-icons first="Stencil" last="'Don't call me a framework' JS"></digi-docs-graphics-function-icons>`
    });
    expect(root).toEqualHtml(`
      <digi-docs-graphics-function-icons first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-graphics-function-icons>
    `);
  });
});
