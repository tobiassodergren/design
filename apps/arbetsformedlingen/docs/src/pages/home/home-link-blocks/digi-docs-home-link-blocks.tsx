import { Component, getAssetPath, h } from '@stencil/core';
import { router } from '../../../global/router';

interface LinkBlockItem {
	heading: string;
	href: string;
	text: string;
	imgPath: string;
	buttonlabel: string;
}

@Component({
	tag: 'digi-docs-home-link-blocks',
	styleUrl: 'digi-docs-home-link-blocks.scss',
	scoped: true
})
export class HomeLinkBlocks {
	Router = router;

	linkBlocks: LinkBlockItem[] = [
		{
			heading: 'Komponenter',
			href: '/komponenter/om-komponenter',
			text:
				'Komponenterna är fundamentet i designsystemet. De är byggstenarna som skapar visuella och funktionella kopplingar mellan produkter och tjänster. Vi visar komponenterna i olika kategorier beroende på funktionalitet och även hur de fungerar och relaterar till andra komponenter.',
			imgPath: '/assets/images/components.svg',
			buttonlabel: 'Mer om komponenter'
		},
		{
			heading: 'Tokens',
			href: '/design-tokens/bibliotek',
			text:
				'Design tokens representerar alla designbeslut och är basen till all design i designsystemet. Det kan vara allt från färger, typografi, kantlinjer, avstånd med mera. Dessa skapas centralt och uppdateras från ett och samma ställe för att kunna administrera designsystemet effektivt.',
			imgPath: '/assets/images/tokens.svg',
			buttonlabel: 'Mer om tokens'
		},
		{
			heading: 'Designmönster',
			href: '/designmonster/introduktion',
			text:
				'För att lösa vanliga designproblem gör vi kontinuerligt nya designmönster. I varje designmönster finns återanvändbara kombinationer av komponenter och beteenden. Så att det går snabbt att bygga nya produkter och ger våra användare en känsla av samhörighet och igenkänning.',
			imgPath: '/assets/images/designmonster.svg',
			buttonlabel: 'Mer om designmönster'
		}
		// {
		// 	heading: 'Knappar',
		// 	href: '/designmonster/knappar',
		// 	text:
		// 		'Knappar används för att utföra en handling eller exekvera en funktion. Knappar finns alltid tillsammans med en annan komponent. Det finns en primär och en sekundär knapp. Inaktiv knapp ska inte användas. Alla knappar ska alltid gå att interagera med. ',
		//   imgPath: '/assets/images/designsystem-hero-team.jpg'
		// }
	];

	linkClickHandler(e) {
		e.detail.preventDefault();
		this.Router.push(e.target.afHref);
	}

	render() {
		return (
			<host>
				<digi-layout-block class="digi-docs-home-link-blocks">
					<digi-typography>
						<h2
							id={'digi-docs-home-link-blocks__heading'}
							class="digi-docs-home-link-blocks__heading"
						>
							Designsystemets grundpelare
						</h2>
					</digi-typography>
					<div class="digi-docs-home-link-blocks__columns">
						{this.linkBlocks.map((item: LinkBlockItem) => {
							return (
								<div class="digi-docs-home-link-block">
									<digi-media-image
										class="digi-docs-home-link-blocks__media"
										afUnlazy
										afSrc={getAssetPath(item.imgPath)}
										afAlt=""
									></digi-media-image>
									<digi-typography>
										<h2 class="digi-docs-home-link-block__heading">{item.heading}</h2>
									</digi-typography>
									<p>{item.text}</p>
									<digi-link-internal
										afHref={item.href}
										onAfOnClick={(e) => this.linkClickHandler(e)}
										af-target="_blank"
										af-variation="small"
									>
										{item.buttonlabel}
									</digi-link-internal>
								</div>
							);
						})}
					</div>
				</digi-layout-block>
			</host>
		);
	}
}
