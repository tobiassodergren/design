import { newE2EPage } from '@stencil/core/testing';

describe('digi-docs-icons-description', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent(
      '<digi-docs-icons-description></digi-docs-icons-description>'
    );

    const element = await page.find('digi-docs-icons-description');
    expect(element).toHaveClass('hydrated');
  });

  it('displays the specified name', async () => {
    const page = await newE2EPage({ url: '/profile/joseph' });

    const profileElement = await page.find(
      'app-root >>> digi-docs-icons-description'
    );
    const element = profileElement.shadowRoot.querySelector('div');
    expect(element.textContent).toContain('Hello! My name is Joseph.');
  });

  // it('includes a div with the class "digi-docs-icons-description"', async () => {
  //   const page = await newE2EPage({ url: '/profile/joseph' });

  // I would like to use a selector like this above, but it does not seem to work
  //   const element = await page.find('app-root >>> digi-docs-icons-description >>> div');
  //   expect(element).toHaveClass('digi-docs-icons-description');
  // });
});
