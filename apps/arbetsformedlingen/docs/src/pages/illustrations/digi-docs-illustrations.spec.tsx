import { newSpecPage } from '@stencil/core/testing';
import { DigiDocsIllustrations } from './digi-docs-illustrations';

describe('digi-docs-illustrations', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsIllustrations],
      html: '<digi-docs-illustrations></digi-docs-illustrations>'
    });
    expect(root).toEqualHtml(`
      <digi-docs-illustrations>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-illustrations>
    `);
  });

  it('renders with values', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsIllustrations],
      html: `<digi-docs-illustrations first="Stencil" last="'Don't call me a framework' JS"></digi-docs-illustrations>`
    });
    expect(root).toEqualHtml(`
      <digi-docs-illustrations first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-illustrations>
    `);
  });
});
