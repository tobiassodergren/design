import { Component, getAssetPath, h, Host, Prop } from '@stencil/core';
import { GoodBadExampleType } from './good-bad-example-type.enum';


@Component({
	tag: 'digi-docs-good-bad-example',
	styleUrl: 'digi-docs-good-bad-example.scss',
	scoped: true,
})
export class GoodBadExample {

	@Prop() exampleType: GoodBadExampleType = GoodBadExampleType.GOOD;

	get exampleIcon() {
		return (
			this.exampleType === GoodBadExampleType.GOOD ?
				<img src={getAssetPath('/assets/images/graphics-example/example-icon-good.svg')} alt="Positivt exempel"/> :
				<img src={getAssetPath('/assets/images/graphics-example/example-icon-bad.svg')} alt="Negativt exempel"/>
		);
	}

	get cssModifiers() {
    return {
      'digi-docs-good-bad-example--good': this.exampleType === GoodBadExampleType.GOOD,
      'digi-docs-good-bad-example--bad': this.exampleType === GoodBadExampleType.BAD,
    };
  }

	render() {
		return (
			<Host class={{
				"digi-docs-good-bad-example": true,
				...this.cssModifiers
			}}>
				<div class="digi-docs-good-bad-example__content">
					<div class="digi-docs-good-bad-example__icon">
						{ this.exampleIcon }
					</div>
					<slot></slot>
				</div>
				<div class="digi-docs-good-bad-example__footer">
					<slot name="footer"></slot>
				</div>
			</Host>
		);
	}
}
