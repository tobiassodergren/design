import { newSpecPage } from '@stencil/core/testing';
import { DigiDocsResources } from '../digi-docs-resources';

describe('digi-docs-resources', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [DigiDocsResources],
      html: `<digi-docs-resources></digi-docs-resources>`,
    });
    expect(page.root).toEqualHtml(`
      <digi-docs-resources>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </digi-docs-resources>
    `);
  });
});
