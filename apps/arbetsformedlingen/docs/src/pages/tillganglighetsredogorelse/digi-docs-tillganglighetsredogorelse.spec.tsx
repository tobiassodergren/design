import { newSpecPage } from '@stencil/core/testing';
import { DigiDocsTillganglighetsredogorelse } from './digi-docs-tillganglighetsredogorelse';

describe('digi-docs-tillganglighetsredogorelse', () => {
  it('renders', async () => {
    const {root} = await newSpecPage({
      components: [DigiDocsTillganglighetsredogorelse],
      html: '<digi-docs-tillganglighetsredogorelse></digi-docs-tillganglighetsredogorelse>'
    });
    expect(root).toEqualHtml(`
      <digi-docs-tillganglighetsredogorelse>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-tillganglighetsredogorelse>
    `);
  });

  it('renders with values', async () => {
    const {root} = await newSpecPage({
      components: [DigiDocsTillganglighetsredogorelse],
      html: `<digi-docs-tillganglighetsredogorelse first="Stencil" last="'Don't call me a framework' JS"></digi-docs-tillganglighetsredogorelse>`
    });
    expect(root).toEqualHtml(`
      <digi-docs-tillganglighetsredogorelse first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-tillganglighetsredogorelse>
    `);
  });
});
