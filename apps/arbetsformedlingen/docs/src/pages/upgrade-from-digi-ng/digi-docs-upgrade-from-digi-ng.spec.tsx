import { newSpecPage } from '@stencil/core/testing';
import { DigiDocsUpgradeFromDigiNg } from './digi-docs-upgrade-from-digi-ng';

describe('digi-docs-upgrade-from-digi-ng', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsUpgradeFromDigiNg],
      html: '<digi-docs-upgrade-from-digi-ng></digi-docs-upgrade-from-digi-ng>'
    });
    expect(root).toEqualHtml(`
      <digi-docs-upgrade-from-digi-ng>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-upgrade-from-digi-ng>
    `);
  });

  it('renders with values', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsUpgradeFromDigiNg],
      html: `<digi-docs-upgrade-from-digi-ng first="Stencil" last="'Don't call me a framework' JS"></digi-docs-upgrade-from-digi-ng>`
    });
    expect(root).toEqualHtml(`
      <digi-docs-upgrade-from-digi-ng first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-upgrade-from-digi-ng>
    `);
  });
});
