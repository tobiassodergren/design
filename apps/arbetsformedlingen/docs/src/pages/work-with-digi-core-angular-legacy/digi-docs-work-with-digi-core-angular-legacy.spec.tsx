import { newSpecPage } from '@stencil/core/testing';
import { WorkWithDigiCoreAngularLegacy } from './digi-docs-work-with-digi-core-angular-legacy';

describe('digi-docs-work-with-digi-core-angular-legacy', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [WorkWithDigiCoreAngularLegacy],
			html:
				'<digi-docs-work-with-digi-core-angular-legacy></digi-docs-work-with-digi-core-angular-legacy>'
		});
		expect(root).toEqualHtml(`
      <digi-docs-work-with-digi-core-angular-legacy>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-work-with-digi-core-angular-legacy>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [WorkWithDigiCoreAngularLegacy],
			html: `<digi-docs-work-with-digi-core-angular-legacy first="Stencil" last="'Don't call me a framework' JS"></digi-docs-work-with-digi-core-angular-legacy>`
		});
		expect(root).toEqualHtml(`
      <digi-docs-work-with-digi-core-angular-legacy first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-work-with-digi-core-angular-legacy>
    `);
	});
});
