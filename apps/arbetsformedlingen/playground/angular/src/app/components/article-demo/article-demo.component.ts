import { Component } from '@angular/core';
import { ButtonSize, ButtonVariation } from 'arbetsformedlingen-dist';

interface LinkItem {
	text: string;
	href?: string;
	currentPage?: boolean;
}

@Component({
  selector: 'at-article-demo',
  templateUrl: './article-demo.component.html',
  styleUrls: ['./article-demo.component.scss']
})
export class ArticleDemoComponent {
  ButtonSize = ButtonSize;
  ButtonVariation = ButtonVariation;
	currentActivePage = 'Ärendetyp';

  public links: LinkItem[] = [
		{ text: 'Start', href: '/' },
		{ text: 'Ekonomi', href: '/ekonomi' }
	];

  addBreadcrumb() {
		this.links.push({ text: 'undersida', href: '/ekonomi/undersida' });
	}

	changeActiveBreadcrumb() {
		this.currentActivePage = 'Ny aktiv sida';
	}

}
