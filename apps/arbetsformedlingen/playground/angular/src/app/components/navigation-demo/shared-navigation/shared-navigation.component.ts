import { Component, HostListener, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UtilBreakpointObserverBreakpoints } from 'arbetsformedlingen-dist';


@Component({
  selector: 'at-shared-navigation',
  templateUrl: './shared-navigation.component.html',
  styleUrls: ['./shared-navigation.component.scss']
})
export class SharedNavigationComponent implements OnInit {

  @Input() navData: [];

  private _isMobile: boolean;

  localState = {
    activeNav: "",
    activeHeading: "När du blir arbetslös"
  }

  constructor(private router: Router) {}

  ngOnInit(): void {
  }

  public setActive(id) {
    return `${id}-link` == this.localState.activeNav;
  }

  showNavigation() {
    const el = document.getElementById('sideNav');
    el.setAttribute('af-active', 'true');
  }

  closeNavigation() {
    const el = document.getElementById('sideNav');
    el.setAttribute('af-active', 'false');
  }

  @HostListener('document:afOnChange', ['$event'])
  onAfOnChange(event) {
    if(event.target.tagName == "DIGI-UTIL-BREAKPOINT-OBSERVER") {
      this._isMobile = event.detail.value === UtilBreakpointObserverBreakpoints.SMALL;
    }
  }

  @HostListener('document:afOnClick', ['$event'])
  onAfOnClick(event) {
    const el = event.detail.target as HTMLElement;

    if(el.matches('.digi-navigation-vertical-menu-item__link')) {
      this.localState.activeNav = el.getAttribute('id');
      document.querySelector('h1').innerText = el.querySelector('.digi-navigation-vertical-menu-item__text').textContent;
      // this.router.navigateByUrl(el.getAttribute('href'));
    }
  }

  @HostListener('document:afOnClose', ['$event'])
  onAfOnClose(event) {
    if(event.target.id === 'sideNav') {
      this.closeNavigation()
    }
  }

  @HostListener('document:afOnEsc', ['$event'])
  onAfOnEsc(event) {
    if(event.target.id === 'sideNav') {
      this.closeNavigation()
    }
  }

}
