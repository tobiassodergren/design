import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormReceiptComponent } from './receipt.component';

describe('ReceiptComponent', () => {
	let component: FormReceiptComponent;
	let fixture: ComponentFixture<FormReceiptComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [FormReceiptComponent]
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(FormReceiptComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
