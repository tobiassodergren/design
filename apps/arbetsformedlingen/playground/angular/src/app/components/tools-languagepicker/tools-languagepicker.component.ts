import { Component, OnInit } from '@angular/core';
import { INavigationContextMenuItem } from 'dist/libs/arbetsformedlingen/dist/types';

@Component({
	selector: 'at-tools-languagepicker',
	templateUrl: './tools-languagepicker.component.html',
	styleUrls: ['./tools-languagepicker.component.scss']
})
export class ToolsLanguagepickerComponent implements OnInit {
	constructor() {}
	debugText = '';
	hideLanguagepicker = false;
	hideListen = false;
	hideSignlang = false;
	iconVariation = 'default';
	textLanguagepicker = 'Languages';
	textListen = 'Lyssna';
	textSignlang = 'Teckenspråk';
	selectedLanguage = 'N/A';
	languages: INavigationContextMenuItem[] = [
		{
			index: 0,
			type: 'button',
			text: 'العربية (Arabiska)',
			lang: 'ar',
			value: 'ar',
			dir: 'rtl'
		},
		{
			index: 1,
			type: 'button',
			text: 'دری (Dari)',
			lang: 'prs',
			value: 'prs',
			dir: 'rtl'
		},
		{
			index: 2,
			type: 'button',
			text: 'به پارسی (Persiska)',
			lang: 'fa',
			value: 'fa',
			dir: 'rtl'
		},
		{
			index: 3,
			type: 'button',
			text: 'English (Engelska)',
			lang: 'en',
			value: 'en',
			dir: 'ltr'
		},
		{
			index: 4,
			type: 'button',
			text: 'Русский (Ryska)',
			lang: 'ru',
			value: 'ru',
			dir: 'ltr'
		},
		{
			index: 5,
			type: 'button',
			text: 'Af soomaali (Somaliska)',
			lang: 'so',
			value: 'so',
			dir: 'ltr'
		},
		{
			index: 6,
			type: 'button',
			text: 'Svenska',
			lang: 'sv',
			value: 'sv',
			dir: 'ltr'
		},
		{
			index: 7,
			type: 'button',
			text: 'ትግርኛ (Tigrinska)',
			lang: 'ti',
			value: 'ti',
			dir: 'ltr'
		}
	];
	languagesJson = [...this.languages];
	languagesJsonString = JSON.stringify(this.languagesJson);

	ngOnInit(): void {}
	stringifyEvent(e) {
		const obj = {};
		for (let k in e) {
			obj[k] = e[k];
		}
		delete obj['isTrusted'];
		delete obj['composed'];
		delete obj['timeStamp'];
		delete obj['NONE'];
		delete obj['CAPTURING_PHASE'];
		delete obj['AT_TARGET'];
		delete obj['BUBBLING_PHASE'];
		delete obj['currentTarget'];

		delete obj['srcElement'];
		return JSON.stringify(
			obj,
			(k, v) => {
				if(v instanceof Element){
					return '#' + v.getAttribute('af-id');
				}
				if (v instanceof Node) {
					console.log('V is: ');
					console.log(v);
					return 'Node';
				}
				if (v instanceof Window) return 'Window';
				return v;
			},
			' '
		);
	}
	formatEvent(e) {
		const date = new Date();
		console.log(e);
		return (
			'(' +
			date.getFullYear() +
			'-' +
			(date.getMonth() + 1) +
			'-' +
			date.getDate() +
			' ' +
			date.getHours() +
			':' +
			date.getMinutes() +
			':' +
			date.getSeconds() +
			':' +
			date.getMilliseconds() +
			')[' +
			this.stringifyEvent(e).replace(/(\r\n|\n|\r)/gm, '') +
			']\n\n'
		);
	}
	public afOnSelect(e) {
		this.selectedLanguage = e.detail;
		this.debugText = this.formatEvent(e) + this.debugText;
	}
	public afOnToggle(e) {
		this.debugText = this.formatEvent(e) + this.debugText;
	}
	public afOnChange(e) {
		this.debugText = this.formatEvent(e) + this.debugText;
	}
	public afOnBlur(e) {
		this.debugText = this.formatEvent(e) + this.debugText;
	}
	public afOnActive(e) {
		this.debugText = this.formatEvent(e) + this.debugText;
	}
	public afOnInactive(e) {
		this.debugText = this.formatEvent(e) + this.debugText;
	}
	public afOnClick(e) {
		this.debugText = this.formatEvent(e) + this.debugText;
	}

	public clearDebugText (){
		console.clear();
		this.debugText = "";
	}

	public onDemoButtonClick() {
		function shuffle(array) {
			let currentIndex = array.length,
				randomIndex;

			// While there remain elements to shuffle.
			while (currentIndex != 0) {
				// Pick a remaining element.
				randomIndex = Math.floor(Math.random() * currentIndex);
				currentIndex--;

				// And swap it with the current element.
				[array[currentIndex], array[randomIndex]] = [
					array[randomIndex],
					array[currentIndex]
				];
			}

			return array;
		}

		const demoarray = [...this.languagesJson];
		this.languagesJson = shuffle(demoarray);
	}
	public onSkanskaButtonClick() {
		this.languagesJson = [
			...this.languagesJson,
			{
				index: 10,
				type: 'button',
				text: 'Skånska',
				lang: 'pag',
				value: 'pag',
				dir: 'ltr'
			}
		];
	}
	public onRemoveLanguageButtonClick() {
		this.languagesJson.pop();
		this.languagesJson = [...this.languagesJson];
	}
}
