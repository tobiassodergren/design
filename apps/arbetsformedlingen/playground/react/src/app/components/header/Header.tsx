import { Link, useLocation } from 'react-router-dom';
import { Routes } from '../../../main';

import {
	DigiHeader,
	DigiHeaderNotification,
	DigiHeaderAvatar,
	DigiHeaderNavigation,
	DigiHeaderNavigationItem,
	DigiLogo,
	DigiIconBellFilled
} from 'arbetsformedlingen-react-dist';

import styles from './Header.module.scss';


const Header = () => {
  const { pathname } = useLocation();
  const routes = [...Routes]
  
	return (
		<DigiHeader afSystemName="Designsystem" afMenuButtonText="Meny">
			<>
				{/* <a slot="header-logo" aria-label="Boka tolk logotyp" href="/"></a> */}
        <Link to="/" slot="header-logo"></Link>
				<div slot="header-content">
					<DigiHeaderNotification af-notification-amount="8">
						<a href="/">
							<DigiIconBellFilled />
							Notiser
						</a>
					</DigiHeaderNotification>
					<DigiHeaderAvatar
						afSrc="https://media.licdn.com/dms/image/C4E03AQFpLZzvprNwTA/profile-displayphoto-shrink_800_800/0/1517354108427?e=1699488000&v=beta&t=QQ4Eam7y-seMXvIRTAA19kpeoXrQ8-iI60a6Ts-vTEs"
						afAlt="Profilbild på Jonas Dahlin"
						afName="Jonas Dahlin"
						afSignature="DAHJS"
						afIsLoggedIn={true}
						afHideSignature={true}
					></DigiHeaderAvatar>
				</div>

				<div slot="header-navigation">
					<DigiHeaderNavigation
						afCloseButtonText="Stäng"
						afCloseButtonAriaLabel="Stäng meny"
						afNavAriaLabel="Huvudmeny"
						afBackdrop={true}
					>
            {routes
              .filter((route) => route.path !== '/')
              .map((route) => 
              <DigiHeaderNavigationItem
                key={route.path}
                afCurrentPage={pathname === route.path}
              >
                <Link
                  to={route.path}
                >{route.label}</Link>
              </DigiHeaderNavigationItem>
            )}
					</DigiHeaderNavigation>
				</div>
			</>
		</DigiHeader>
	);
};

export default Header;
