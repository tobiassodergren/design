import React, { useState } from 'react';
import {
	DigiLayoutBlock,
	DigiButton,
	DigiIconHeart,
	DigiIconHeartSolid,
	DigiTypographyHeadingJumbo
} from 'arbetsformedlingen-react-dist';

const ButtonTest = () => {
	return (
		<DigiLayoutBlock afVerticalPadding>
			<DigiTypographyHeadingJumbo afText="Buggtest knappar"></DigiTypographyHeadingJumbo>
			<ButtonApp demoTemplate={1}></ButtonApp>
			<hr />
			<ButtonApp demoTemplate={2}></ButtonApp>
		</DigiLayoutBlock>
	);
};

type IDummyData = {
	title: string;
	selected: boolean;
};

function ButtonIcon(props: IDummyData) {
	if (props.selected) {
		return <DigiIconHeart slot="icon-secondary"></DigiIconHeart>;
	} else {
		return <DigiIconHeartSolid slot="icon-secondary"></DigiIconHeartSolid>;
	}
}

type buttonAppProps = {
	demoTemplate: number;
};

function ButtonApp(props: buttonAppProps) {

	const dummydataModel: IDummyData[] = [
		{ title: 'Button 1', selected: true },
		{ title: 'Button 2', selected: false },
		{ title: 'Button 3', selected: false },
		{ title: 'Button 4', selected: false }
	];
	const [dummystate, setDummyState] = useState<boolean>(false);
	const [dummydata, setDummyData] = useState<IDummyData[]>(dummydataModel);

	const setDummyStateClickHandler = () => {
		setDummyState(!dummystate);
	};

	const changeDummyData = (i: number) => {
		const data = [...dummydata];
		data[i].selected = !data[i].selected;
		setDummyData(data);
		console.log('Hello world');
	};

	if (props.demoTemplate === 1) {
		return (
			<>
			{JSON.stringify(dummydata)}
				<h1>{dummystate ? 'State is true' : 'State is false'}</h1>
				<p>
					<button type="button" onClick={setDummyStateClickHandler} style={{border:'3px solid pink', color:"red", padding: "10px"}}>
						Change state
					</button>
				</p>
				<div>
				{dummystate && (
					<DigiButton af-size="medium" af-variation="primary" af-full-width="false">
						Hello world
						<DigiIconHeartSolid slot="icon-secondary"></DigiIconHeartSolid>
					</DigiButton>
				)}
				{!dummystate && (
					<DigiButton af-size="medium" af-variation="primary" af-full-width="false">
						Hello world
						<DigiIconHeart slot="icon-secondary"></DigiIconHeart>
					</DigiButton>
				)}
				</div>
			</>
		);
	}
	if (props.demoTemplate === 2) {
		return (
			<>
				{JSON.stringify(dummydata)}
				<ul>
					{dummydata &&
						dummydata.map((data, i) => (
							<li key={data.title + i} style={{ marginBottom: '10px' }}>
									{data.selected ? (
										<DigiButton
											af-size="medium"
											af-variation="primary"
											af-full-width="false"
											onClick={() => {
												changeDummyData(i);
											}}
										>
											{data.title} - Avfölj
											<DigiIconHeartSolid slot="icon-secondary"></DigiIconHeartSolid>
										</DigiButton>
									) : (
										<DigiButton
											af-size="medium"
											af-variation="primary"
											af-full-width="false"
											key={data.title + i + 'x'}
											onClick={() => {
												changeDummyData(i);
											}}
										>
											{data.title} - Följ
											<DigiIconHeart slot="icon-secondary"></DigiIconHeart>
										</DigiButton>
									)}
								
							</li>
						))}
				</ul>
			</>
		);
	}

	return <h1>Please choose valid DEMO_TEMPLATE</h1>;
}

export default ButtonTest;
