import { FunctionalComponent, getAssetPath, h } from '@stencil/core';

export const CardLinkImage: FunctionalComponent = () => (
	<img
		slot="image"
		src={getAssetPath('/assets/images/skv-media-object-example.png')}
		width="392"
		height="220"
		style={{ width: '100%', height: 'auto', display: 'block' }}
		loading="lazy"
	/>
);
