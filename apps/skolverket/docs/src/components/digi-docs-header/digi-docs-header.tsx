import { Component, h } from '@stencil/core';
import { href } from 'stencil-router-v2';
import { router } from '@digi/skolverket-docs/global/routes/router';
import {
	mainNavigation,
	NavItem
} from '@digi/skolverket-docs/global/navigation';
import { slugify } from '@digi/utils';

interface MenuItemProps {
	item: NavItem;
	title: string;
	path: string;
	activePath: string;
}

const SubMenuItem = ({ item, title, path, activePath }: MenuItemProps) => {
	return (
		<li>
			{item.mainLinkLabel !== false && (
				<a
					{...href(`/${path}`)}
					aria-current={`/${path}` === activePath ? 'page' : null}
				>
					{item.mainLinkLabel || title}
				</a>
			)}
			{item.children && (
				<ul>
					{Object.entries(item.children).map(([key, value]) => (
						<SubMenuItem
							item={value}
							title={key}
							path={value.path || `${path}/${slugify(key)}`}
							activePath={activePath}
						/>
					))}
				</ul>
			)}
		</li>
	);
};

const MainMenuItem = ({ item, title, path, activePath }: MenuItemProps) => {
	return (
		<li>
			<button>{title}</button>
			<digi-navigation-main-menu-panel>
				{item.mainLinkLabel !== false && (
					<a
						{...href(`/${path}`)}
						slot="main-link"
						aria-current={`/${path}` === activePath ? 'page' : null}
					>
						{item.mainLinkLabel || title}
					</a>
				)}
				{item.children && (
					<ul>
						{Object.entries(item.children).map(([key, value]) => (
							<SubMenuItem
								item={value}
								title={key}
								path={value.path || `${path}/${slugify(key)}`}
								activePath={activePath}
							/>
						))}
					</ul>
				)}
			</digi-navigation-main-menu-panel>
		</li>
	);
};

@Component({
	tag: 'digi-docs-header',
	styleUrl: 'digi-docs-header.scss',
	scoped: true
})
export class DigiDocsHeader {
	mainNavigationTopLevelItems = Object.keys(mainNavigation);

	render() {
		return (
			<header>
				<digi-page-header>
					<span slot="top">
						Till <a href="https://www.skolverket.se/">skolverket.se</a>
					</span>
					<a slot="logo" aria-label="Designsystem startsida" {...href('/')}>
						<digi-logo-service
							afName={'Designsystem'}
							afDescription={`Designmönster, kod och dokumentation från Skolverket`}
						/>
					</a>
					<nav slot="nav">
						<ul>
							{this.mainNavigationTopLevelItems.map((title) => (
								<MainMenuItem
									item={mainNavigation[title]}
									title={title}
									path={mainNavigation[title].path || slugify(title)}
									activePath={router.activePath}
								/>
							))}
						</ul>
					</nav>
				</digi-page-header>
			</header>
		);
	}
}
