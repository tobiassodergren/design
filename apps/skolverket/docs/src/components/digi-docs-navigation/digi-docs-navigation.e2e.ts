import { newE2EPage } from '@stencil/core/testing';

describe('digi-docs-navigation', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-docs-navigation></digi-docs-navigation>');

    const element = await page.find('digi-docs-navigation');
    expect(element).toHaveClass('hydrated');
  });

  it('contains a "Profile Page" button', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-docs-navigation></digi-docs-navigation>');

    const element = await page.find('digi-docs-navigation >>> button');
    expect(element.textContent).toEqual('Profile page');
  });
});
