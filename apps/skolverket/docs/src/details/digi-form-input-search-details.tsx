import { Component, Prop, h, State, Fragment } from '@stencil/core';
import {
	FormInputType,
	FormInputSearchVariation,
	CodeExampleLanguage,
	FormInputButtonVariation
} from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';

@Component({
	tag: 'digi-form-input-search-details',
	scoped: true
})
export class DigiFormInputSearchDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() formInputType: FormInputType = FormInputType.SEARCH;
	@State() formInputSearchVariation: FormInputSearchVariation =
		FormInputSearchVariation.MEDIUM;
	@State() hasDescription: boolean = false;

	get searchInputCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
       <digi-form-input-search
         af-label="Etikett"
         af-button-variation="secondary"
         af-type="${this.formInputType}"\
         ${
										this.hasDescription
											? '\n         af-label-description="Beskrivande text"\t '
											: ''
									}
         af-button-text="Knapptext" 
       >
       </digi-form-input-search>`,
			[CodeExampleLanguage.ANGULAR]: `\
       <digi-form-input-search
         [attr.af-label]="'Etikett'"
				 [attr.af-button-variation]="FormInputButtonVariation.SECONDARY"
         [attr.af-type]="FormInputType.${Object.keys(FormInputType).find(
										(key) => FormInputType[key] === this.formInputType
									)}"\
         ${
										this.hasDescription
											? `\n         [attr.af-label-description]="'Beskrivande text'"\t `
											: ''
									}
         [attr.af-button-text]="'Knapptext'"
       >
       </digi-form-input-search>`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.searchInputCode)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				<div style={{ width: '550px' }}>
					<digi-form-input-search
						afLabel="Etikett"
						afLabelDescription={this.hasDescription ? 'Beskrivande text' : null}
						af-validation-text="Det här är ett valideringsmeddelande"
						afButtonVariation={FormInputButtonVariation.SECONDARY}
						af-type={this.formInputType}
						afButtonText="Sök"
					></digi-form-input-search>
				</div>
				{!this.afShowOnlyExample && (
					<div class="slot__controls" slot="controls">
						<digi-form-select
							afLabel="Typ"
							onAfOnChange={(e) => (this.formInputType = (e.target as any).value)}
							af-variation="small"
							af-value="search"
						>
							<option value="color">Färg</option>
							<option value="date">Datum</option>
							<option value="datetime-local">Lokalt datum</option>
							<option value="email">Epostadress</option>
							<option value="month">Månad</option>
							<option value="number">Nummer</option>
							<option value="password">Lösenord</option>
							<option value="search">Sök</option>
							<option value="tel">Telefonnummer</option>
							<option value="text">Text</option>
							<option value="time">Tid</option>
							<option value="url">URL</option>
							<option value="week">Vecka</option>
						</digi-form-select>
						<digi-form-fieldset afLegend="Beskrivande text">
							<digi-form-checkbox
								afLabel="Ja"
								afChecked={this.hasDescription}
								onAfOnChange={() => (this.hasDescription = !this.hasDescription)}
							></digi-form-checkbox>
						</digi-form-fieldset>
					</div>
				)}
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Sökfältskomponenten är utformad så att användaren kan ange sökord via
			ett inmatningsfält. Den består av en etikett, ett inmatningsfält och en
			knapp."
				example={this.example()}
				description={
					<Fragment>
						<h3>Knapposition</h3>
						<p>
							På Skolverket vill vi ha knappen separerad från inmatningsfältet. Justera
							detta genom att sätta <digi-code af-code="af-button-variation" /> till{' '}
							<digi-code af-code="secondary" />
						</p>
						<h3>Typer</h3>
						<p>
							Inmatningsfältet stödjer de flesta fälttyper och mappar direkt till
							inputelementets <digi-code af-code="type"></digi-code>
							-attribut.
						</p>
						<h3>Sökknapp</h3>
						<p>
							Sökfältsknappens text sätts via
							<digi-code af-code="afButtonText" />.{' '}
							<digi-code af-code="afButtonType" /> sätter knappelementets attribut
							'type'. Bör vara 'submit' eller 'button'{' '}
						</p>
					</Fragment>
				}
			/>
		);
	}
}
