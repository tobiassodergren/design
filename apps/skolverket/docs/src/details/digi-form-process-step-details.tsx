import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';
import { Component, Prop, h, Fragment } from '@stencil/core';
import { CodeExampleLanguage } from '@digi/skolverket';
import { ComponentLink } from '../components/ComponentLink';

@Component({
	tag: 'digi-form-process-step-details',
	scoped: true
})
export class DigiFormProcessStepDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;

	get formProcessStepsCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-form-process-steps>
  <li>
    <digi-form-process-step
      af-label="Tillfällig lagring"
    ></digi-form-process-step>
  </li>
  <li>
    <digi-form-process-step 
      af-href="/en-lank"
      af-label="Användare"
    ></digi-form-process-step>
   </li>
  <li>
    <digi-form-process-step
      af-label="Kontaktuppgifter"
    ></digi-form-process-step>
  </li>
  <li>
    <digi-form-process-step
      af-label="Hantera ärende"
    ></digi-form-process-step>
  </li>
</digi-form-process-steps>
				`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-form-process-steps>
  <li>
    <digi-form-process-step
      af-label="Tillfällig lagring"
    ></digi-form-process-step>
  </li>
  <li>
    <digi-form-process-step 
      af-href="/en-lank"
      af-label="Användare"
    ></digi-form-process-step>
   </li>
  <li>
    <digi-form-process-step
      af-label="Kontaktuppgifter"
    ></digi-form-process-step>
  </li>
  <li>
    <digi-form-process-step
      af-label="Hantera ärende"
    ></digi-form-process-step>
  </li>
</digi-form-process-steps>
				`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.formProcessStepsCode)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				<digi-form-process-steps afCurrentStep={3}>
					<li>
						<digi-form-process-step
							afLabel={'Tillfällig lagring'}
						></digi-form-process-step>
					</li>
					<li>
						<digi-form-process-step
							af-href="#"
							afLabel={'Användare'}
						></digi-form-process-step>
					</li>
					<li>
						<digi-form-process-step
							afLabel={'Kontaktuppgifter'}
						></digi-form-process-step>
					</li>
					<li>
						<digi-form-process-step
							afLabel={'Hantera ärende'}
						></digi-form-process-step>
					</li>
				</digi-form-process-steps>
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				example={this.example()}
				preamble="För större formulär används en paginering som ger användaren en guidning kring var hen befinner sig i processen och en uppfattning om hur mycket arbete som återstår. Alla stegade formulär och processer bör ha ett sista steg som sammanställer allt för granskning innan formuläret skickas in."
				description={
					<Fragment>
						<p>
							Denna komponent ska alltid användas inuti{' '}
							<ComponentLink
								tag="digi-form-process-steps"
								text="processtegskomponenten"
							/>
							. Se den för användning och riktlinjer.
						</p>
					</Fragment>
				}
			/>
		);
	}
}
