import { Component, h, Prop, State } from '@stencil/core';
import {
	CodeExampleLanguage,
	FormRadiobuttonVariation
} from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';

@Component({
	tag: 'digi-form-radiogroup-details',
	scoped: true
})
export class DigiFormRadiogroup {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() FormRadiobuttonVariation: FormRadiobuttonVariation =
		FormRadiobuttonVariation.PRIMARY;
	@State() validation: boolean = false;

	get radiobuttonCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-form-radiogroup af-name="myRadiogroupName">
	<digi-form-radiobutton 
		af-label="Kryssruta 1"
		af-value="val-1"
	></digi-form-radiobutton>
	<digi-form-radiobutton 
		af-label="Kryssruta 2"
		af-value="val-2"
	></digi-form-radiobutton>
	<digi-form-radiobutton 
		af-label="Kryssruta 3"
		af-value="val-3"
	></digi-form-radiobutton>
</digi-form-radiogroup>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-form-radiogroup 
	[attr.af-name]="'myRadiogroupName'"
	formControlName="myRadiogroup"
>
	<digi-form-radiobutton
		[attr.af-label]="'Kryssruta 1'"
		[attr.af-value]="'val-1'"
	></digi-form-radiobutton>
	<digi-form-radiobutton
		[attr.af-label]="'Kryssruta 2'"
		[attr.af-value]="'val-2'"
	></digi-form-radiobutton>
	<digi-form-radiobutton
		[attr.af-label]="'Kryssruta 3'"
		[attr.af-value]="'val-3'"
	></digi-form-radiobutton>
</digi-form-radiogroup>`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.radiobuttonCode)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				<digi-form-radiogroup>
					<digi-form-radiobutton afLabel="Kryssruta 1" afValue="val-1" />
					<digi-form-radiobutton afLabel="Kryssruta 2" afValue="val-2" />
					<digi-form-radiobutton afLabel="Kryssruta 3" afValue="val-3" />
				</digi-form-radiogroup>
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Den här komponenten används för att fånga upp värdet för en grupp av
			radioknappar."
				example={this.example()}
				description={
					<p>
						Den här komponenten kan användas för att få stöd för{' '}
						<digi-code afCode="ControlValueAccessor"></digi-code> för radioknappar i
						Angular.
					</p>
				}
			/>
		);
	}
}
