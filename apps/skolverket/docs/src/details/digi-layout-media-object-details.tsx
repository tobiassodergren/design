import {
	Component,
	Prop,
	h,
	State,
	getAssetPath,
	Fragment
} from '@stencil/core';
import {
	CodeExampleLanguage,
	LayoutMediaObjectAlignment
} from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';

@Component({
	tag: 'digi-layout-media-object-details',
	scoped: true
})
export class DigiLayoutMediaObjectDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() layoutMediaObjectAlignment: LayoutMediaObjectAlignment =
		LayoutMediaObjectAlignment.START;

	get layoutMediaObjectCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-layout-media-object af-alignment="${this.layoutMediaObjectAlignment}">
  <digi-media-image slot="media">
  </digi-media-image>
    ...  
</digi-layout-media-object>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-layout-media-object
  [attr.af-alignment]="LayoutColumnsVariation.${Object.keys(
			LayoutMediaObjectAlignment
		).find(
			(key) => LayoutMediaObjectAlignment[key] === this.layoutMediaObjectAlignment
		)}">
  <digi-media-image slot="media">
  </digi-media-image>
    ...  
</digi-layout-media-object>`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.layoutMediaObjectCode)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				{!this.afHideControls && (
					<div class="slot__controls" slot="controls">
						<digi-form-fieldset
							af-legend="Justering"
							af-name="Justering"
							onChange={(e) =>
								(this.layoutMediaObjectAlignment = (e.target as any).value)
							}
						>
							<digi-form-radiobutton
								afName="Justering"
								afLabel="Start"
								afValue={LayoutMediaObjectAlignment.START}
								afChecked={true}
							/>
							<digi-form-radiobutton
								afName="Justering"
								afLabel="Mitten"
								afValue={LayoutMediaObjectAlignment.CENTER}
							/>
							<digi-form-radiobutton
								afName="Justering"
								afLabel="Slut"
								afValue={LayoutMediaObjectAlignment.END}
							/>
							<digi-form-radiobutton
								afName="Justering"
								afLabel="Stretch"
								afValue={LayoutMediaObjectAlignment.STRETCH}
							/>
						</digi-form-fieldset>
					</div>
				)}
				<digi-layout-media-object afAlignment={this.layoutMediaObjectAlignment}>
					<digi-media-image
						style={{ display: 'block', width: '300px' }}
						slot="media"
						afUnlazy
						af-src={getAssetPath('/assets/images/skv-media-object-example.png')}
						afAlt="Skolverkets logotyp"
					></digi-media-image>
					<h3>Medieobjektet</h3>
					<p>
						Nullam quis risus eget urna mollis ornare vel eu leo. Vivamus sagittis
						lacus vel augue laoreet rutrum faucibus dolor auctor. Vestibulum id ligula
						porta felis euismod semper. Nulla vitae elit libero, a pharetra augue.
					</p>
				</digi-layout-media-object>
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Standardlayout för att visa bild (oftast) med text bredvid"
				example={this.example()}
				description={
					<Fragment>
						<p>
							Medieobjekt är ett klassiskt layoutmönster för att visa bild med text
							bredvid.
						</p>
						<p>
							Med hjälp av <digi-code af-code="af-alignment" /> kan medieobjektet
							justeras till start, end, center eller stretch. Start är standard.
							Typskriptanvändare bör importera och använda{' '}
							<digi-code af-code="LayoutMediaObjectAlignment" /> för att ställa in
							justering.
						</p>
						<p>
							<a href="https://developer.mozilla.org/en-US/docs/Web/CSS/Layout_cookbook/Media_objects">
								Om medieobjekt på MDN
							</a>
						</p>
					</Fragment>
				}
			/>
		);
	}
}
