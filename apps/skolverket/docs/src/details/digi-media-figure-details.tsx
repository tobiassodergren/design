import { Component, Prop, h, State, getAssetPath } from '@stencil/core';
import { CodeExampleLanguage, MediaFigureAlignment } from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';

@Component({
	tag: 'digi-media-figure-details',
	scoped: true
})
export class DigiMediaFigureDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() hasCaption: boolean = true;
	@State() mediaFigureAlignment: MediaFigureAlignment =
		MediaFigureAlignment.START;

	get mediaFigureCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
  <digi-media-figure\
  ${
			this.hasCaption
				? `\n    af-figcaption="Jag är en bildtext" \n    af-alignment="${this.mediaFigureAlignment}"\n  >`
				: '>'
		}
    <digi-media-image af-src="..."/>
  </digi-media-figure>`,
			[CodeExampleLanguage.ANGULAR]: `\
  <digi-media-figure
${
	this.hasCaption
		? `    [attr.af-figcaption]="Jag är en bildtext" \n    [attr.af-alignment]="MediaFigureAlignment.${Object.keys(
				MediaFigureAlignment
		  ).find(
				(key) => MediaFigureAlignment[key] === this.mediaFigureAlignment
		  )}"\n  >`
		: '>'
}
    <digi-media-image [attr.af-src]="..."/>
  </digi-media-figure>`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.mediaFigureCode)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				{!this.afHideControls && (
					<div class="slot__controls" slot="controls">
						<digi-form-fieldset afLegend="Bildtext">
							<digi-form-checkbox
								afLabel="Ja"
								afChecked={this.hasCaption}
								onAfOnChange={() =>
									this.hasCaption ? (this.hasCaption = false) : (this.hasCaption = true)
								}
							></digi-form-checkbox>
						</digi-form-fieldset>
						{this.hasCaption && (
							<digi-form-fieldset
								afName="Justering"
								afLegend="Bildtextens placering"
								onChange={(e) => (this.mediaFigureAlignment = (e.target as any).value)}
							>
								<digi-form-radiobutton
									afName="Justering"
									afLabel="Start"
									afValue={MediaFigureAlignment.START}
									afChecked={this.mediaFigureAlignment === MediaFigureAlignment.START}
								/>
								<digi-form-radiobutton
									afName="Justering"
									afLabel="Center"
									afValue={MediaFigureAlignment.CENTER}
									afChecked={this.mediaFigureAlignment === MediaFigureAlignment.CENTER}
								/>
								<digi-form-radiobutton
									afName="Justering"
									afLabel="Slut"
									afValue={MediaFigureAlignment.END}
									afChecked={this.mediaFigureAlignment === MediaFigureAlignment.END}
								/>
							</digi-form-fieldset>
						)}
					</div>
				)}
				<div style={{ width: '420px' }}>
					{this.hasCaption && (
						<digi-media-figure
							af-figcaption="Jag är en bildtext"
							af-alignment={this.mediaFigureAlignment}
						>
							<digi-media-image
								afUnlazy
								afHeight="533"
								afWidth="800"
								af-src={getAssetPath('/assets/images/skv-media-object-example.png')}
								afAlt="Illustration av en skola"
							></digi-media-image>
						</digi-media-figure>
					)}
					{!this.hasCaption && (
						<digi-media-figure af-alignment={this.mediaFigureAlignment}>
							<digi-media-image
								afUnlazy
								afHeight="533"
								afWidth="800"
								af-src={getAssetPath('/assets/images/skv-media-object-example.png')}
								afAlt="Illustration av en skola"
							></digi-media-image>
						</digi-media-figure>
					)}
				</div>
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Denna komponent kan användas för att omsluta en bild eller vår
			mediebildkomponent. Det går också att lägga till en bildtext."
				example={this.example()}
				description={
					<p>
						Skapar upp ett <digi-code af-code="<figure>" />
						-element, och ett <digi-code af-code="<figcaption>" />
						-element om det finns en bildtext.
					</p>
				}
			/>
		);
	}
}
