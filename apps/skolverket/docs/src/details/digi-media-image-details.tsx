import { Component, Prop, h, getAssetPath, Fragment } from '@stencil/core';
import {
	CodeExampleLanguage,
	NotificationDetailVariation
} from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';

@Component({
	tag: 'digi-media-image-details',
	scoped: true
})
export class DigiMediaImageDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;

	get mediaFigureCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-media-image
  af-height="300"
  af-width="300"
  af-src='image.jpg'
  af-alt="Beskrivande text"
>
</digi-media-image>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-media-image
  [attr.af-height]="300"
  [attr.af-width]="300"
  af-src='image.jpg'
  af-alt="Beskrivande text"
>
</digi-media-image>`
		};
	}

	example() {
		return (
			<digi-layout-rows>
				<digi-code-example
					af-code={JSON.stringify(this.mediaFigureCode)}
					af-hide-controls={this.afHideControls ? 'true' : 'false'}
					af-hide-code={this.afHideCode ? 'true' : 'false'}
				>
					<digi-media-image
						afHeight="533"
						afWidth="800"
						af-src={getAssetPath('/assets/images/skv-media-object-example.png')}
						afAlt="Illustration av en skola"
					></digi-media-image>
				</digi-code-example>
				<digi-notification-detail afVariation={NotificationDetailVariation.WARNING}>
					<h3 slot="heading">Kommer snart att skrivas om</h3>
					<p>
						Den här komponenten kommer snart att ersättas med en ny som använder mer
						standardiserad webbläsarfunktionalitet. Det kommer innebära en del
						förändringar i api:et
					</p>
				</digi-notification-detail>
			</digi-layout-rows>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Det här är Skolverkets komponent för bilder."
				example={this.example()}
				description={
					<Fragment>
						<h3>Alt-text</h3>
						<p>
							Alt-texten är obligatorisk och ställs in med{' '}
							<digi-code af-code="af-alt" />.
						</p>
						<h3>Bredd och höjd</h3>
						<p>
							Med hjälp av <digi-code af-code="af-height" /> och{' '}
							<digi-code af-code="af-width" /> går det att ställa in bildens storlek.
							Det går också att använda <digi-code af-code="af-fullwidth" /> så kommer
							bilden sättas till 100% och anpassar höjden automatiskt efter bredden.
						</p>
						<h3>Lazy loading</h3>
						<p>
							Bilden har inbyggd lazy loadning. För att ta bort det kan man sätta
							attributet <digi-code af-code="af-unlazy" /> till true. Om komponenten
							får mått för bredd och höjd så kan en platshållare se till så att sidan
							inte hoppar till när bilden har laddat färdigt.
						</p>
					</Fragment>
				}
			/>
		);
	}
}
