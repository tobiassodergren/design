import { Component, Prop, h, Fragment } from '@stencil/core';
import { CodeExampleLanguage } from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';
import { ComponentLink } from '../components/ComponentLink';

@Component({
	tag: 'digi-navigation-tab-details',
	scoped: true,
	styles: `digi-navigation-tab p { margin-block-start: var(--digi--responsive-grid-gutter); }`
})
export class DigiNavigationTabDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;

	get navigationTabCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-navigation-tabs
  af-aria-label="En tablist label"
  >
  <digi-navigation-tab 
    af-aria-label="Flik 1" 
    af-id="flikb1"
  >
    <p>Jag är flik 1</p>
  </digi-navigation-tab>
  <digi-navigation-tab 
    af-aria-label="Flik 2" 
    af-id="flikb2"
  >
    <p>Jag är flik 2</p>
  </digi-navigation-tab>
  <digi-navigation-tab 
    af-aria-label="Flik 3" 
    af-id="flikb3"
  >
    <p>Jag är flik 3</p>
  </digi-navigation-tab>
</digi-navigation-tabs>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-navigation-tabs
  [attr.af-aria-label]="En tablist label"
  >
  <digi-navigation-tab 
    [attr.af-aria-label]="'Flik 1'" 
    [attr.af-id]="flikb1"
  >
    <p>Jag är flik 1</p>
  </digi-navigation-tab>
  <digi-navigation-tab 
    [attr.af-aria-label]="'Flik 2'" 
    [attr.af-id]="flikb2"
  >
    <p>Jag är flik 2</p>
  </digi-navigation-tab>
  <digi-navigation-tab 
    [attr.af-aria-label]="'Flik 3'" 
    [attr.af-id]="flikb3"
  >
    <p>Jag är flik 3</p>
  </digi-navigation-tab>
</digi-navigation-tabs>`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.navigationTabCode)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				<digi-navigation-tabs afAriaLabel="En tablist label">
					<digi-navigation-tab afAriaLabel="Flik 1" afId="flikb1">
						<p>Jag är flik 1</p>
					</digi-navigation-tab>
					<digi-navigation-tab afAriaLabel="Flik 2" afId="flikb2">
						<p>Jag är flik 2</p>
					</digi-navigation-tab>
					<digi-navigation-tab afAriaLabel="Flik 3" afId="flikb3">
						<p>Jag är flik 3</p>
					</digi-navigation-tab>
				</digi-navigation-tabs>
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="En flikkomponent som alltid ska användas inuti ett flikfält."
				example={this.example()}
				description={
					<Fragment>
						<p>
							Komponenten ska alltid användas inuti{' '}
							<ComponentLink tag="digi-navigation-tabs" /> och fungerar ej fristående
							utan.
						</p>
						<p>
							Varje flik skapar upp en tabpanel inuti flikfältskomponenten. afId och
							afAriaLabel används för att skapa upp kopplingarna mellan flikfältet och
							de enskilda flikarna.
						</p>
					</Fragment>
				}
			/>
		);
	}
}
