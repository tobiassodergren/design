import { Component, Prop, h, Fragment } from '@stencil/core';
import { CodeExampleLanguage, CodeLanguage } from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';

@Component({
	tag: 'digi-navigation-toc-details',
	scoped: true
})
export class DigiNavigationTocDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;

	get code() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-navigation-toc>
  <h2 slot="heading">Innehåll på denna sida</h2>
  <li><a href="#oversikt">Översikt</a></li>
  <li><a href="#kontakt">Kontakt</a></li>
  <li><a href="#hitta_hit">Hitta hit</a></li>
</digi-navigation-toc>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-navigation-toc>
  <h2 slot="heading">Innehåll på denna sida</h2>
  <li><a href="#oversikt">Översikt</a></li>
  <li><a href="#kontakt">Kontakt</a></li>
  <li><a href="#hitta_hit">Hitta hit</a></li>
</digi-navigation-toc>`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.code)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				<digi-navigation-toc>
					<h2 slot="heading">Innehåll på denna sida</h2>
					<li>
						<a href="#oversikt">Översikt</a>
					</li>
					<li>
						<a href="#kontakt">Kontakt</a>
					</li>
					<li>
						<a href="#hitta_hit">Hitta hit</a>
					</li>
				</digi-navigation-toc>
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Innehållsförteckning för sidor med mycket innehåll."
				example={this.example()}
				description={
					<Fragment>
						<p>
							För längre innehållsidor kan en inpage-scroll användas. Den består av
							ankarlänkar och listar alla H2-rubriker. Den placeras efter ingress i
							textytan.
						</p>
						<p>
							Komponenten övervakar förändringar i{' '}
							<digi-code
								af-code="window.location.hash"
								afLanguage={CodeLanguage.JAVASCRIPT}
							/>{' '}
							och lägger automatiskt till <digi-code af-code="aria-current" /> på
							relevant länk
						</p>
					</Fragment>
				}
			/>
		);
	}
}
