import { Component, Prop, h, State, Fragment } from '@stencil/core';
import {
	NotificationAlertVariation,
	CodeExampleLanguage,
	FormCheckboxVariation
} from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';
import { ComponentLink } from '@digi/skolverket-docs/components/ComponentLink';

@Component({
	tag: 'digi-notification-alert-details',
	scoped: true
})
export class DiginNotificationAlertTagDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() closeable: boolean = false;
	@State() notificationAlertVariation: NotificationAlertVariation =
		NotificationAlertVariation.INFO;

	get notificationAlertCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-notification-alert
  af-variation="${this.notificationAlertVariation}"
  ${this.closeable ? `af-closeable="${this.closeable}"\n  >` : '>'}
  <h2 slot="heading">En rubrik</h2>
	Meddelandebeskrivning
  <a href="#" slot="link">En länk</a>
</digi-notification-alert>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-notification-alert
  [attr.af-variation]="NotificationAlertVariation.${Object.keys(
			NotificationAlertVariation
		).find(
			(key) => NotificationAlertVariation[key] === this.notificationAlertVariation
		)}"
  ${this.closeable ? `[attr.af-closeable]="${this.closeable}"\n  >` : '>'}
  <h2 slot="heading">En rubrik</h2>
  Meddelandebeskrivning
  <a href="#" slot="link">En länk</a>
</digi-notification-alert>`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.notificationAlertCode)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				{!this.afHideControls && (
					<div class="slot__controls" slot="controls">
						<digi-form-fieldset
							afName="Variation"
							afLegend="Variant"
							onChange={(e) =>
								(this.notificationAlertVariation = (e.target as any).value)
							}
						>
							<digi-form-radiobutton
								afName="Variation"
								afLabel="Info"
								afValue={NotificationAlertVariation.INFO}
								afChecked={
									this.notificationAlertVariation === NotificationAlertVariation.INFO
								}
							/>
							<digi-form-radiobutton
								afName="Variation"
								afLabel="Fara"
								afValue={NotificationAlertVariation.DANGER}
								afChecked={
									this.notificationAlertVariation === NotificationAlertVariation.DANGER
								}
							/>
							<digi-form-radiobutton
								afName="Variation"
								afLabel="Varning"
								afValue={NotificationAlertVariation.WARNING}
								afChecked={
									this.notificationAlertVariation === NotificationAlertVariation.WARNING
								}
							/>
						</digi-form-fieldset>
						<digi-form-fieldset af-legend="Stängbar">
							<digi-form-checkbox
								afLabel="Ja"
								afVariation={FormCheckboxVariation.PRIMARY}
								afChecked={this.closeable}
								onAfOnChange={() =>
									this.closeable ? (this.closeable = false) : (this.closeable = true)
								}
							></digi-form-checkbox>
						</digi-form-fieldset>
					</div>
				)}
				<digi-notification-alert
					af-variation={this.notificationAlertVariation}
					af-closeable={this.closeable}
				>
					<h2 slot="heading">En rubrik</h2>
					<p>Meddelandebeskrivning</p>
					<a href="#" slot="link">
						En länk
					</a>
				</digi-notification-alert>
				`
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Informationsmeddelande används för att lyfta fram viktig
			information till användaren."
				example={this.example()}
				description={
					<Fragment>
						<p>
							För att skapa ett pålitligt och konsekvent system där Skolverket är en
							tydlig avsändare samtidigt som tjänster och sajter särskiljande kommer
							till sin rätt har ett system i tre nivåer tagits fram.
						</p>
						<h3>Globala meddelanden</h3>
						<p>
							Globala meddelande placeras över{' '}
							<ComponentLink tag="digi-navigation-breadcrumbs" text="sidhuvudet" />{' '}
							puttar ner den med sin egen höjd. Används för meddelanden som berör hela
							siten och en stor del av användarna, om möjligt är lokala och detaljerade
							meddelanden att föredra då en mindre målgrupp berörs.
						</p>
						<h3>Lokala meddelanden</h3>
						<p>
							Lokala meddelanden placeras efter{' '}
							<ComponentLink tag="digi-navigation-breadcrumbs" text="brödsmulor" /> och
							puttar ner resterande innehåll. Används för att markera att meddelandet
							gäller just den här delen av strukturen på webben.
						</p>
						<h3>Detaljmeddelanden</h3>
						<p>
							Detaljmeddelanden placeras löpande där de behövs. Denna meddelandetyp har
							en egen komponent: <ComponentLink tag="digi-notification-detail" />.
						</p>
					</Fragment>
				}
			/>
		);
	}
}
