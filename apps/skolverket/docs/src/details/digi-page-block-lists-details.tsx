import { Component, Prop, h, State, Fragment } from '@stencil/core';
import { CodeExampleLanguage, PageBlockListsVariation } from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';
import { ComponentLink } from '../components/ComponentLink';

@Component({
	tag: 'digi-page-block-lists-details',
	scoped: true
})
export class DigiPageBlockListsDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() variation: PageBlockListsVariation = PageBlockListsVariation.START;

	get code() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-page-block-lists
  af-variation="${this.variation}"
>
  <h2 slot="heading">Avsnittsrubrik</h2>
  <digi-list-link>
    <li><a href="#">Undersida</a></li>
    <li><a href="#">Undersida</a></li>
    <li><a href="#">Undersida</a></li>
    <li><a href="#">Undersida</a></li>
  </digi-list-link>
  <digi-list-link>
    <li><a href="#">Undersida</a></li>
    <li><a href="#">Undersida</a></li>
    <li><a href="#">Undersida</a></li>
    <li><a href="#">Undersida</a></li>
  </digi-list-link>
</digi-page-block-lists>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-page-block-lists
  [attr.af-variation]="PageBlockListsVariation.${Object.keys(
			PageBlockListsVariation
		).find((key) => PageBlockListsVariation[key] === this.variation)}"
>
  <h2 slot="heading">Avsnittsrubrik</h2>
  <digi-list-link>
	  <li><a href="#">Undersida</a></li>
	  <li><a href="#">Undersida</a></li>
	  <li><a href="#">Undersida</a></li>
	  <li><a href="#">Undersida</a></li>
  </digi-list-link>
  <digi-list-link>
	  <li><a href="#">Undersida</a></li>
	  <li><a href="#">Undersida</a></li>
	  <li><a href="#">Undersida</a></li>
	  <li><a href="#">Undersida</a></li>
  </digi-list-link>
</digi-page-block-lists>`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.code)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				{!this.afHideControls && (
					<div class="slot__controls" slot="controls">
						<digi-form-fieldset
							afName="Variation"
							afLegend="Variation"
							onChange={(e) => (this.variation = (e.target as any).value)}
						>
							<digi-form-radiobutton
								afName="Variation"
								afLabel="Startsida"
								afValue={PageBlockListsVariation.START}
								afChecked={this.variation === PageBlockListsVariation.START}
							/>
							<digi-form-radiobutton
								afName="Variation"
								afLabel="Undersida"
								afValue={PageBlockListsVariation.SUB}
								afChecked={this.variation === PageBlockListsVariation.SUB}
							/>
							<digi-form-radiobutton
								afName="Variation"
								afLabel="Sektionssida"
								afValue={PageBlockListsVariation.SECTION}
								afChecked={this.variation === PageBlockListsVariation.SECTION}
							/>
							<digi-form-radiobutton
								afName="Variation"
								afLabel="Processida"
								afValue={PageBlockListsVariation.PROCESS}
								afChecked={this.variation === PageBlockListsVariation.PROCESS}
							/>
							<digi-form-radiobutton
								afName="Variation"
								afLabel="Artikelsida"
								afValue={PageBlockListsVariation.ARTICLE}
								afChecked={this.variation === PageBlockListsVariation.ARTICLE}
							/>
						</digi-form-fieldset>
					</div>
				)}
				<digi-page-block-lists af-variation={this.variation}>
					<h2 slot="heading">Avsnittsrubrik</h2>
					<digi-list-link>
						<li>
							<a href="#">Undersida</a>
						</li>
						<li>
							<a href="#">Undersida</a>
						</li>
						<li>
							<a href="#">Undersida</a>
						</li>
						<li>
							<a href="#">Undersida</a>
						</li>
					</digi-list-link>
					<digi-list-link>
						<li>
							<a href="#">Undersida</a>
						</li>
						<li>
							<a href="#">Undersida</a>
						</li>
						<li>
							<a href="#">Undersida</a>
						</li>
						<li>
							<a href="#">Undersida</a>
						</li>
					</digi-list-link>
				</digi-page-block-lists>
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Sidblockskomponent med länkade kort."
				example={this.example()}
				description={
					<Fragment>
						<p>
							Blocket förväntar sig flera{' '}
							<ComponentLink tag="digi-card-link" text="länkade kort" /> som barn.
						</p>
					</Fragment>
				}
			/>
		);
	}
}
