import { Component, Prop, h, State, Fragment } from '@stencil/core';
import { CodeExampleLanguage, TypographyTimeVariation } from '@digi/skolverket';
import { ComponentDetails } from '../components/ComponentDetails';

@Component({
	tag: 'digi-typography-time-details',
	scoped: true
})
export class DigiTypographyTimeDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() typographyTimeVariation: TypographyTimeVariation =
		TypographyTimeVariation.PRIMARY;
	@State() dateDistance: any = new Date().getDate();

	get dateCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-typography-time
	af-variation="${this.typographyTimeVariation}"
	af-date-time="${
		this.dateDistance == new Date().getDate() ? '' : this.dateDistance
	}"
>
</digi-typography-time>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-typography-time
	[attr.af-variation]="TypographyTimeVariation.${Object.keys(
		TypographyTimeVariation
	).find(
		(key) => TypographyTimeVariation[key] === this.typographyTimeVariation
	)}"
	[attr.af-date-time]="${
		this.dateDistance == new Date().getDate() ? 'new Date()' : this.dateDistance
	}"
>
</digi-typography-time>`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.dateCode)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				{!this.afHideControls && (
					<div class="slot__controls" slot="controls">
						<digi-form-fieldset
							afName="Variant"
							afLegend="Variant"
							onChange={(e) =>
								(this.typographyTimeVariation = (e.target as any).value)
							}
						>
							<digi-form-radiobutton
								afName="Variant"
								afLabel="Primär"
								afValue={TypographyTimeVariation.PRIMARY}
								afChecked={
									this.typographyTimeVariation === TypographyTimeVariation.PRIMARY
								}
							/>
							<digi-form-radiobutton
								afName="Variant"
								afLabel="Naturlig"
								afValue={TypographyTimeVariation.PRETTY}
								afChecked={
									this.typographyTimeVariation === TypographyTimeVariation.PRETTY
								}
							/>
							<digi-form-radiobutton
								afName="Variant"
								afLabel="Distans"
								afValue={TypographyTimeVariation.DISTANCE}
								afChecked={
									this.typographyTimeVariation === TypographyTimeVariation.DISTANCE
								}
							/>
						</digi-form-fieldset>
						<digi-form-fieldset
							afName="Datum"
							afLegend="Datum"
							onChange={(e) => (this.dateDistance = (e.target as any).value)}
						>
							{this.typographyTimeVariation === TypographyTimeVariation.DISTANCE && (
								<span>
									<digi-form-radiobutton
										afName="Datum"
										afLabel="Idag"
										afValue={new Date().toString()}
										afChecked={true}
									/>
									<digi-form-radiobutton
										afName="Datum"
										afLabel="Bak i tiden"
										afValue={'2022-04-30'}
										afChecked={this.dateDistance === '2022-04-30'}
									/>
									<digi-form-radiobutton
										afName="Datum"
										afLabel="Fram i tiden"
										afValue={'2024-12-01'}
										afChecked={this.dateDistance === '2024-12-01'}
									/>
								</span>
							)}

							{this.typographyTimeVariation !== TypographyTimeVariation.DISTANCE && (
								<p>
									<em>Datumalternativen är endast tillgängliga för distansvarianten.</em>
								</p>
							)}
						</digi-form-fieldset>
					</div>
				)}

				<digi-typography-time
					afVariation={this.typographyTimeVariation}
					afDateTime={
						this.typographyTimeVariation != 'distance'
							? new Date()
							: new Date(this.dateDistance)
					}
				></digi-typography-time>
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Datumkomponenten används för att formatera datum så att det stämmer
			överens med Skolverkets grafiska profil."
				example={this.example()}
				description={
					<Fragment>
						<h3>Varianter</h3>
						<p>
							Datumkomponenten finns i tre olika varianter: primär, distans och
							naturlig. Det sätts med hjälp av <digi-code af-code="af-variation" />.{' '}
							<digi-code af-code="af-date-time" /> används för önskat datum, den
							förväntar sig ett datumobjekt eller ett datumformatterat nummer.
						</p>
					</Fragment>
				}
			/>
		);
	}
}
