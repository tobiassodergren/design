import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';
import { Component, Fragment, h } from '@stencil/core';

@Component({
	tag: 'digi-util-breakpoint-observer-details',
	scoped: true
})
export class DigiUtilBreakpointObserverDetails {
	render() {
		return (
			<ComponentDetails
				showOnlyExample={false}
				preamble="Observerar förändringar i satta fönsterstorlekar och skapar events när
      brytpunkterna ändras."
				description={
					<Fragment>
						<p>
							Det är användbart när du behöver ändra egenskaper eller värden i
							javascript baserat på olika fasta storlekar på webbläsarfönstret.
						</p>
						<p>Pseudokodsexempel i Angularformat nedan:</p>
						<digi-code-block
							afCode={`<digi-util-breakpoint-observer (afOnMedium)="isMedium === true">
  <div *ngIf="isMedium">När fönsterstorleken är över medium</div>
  <div *ngId="!isMedium">När fönsterstorleken inte är över medium</div>
</digi-util-breakpoint-observer>`}
						/>
					</Fragment>
				}
			/>
		);
	}
}
