import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';
import { Component, h, Fragment } from '@stencil/core';

@Component({
	tag: 'digi-util-mutation-observer-details',
	scoped: true
})
export class DigiUtilMutationObserverDetails {
	render() {
		return (
			<ComponentDetails
				showOnlyExample={false}
				preamble="Övervakar när element förändras, tas bort eller läggs till i dokumentet."
				description={
					<Fragment>
						<p>
							Det är mycket användbart för att upptäcka ändringar i element inuti
							dokumentet (när noder läggs till eller tas bort, ändrar attribut etc).
						</p>
						<p>
							Komponenten implementerar{' '}
							<a href="https://developer.mozilla.org/en-US/docs/Web/API/Mutation_Observer_API">
								Mutation Observer API
							</a>
							, och accepterar dess inställningar som ett attribut.
						</p>
						<p>Pseudokodsexempel i Angularformat nedan:</p>
						<digi-code-block
							afCode={`<digi-util-mutation-observer (afOnChange)="itemsChanged = true">
  <ul>
    <li *ngFor="let item of items">
      <a href="/{{item.href}}">{{item.title}}</a>
    </li>
  </ul>
</digi-util-mutation-observer>`}
						/>
					</Fragment>
				}
			/>
		);
	}
}
