import { componentCategoryMap } from '@digi/skolverket-docs/data/categoryData';
import { slugify } from '@digi/utils';
import { components } from '@digi/skolverket-docs/data/componentData';
import { ComponentTag } from './componentTagsAndNames.generated';

/**
 * Generates route to component page (with or without opening slash)
 */
export const getComponentRoute = (tag: ComponentTag, openingSlash = false) => {
	const component = components.find((component) => component.tag === tag);
	return `${openingSlash ? '/' : ''}komponenter/${slugify(
		componentCategoryMap[tag] || 'ovrigt'
	)}/${slugify(
		component.docsTags?.find((docsTag) => docsTag.name === 'swedishName')?.text ||
			tag
	)}`;
};
