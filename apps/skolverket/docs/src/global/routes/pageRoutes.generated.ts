// Autogenerated file 

 export const pageRoutes = [
 {
  "route": "/grafisk-profil/bildsprak",
  "tag": "page-bildsprak"
 },
 {
  "route": "/grafisk-profil/farger",
  "tag": "page-farger"
 },
 {
  "route": "/grafisk-profil/grafik",
  "tag": "page-grafik"
 },
 {
  "route": "/grafisk-profil/illustrationer",
  "tag": "page-illustrationer"
 },
 {
  "route": "/grafisk-profil",
  "tag": "page-grafisk-profil"
 },
 {
  "route": "/grafisk-profil/logotyp",
  "tag": "page-logotyp"
 },
 {
  "route": "/grafisk-profil/namngivning-och-klarsprak",
  "tag": "page-namngivning-och-klarsprak"
 },
 {
  "route": "/grafisk-profil/typografi",
  "tag": "page-typografi"
 },
 {
  "route": "/grafisk-profil/varumarket",
  "tag": "page-varumarket"
 },
 {
  "route": "/",
  "tag": "page-home"
 },
 {
  "route": "/kom-igang",
  "tag": "page-kom-igang"
 },
 {
  "route": "/kom-igang/jobba-med-design",
  "tag": "page-jobba-med-design"
 },
 {
  "route": "/kom-igang/jobba-med-kod",
  "tag": "page-jobba-med-kod"
 },
 {
  "route": "/kom-igang/release-notes",
  "tag": "page-release-notes"
 },
 {
  "route": "/komponenter/dialog",
  "tag": "page-category-dialog"
 },
 {
  "route": "/komponenter/formular",
  "tag": "page-category-formular"
 },
 {
  "route": "/komponenter/ikoner",
  "tag": "page-category-ikoner"
 },
 {
  "route": "/komponenter",
  "tag": "page-category-komponenter"
 },
 {
  "route": "/komponenter/knappar",
  "tag": "page-category-knappar"
 },
 {
  "route": "/komponenter/kod",
  "tag": "page-category-kod"
 },
 {
  "route": "/komponenter/kort",
  "tag": "page-category-kort"
 },
 {
  "route": "/komponenter/laddningsindikatorer",
  "tag": "page-category-laddningsindikatorer"
 },
 {
  "route": "/komponenter/lankar",
  "tag": "page-category-lankar"
 },
 {
  "route": "/komponenter/layout",
  "tag": "page-category-layout"
 },
 {
  "route": "/komponenter/listor",
  "tag": "page-category-listor"
 },
 {
  "route": "/komponenter/logo",
  "tag": "page-category-logo"
 },
 {
  "route": "/komponenter/meddelanden",
  "tag": "page-category-meddelanden"
 },
 {
  "route": "/komponenter/media",
  "tag": "page-category-media"
 },
 {
  "route": "/komponenter/navigation",
  "tag": "page-category-navigation"
 },
 {
  "route": "/komponenter/sidblock-och-delar",
  "tag": "page-category-sidblock-och-delar"
 },
 {
  "route": "/komponenter/tabeller",
  "tag": "page-category-tabeller"
 },
 {
  "route": "/komponenter/typografi",
  "tag": "page-category-typografi"
 },
 {
  "route": "/komponenter/utfallbart",
  "tag": "page-category-utfallbart"
 },
 {
  "route": "/komponenter/verktyg",
  "tag": "page-category-verktyg"
 },
 {
  "route": "/sprak",
  "tag": "page-sprak"
 },
 {
  "route": "/sprak/klarsprak",
  "tag": "page-klarsprak"
 },
 {
  "route": "/sprak/oversattningar",
  "tag": "page-oversattningar"
 },
 {
  "route": "/tillganglighet",
  "tag": "page-tillganglighet"
 },
 {
  "route": "/tillganglighet/lagkrav-och-riktlinjer",
  "tag": "page-lagkrav-och-riktlinjer"
 },
 {
  "route": "/tillganglighet/nivaer-i-wcag",
  "tag": "page-nivaer-i-wcag"
 },
 {
  "route": "/tillganglighet/process-for-tillganglighetsredogorelse/externa-webbplatser",
  "tag": "page-externa-webbplatser"
 },
 {
  "route": "/tillganglighet/process-for-tillganglighetsredogorelse",
  "tag": "page-process-for-tillganglighetsredogorelse"
 },
 {
  "route": "/tillganglighet/process-for-tillganglighetsredogorelse/interna-webbplatser",
  "tag": "page-interna-webbplatser"
 },
 {
  "route": "/tillganglighet/process-for-tillganglighetsredogorelse/lista-med-tillganglighetsbrister",
  "tag": "page-lista-med-tillganglighetsbrister"
 },
 {
  "route": "/tillganglighet/tillganglighetslista",
  "tag": "page-tillganglighetslista"
 },
 {
  "route": "/tillganglighetsredogorelse",
  "tag": "page-tillganglighetsredogorelse"
 }
]