import { ArticlePage } from '@digi/skolverket-docs/components/ArticlePage';
import { ComponentLink } from '@digi/skolverket-docs/components/ComponentLink';
import { Component, getAssetPath, h } from '@stencil/core';

@Component({
	tag: 'page-illustrationer',
	scoped: true
})
export class PageIllustrationer {
	render() {
		return (
			<ArticlePage
				head={<title>Illustrationer, diagram och tabeller</title>}
				heading="Illustrationer, diagram och tabeller"
				preamble="Illustrationer, diagram och tabeller kan hjälpa till att förklara komplexa system och förhållanden på ett mer inspirerande vis."
			>
				<h2>Illustrationer</h2>
				<p>
					Med dem kan vi ge kontext och sammanhang men även smycka och de är även
					lättare att anpassa för olika format och sammanhang. Mer detaljerade
					riktlinjer för illustrationer finns i den grafiska manualen.
				</p>
				<p>
					<digi-media-image
						afSrc={getAssetPath('/assets/images/illustrationer-exempel.jpg')}
						afAlt={'Illustrationsexempel med människor utomhus och kontorsmaterial'}
						afWidth={'674'}
						afHeight={'197'}
					/>
				</p>
				<h3>Principer för våra illustrationer</h3>
				<p>
					Illustrationerna ska ha ett enkelt och informativt formspråk, som är
					tydligt och lätt att ta till sig och som följer Skolverkets
					illustrationsmanér.
				</p>
				<ul>
					<li>Utgå från ett enkelt och informativt formspråk</li>
					<li>Använd Skolverkets färger i illustrationer</li>
					<li>Eftersträva ett enkelt och informativt formspråk</li>
					<li>Färger kan kompletteras vid mer komplexa illustrationer</li>
				</ul>
				<h2>Tabeller</h2>
				<p>
					Tabeller kan användas för att på ett enkelt sätt presentera en
					sammanställning av data för läsaren. Men tabeller ska aldrig användas som
					ett dekorativt inslag eller som platshållare för text.
				</p>
				<p>
					<digi-notification-detail>
						<h3 slot="heading">Riktlinjer för tabeller</h3>
						<ul>
							<li>
								Använd bara tabeller för innehåll som behöver presenteras i en tabell.
							</li>
							<li>
								Tabellen ska vara enkel. Om din tabell är komplex med flera nivåer bör
								du dela upp tabellen i flera tabeller.
							</li>
							<li>Tabeller ska vara tillgängliga och responsiva.</li>
						</ul>
					</digi-notification-detail>
				</p>
				<h3>Därför ska tabeller vara tillgängliga och responsiva</h3>
				<h4>Tillgänglig tabell</h4>
				<p>
					Tabellen ska kunna läsas och förstås av personer med funktionsnedsättning
					och som använder exempelvis skärmläsare. Tabellen måste därför korrekt
					märkas upp vad som är rubriker och dataceller.
				</p>
				<h4>Responsiv tabell</h4>
				<p>
					Tabellen ska kunna läsas på olika plattformar - såsom dataskärm, surfplatta
					eller smarta telefoner. Beroende på din tabells storlek kan du behöva
					justera hur tabellen ska se ut på olika plattformar.
				</p>
				<p>
					<ComponentLink
						tag="digi-table"
						text="Se tabellkomponenten för användning på webben"
					/>
				</p>
				<h2>Mer om illustrationer, diagram och tabeller</h2>
				<p>
					Läs mer om riktlinjer för illustrationer och hur du skapar tabeller i våra
					olika manualer.
				</p>
				<digi-list-link>
					<li>
						<a href="#">Skapa tabeller</a>
					</li>
					<li>
						<a href="#">Grafisk manual</a>
					</li>
				</digi-list-link>
			</ArticlePage>
		);
	}
}
