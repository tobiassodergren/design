import { Component, getAssetPath, h, Host } from '@stencil/core';
import { href } from 'stencil-router-v2';
import { router } from '../global/routes/router';

@Component({
	tag: 'page-home',
	scoped: true
})
export class PageHome {
	render() {
		console.log(router.activePath);
		return (
			<Host>
				<digi-page-block-hero af-variation="section">
					<h1 slot="heading">
						Användbara och tillgängliga tjänster för den svenska skolan
					</h1>
					<digi-typography-preamble>
						Skolverkets designsystem innehåller teknik, verktyg och riktlinjer för
						myndighetens digitala tjänster och möjliggör enhetlighet och effektivitet
						i utvecklingsarbetet. Innehållet tas fram i löpande samverkan med andra
						myndigheter och bygger på gemensamma insikter, forskning och
						användarbehov.
						<span style={{ display: 'block', 'margin-block-start': '1em' }}>
							Tillsammans skapar vi bättre tjänster för den svenska skolan.
						</span>
					</digi-typography-preamble>

					<digi-list-link>
						<li>
							<a {...href('/kom-igang')}>Kom igång</a>
						</li>
					</digi-list-link>
				</digi-page-block-hero>
				<digi-page-block-cards>
					<h2 slot="heading">Välj din väg</h2>
					<digi-card-link>
						<img
							slot="image"
							src={getAssetPath(
								'/assets/images/startsida_puffbild_komponentbibliotek-min.jpg'
							)}
							width="704"
							height="396"
							alt="Flicka som sitter vid en dator"
						/>
						<h3 slot="link-heading">
							<a {...href('/komponenter')}>Komponentbibliotek</a>
						</h3>
						<p>
							Få en snabb överblick över designsystemets byggstenar och läs vägledning
							och dokumentation om hur du använder dem i utvecklingen av webb och andra
							digitala tjänster.
						</p>
					</digi-card-link>
					<digi-card-link>
						<img
							slot="image"
							src={getAssetPath(
								'/assets/images/startsida_puffbild_grafisk_profil.png'
							)}
							width="704"
							height="396"
							alt="Skolverkets logotyp mot grafisk bakgrund"
						/>
						<h3 slot="link-heading">
							<a {...href('/grafisk-profil')}>Grafisk profil</a>
						</h3>
						<p>
							Få en beskrivning av Skolverkets visuella design och uttryck för digitala
							medier. Läs om hur vi använder logotyp, bilder, typsnitt och färger med
							mera. På ett korrekt sätt.
						</p>
					</digi-card-link>
					<digi-card-link>
						<img
							slot="image"
							src={getAssetPath(
								'/assets/images/startsida_puffbild_tillganglighet-min.jpg'
							)}
							width="704"
							height="396"
							alt="Barn står på en bänk för att se över ett staket"
						/>
						<h3 slot="link-heading">
							<a {...href('/tillganglighet')}>Digital tillgänglighet</a>
						</h3>
						<p>
							Vägledning och stöd som hjälper oss att hjälpa våra användare att förstå
							och interagera med våra användargränssnitt. Med utgångspunkt i universell
							utformning och webbriktlinjer.
						</p>
					</digi-card-link>
				</digi-page-block-cards>
			</Host>
		);
	}
}
