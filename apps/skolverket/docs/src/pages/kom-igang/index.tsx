import { ArticlePage } from '@digi/skolverket-docs/components/ArticlePage';
import { Component, h } from '@stencil/core';
import { href } from 'stencil-router-v2';

@Component({
	tag: 'page-kom-igang',
	scoped: true
})
export class PageKomIgang {
	render() {
		return (
			<ArticlePage
				head={<title>Kom igång</title>}
				heading="Kom igång"
				preamble="Designsystemet består av två delar. Den viktigaste delen är alla vi, människorna, som delar en gemensam filosofi om att vi vill samarbeta och tillsammans ta fram bästa tänkbara tjänster och produkter för våra användare."
			>
				<p>
					Den andra delen är hur vi ska göra detta. Själva verktygslådan.
					Designsystemet som byggsten samlar återanvändbara verktyg, processer och
					riktlinjer på ett ställe. När vi både har en gemensam samarbetsmodell och
					konkret verktygslåda kan vi alla bidra till att hela tiden förbättra både
					designsystemet och våra produkter.
				</p>
				<p>
					Vi kan känna oss trygga i att det vi tar fram lever upp till lagkrav och
					varumärkesriktlinjer. Det i sin tur ger våra användare en mer sammanhållen
					och tillgänglig upplevelse så de kan känna sig trygga i att de får utföra
					sitt ärende på ett bra och säkert sätt.
				</p>
				<h2>Det här är designsystemet</h2>
				<p>
					Skolverkets designsystem består av de här delarna:
					<ul>
						<li>
							<strong>En väletablerad samarbetsmodell:</strong> Vi ska utveckla
							designsystemet tillsammans och för detta arbetar vi fram en
							samarbetsmodell som gör det enkelt att både komma igång och hitta sätt
							att bidra in till designsystemet.
						</li>
						<li>
							<strong>Designmönster:</strong> Design- och interaktionsmönster för hur
							gränssnitt bör utformas för att lösa specifika behov.
						</li>
						<li>
							<strong>Designprinciper:</strong> Ramverk kring hur en designer ska och
							inte ska göra i olika situationer.
						</li>
						<li>
							<strong>Grafisk profil:</strong> Verktyg och riktlinjer för hur du jobbar
							med Skolverkets varumärke och grafiska profil.
						</li>
						<li>
							<strong>Komponentbibliotek:</strong> Komponentbiblioteket är uppdelat i
							design (Adobe XD) och kod (Webbkomponenter via NPM).
						</li>
						<li>
							<strong>Språk och översättningar:</strong> Riktlinjer för hur vi
							förhåller oss och arbetar med andra språk än svenska.
						</li>
						<li>
							<strong>Praktisk copy:</strong> Riktlinjer för systemtexter och
							mikrocopy.
						</li>
						<li>
							<strong>Tillgänglighetsriktlinjer:</strong> Kunskapsstöd för hur
							produkter skapar lösningar för alla.
						</li>
						<li>
							<strong>Dokumentationsplattform:</strong> Designsystemets webbplats och
							dokumentationsplattform på{' '}
							<a href="https://designsystem.skolverket.se">
								designsystem.skolverket.se
							</a>
						</li>
					</ul>
				</p>
				<h2>Hur kommer jag igång?</h2>
				<digi-list-link>
					<li>
						<a {...href('/kom-igang/jobba-med-kod')}>Jobba med kod</a>
					</li>
					<li>
						<a {...href('/kom-igang/jobba-med-design')}>Jobba med design</a>
					</li>
				</digi-list-link>
			</ArticlePage>
		);
	}
}
