import { ArticlePage } from '@digi/skolverket-docs/components/ArticlePage';
import { Component, getAssetPath, h } from '@stencil/core';
import { href } from 'stencil-router-v2';

@Component({
	tag: 'page-jobba-med-design',
	scoped: true,
	styles: `a:not(:last-of-type) { display: block; }`
})
export class PageJobbaMedDesign {
	render() {
		return (
			<ArticlePage
				head={<title>Jobba med design</title>}
				heading="Jobba med design"
				preamble="All gränssnittsdesign finns beskriven i designsystemets designfiler och vår grafiska profil."
			>
				<h2>Designfiler</h2>
				<p>
					På Skolverket använder vi Adobe XD för att ta fram skisser och prototyper.
				</p>
				<p>
					<a
						href={getAssetPath('/assets/documents/designsystem-skolverket-220629.xd')}
						download="designsystem-skolverket-220629"
					>
						Skolverkets Adobe XD-fil för designsystemet (.xd)
					</a>
					<a
						href={getAssetPath(
							'/assets/documents/designsystem-skolverket-220629.pdf'
						)}
						download="designsystem-skolverket-220629"
					>
						Samma fil i PDF-format (.pdf)
					</a>
				</p>
				<h2>Grafisk profil</h2>
				<p>
					Det mesta finns beskrivet på{' '}
					<a {...href('/grafisk-profil')}>sidan om vår grafiska profil</a>.
				</p>
			</ArticlePage>
		);
	}
}
