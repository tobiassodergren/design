import { Component, h } from '@stencil/core';
import { Category } from '@digi/taxonomies';
import { CategoryPage } from '@digi/skolverket-docs/components/CategoryPage';

@Component({
	tag: 'page-category-formular',
	scoped: true
})
export class PageCategoryFormular {
	render() {
		return (
			<CategoryPage
				category={Category.FORM}
				preamble="Inmatningsfält, etiketter, processer och annat som används för att skapa
      formulär."
			></CategoryPage>
		);
	}
}
