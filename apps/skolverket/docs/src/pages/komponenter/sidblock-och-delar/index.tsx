import { Component, h } from '@stencil/core';
import { Category } from '@digi/taxonomies';
import { CategoryPage } from '@digi/skolverket-docs/components/CategoryPage';
import { href } from 'stencil-router-v2';

@Component({
	tag: 'page-category-sidblock-och-delar',
	scoped: true
})
export class PageCategorySidbockOchDelar {
	render() {
		return (
			<CategoryPage
				category={Category.PAGE}
				preamble="Sidlayouter, sidhuvud, sidfot, fullbreddsblock och annat för att snabbt skapa sidvyer enligt Skolverkets designmönster och riktlinjer."
			>
				<p>
					<digi-notification-detail>
						<h2 slot="heading">Hittar du inte det du behöver?</h2>
						<p>
							Mer grundläggande layoutkomponenter, som kolumner och rader, hittar du i{' '}
							<a {...href('/komponenter/layout')}>kategorin för layout</a>.
						</p>
					</digi-notification-detail>
				</p>
			</CategoryPage>
		);
	}
}
