# digi-badge-status



<!-- Auto Generated Below -->


## Properties

| Property              | Attribute       | Description                                      | Type                                                                                                                                                         | Default                        |
| --------------------- | --------------- | ------------------------------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------ | ------------------------------ |
| `afAriaLabel`         | `af-aria-label` | Sätter attributet 'aria-label'.                  | `string`                                                                                                                                                     | `undefined`                    |
| `afText` _(required)_ | `af-text`       | Sätter text i statusindikatorn.                  | `string`                                                                                                                                                     | `undefined`                    |
| `afType`              | `af-type`       | Sätter attributet 'type'.                        | `BadgeStatusType.APPROVED \| BadgeStatusType.BETA \| BadgeStatusType.DENIED \| BadgeStatusType.MISSING \| BadgeStatusType.NEUTRAL \| BadgeStatusType.PROMPT` | `BadgeStatusType.NEUTRAL`      |
| `afVariation`         | `af-variation`  | Sätter variation. Kan vara primär eller sekundär | `BadgeStatusVariation.PRIMARY \| BadgeStatusVariation.SECONDARY`                                                                                             | `BadgeStatusVariation.PRIMARY` |


## CSS Custom Properties

| Name                                                          | Description                                                                                     |
| ------------------------------------------------------------- | ----------------------------------------------------------------------------------------------- |
| `--digi--badge-status--background-color--approved--primary`   | var(--digi--global--color--function--success--dark);                                            |
| `--digi--badge-status--background-color--approved--secondary` | var(--digi--global--color--function--success--lightest-2);                                      |
| `--digi--badge-status--background-color--beta--primary`       | var(--digi--color--background--complementary-2);                                                |
| `--digi--badge-status--background-color--beta--secondary`     | var(--digi--global--color--neutral--grayscale--lightest);                                       |
| `--digi--badge-status--background-color--denied--primary`     | var(--digi--global--color--function--danger--dark);                                             |
| `--digi--badge-status--background-color--denied--secondary`   | var(--digi--color--background--danger-2);                                                       |
| `--digi--badge-status--background-color--missing--primary`    | var(--digi--global--color--function--warning--dark);                                            |
| `--digi--badge-status--background-color--missing--secondary`  | var(--digi--global--color--function--warning--lightest);                                        |
| `--digi--badge-status--background-color--neutral--primary`    | var(--digi--color--background--neutral-4);                                                      |
| `--digi--badge-status--background-color--neutral--secondary`  | var(--digi--global--color--neutral--grayscale--lightest);                                       |
| `--digi--badge-status--background-color--prompt--primary`     | var(--digi--color--background--inverted-6);                                                     |
| `--digi--badge-status--background-color--prompt--secondary`   | var(--digi--global--color--function--info--lightest-3);                                         |
| `--digi--badge-status--border--approved--secondary`           | var(--digi--border-width--secondary) solid var(--digi--color--background--success-1);           |
| `--digi--badge-status--border--beta--secondary`               | var(--digi--border-width--secondary) solid var(--digi--color--background--complementary-2);     |
| `--digi--badge-status--border--denied--secondary`             | var(--digi--border-width--secondary) solid var(--digi--global--color--function--danger--dark);  |
| `--digi--badge-status--border--missing--secondary`            | var(--digi--border-width--secondary) solid var(--digi--global--color--function--warning--dark); |
| `--digi--badge-status--border--neutral--secondary`            | var(--digi--border-width--secondary) solid var(--digi--color--background--neutral-4);           |
| `--digi--badge-status--border--prompt--secondary`             | var(--digi--border-width--secondary) solid var(--digi--color--background--inverted-6);          |
| `--digi--badge-status--color--missing`                        | var(--digi--global--color--neutral--grayscale--darkest-6);                                      |
| `--digi--badge-status--color--primary`                        | var(--digi--color--text--inverted);                                                             |
| `--digi--badge-status--color--secondary`                      | var(--digi--global--color--neutral--grayscale--darkest-6);                                      |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
