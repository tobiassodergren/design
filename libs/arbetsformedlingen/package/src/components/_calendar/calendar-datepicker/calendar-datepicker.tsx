import {
	Component,
	h,
	Host,
	State,
	Event,
	EventEmitter,
	Element,
	Prop
} from '@stencil/core';
import { HTMLStencilElement, Watch } from '@stencil/core/internal';
import { isAfter, isBefore, isValid } from 'date-fns';
import { FormInputValidation } from 'libs/core/package/src/enums-core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { CalendarDatepickerValidation } from './calendar-datepicker-validation.enum';

/**
 * @swedishName Datumväljare
 */
@Component({
	tag: 'digi-calendar-datepicker',
	styleUrl: 'calendar-datepicker.scss',
	scoped: true
})
export class CalendarDatepicker {
	@Element() hostElement: HTMLStencilElement;

	@State() showWrongFormatError = false;
	@State() showDisabledDateError = false;
	@State() showCalendar = false;
	futureYears: number = 10;

	/**
	 * Sätter ett id attribut.
	 * @en Sets an id attribute. Standard är slumpmässig sträng.
	 */
	@Prop() afId: string = randomIdGenerator('digi-calendar-datepicker');

	/**
	 * Texten till labelelementet
	 * @en The label text
	 */
	@Prop() afLabel: string = 'Skriv in eller välj datum (åååå-mm-dd)';

	/**
	 * Valfri beskrivande text
	 * @en A description text
	 */
	@Prop() afLabelDescription: string = 'Exempel: 1992-06-26';

	/**
	 * Sätter valideringsstatus. Kan vara 'success', 'error', 'warning', 'neutral' eller ingenting.
	 * @en Set input validation status
	 */
	@Prop() afValidation: CalendarDatepickerValidation =
		CalendarDatepickerValidation.NEUTRAL;

	/**
	 * Sätter valideringsmeddelandet
	 * @en Set input validation message
	 */
	@Prop() afValidationText: string;

	/**
	 * Sätter valideringsmeddelandet när du använder afInvalid
	 * @en Set input validation message when using afInvalid property
	 */
	@Prop() afInvalid: boolean = false;

	/**
	 * Sätter valideringsmeddelandet när du använder afInvalid
	 * @en Set input validation message when using afInvalid property
	 */
	@Prop() afValidationMessage: string = 'Eget meddelande';

	/**
	 * Sätter valideringsmeddelandet
	 * @en Set input validation message
	 */
	@Prop() afValidationWrongFormat: string = 'Fel format';

	/**
	 * Sätter valideringsmeddelandet
	 * @en Set input validation message
	 */
	@Prop() afValidationDisabledDate: string = 'Ej valbart datum';

	/**
	 * Sätter attributet 'required'.
	 * @en Input required attribute
	 */
	@Prop() afRequired: boolean;

	/**
	 * Sätter text för afRequired.
	 * @en Set text for afRequired.
	 */

	@Prop() afRequiredText: string;

	/**
	 * Valda datum i kalendern
	 * @en Selected dates in calendar
	 */
	@Prop() afSelectedDates: Array<Date> = [];

	/**
	 * Sätter attributet 'value'
	 * @en Input value attribute
	 * @ignore
	 */
	@Prop() value: Array<Date> = [];

	/**
	 * Tidigaste valbara datumet
	 * @en Earliest selectable date
	 */
	@Prop() afMinDate: Date;

	/**
	 * Senaste valbara datumet
	 * @en Latest selectable date
	 */
	@Prop() afMaxDate: Date;

	/**
	 * Visa veckonummer i kalender. Förvalt är false
	 * @en Show week number in the calander. Default is false
	 */
	@Prop() afShowWeekNumber: boolean = false;

	/**
	 * Sätt denna till true för att kunna markera mer en ett datum i kalendern. Förvalt är false
	 * @en Set this to true to be able to select multiple dates in the calander. Default is false
	 */
	@Prop() afMultipleDates: boolean = false;
	@Watch('afMultipleDates')
	afMultipleDatesChangeHandler() {
		if (!this.afMultipleDates && this.afSelectedDates.length > 1) {
			this.afSelectedDates = this.afSelectedDates.slice(-1);
			this.value = this.afSelectedDates ?? [];
		}
	}

	/**
	 * Sätt denna till true om kalendern ska stängas när användaren väljer ett datum. Förvalt är false
	 * @en Set this to true if the calendar should close when a date is selected. Default is false
	 */
	@Prop() afCloseOnSelect: boolean = false;

	/**
	 * Sker vid datum uppdatering
	 * @en When date is updated
	 */
	@Event() afOnDateChange: EventEmitter;

	componentWillLoad() {}
	componentDidLoad() {}
	componentWillUpdate() {}

	get cssModifiers() {
		return {
			'digi-calendar-datepicker__show-week-numbers': this.afShowWeekNumber,
			'digi-calendar-datepicker__show-error': this.getShowError(),
			'digi-calendar-datepicker__show-calendar': this.showCalendar
		};
	}

	toggleCalendar() {
		this.showCalendar = !this.showCalendar;
	}

	parseInput(evt) {
		if (evt.target.value == '') {
			this.showWrongFormatError = false;
			this.showDisabledDateError = false;
			this.afSelectedDates = [];
			this.value = [];
			this.afOnDateChange.emit([]);
		} else {
			const parsedInput = evt.target.value
				.split(/[&,]|och/g)
				.map((text) => this.parseDateString(text.trim()));
			this.showWrongFormatError = !!parsedInput.find((date) => !isValid(date));
			this.showDisabledDateError = !!parsedInput.find(
				(date) => !this.isDateSelectable(date)
			);
			if (
				!this.getShowError() &&
				!this.areSameDates(this.afSelectedDates, parsedInput)
			) {
				this.afSelectedDates = this.filterDates(parsedInput);
				this.value = this.afSelectedDates ?? [];
				this.afOnDateChange.emit(this.afSelectedDates ?? []);
			}
		}
	}
	parseCalendar(evt) {
		console.log('parseCalendar');
		console.log(evt);
		if (
			evt &&
			evt.detail &&
			!this.areSameDates(this.afSelectedDates, evt.detail)
		) {
			this.afSelectedDates = evt.detail ?? [];
			this.value = this.afSelectedDates ?? [];
			this.showWrongFormatError = false;
			this.showDisabledDateError = false;
			this.afOnDateChange.emit(this.afSelectedDates ?? []);
			if (this.afCloseOnSelect) {
				this.toggleCalendar();
			}
		}
	}
	parseDateString(s: string): Date {
		let date: Date = new Date(s),
			future: Date = new Date();
		future.setFullYear(future.getFullYear() + this.futureYears);
		if (!isValid(date) || date > future)
			date = new Date(
				this.getFullYear(s.slice(0, -4)) + '-' + s.slice(-4, -2) + '-' + s.slice(-2)
			);
		return date;
	}
	getFullYear(s: string): string {
		if (s.length === 4) return s;
		const y = new Date().getFullYear().toString();
		return parseInt(s) > parseInt(y.slice(-2)) + this.futureYears
			? parseInt(y.slice(0, -2)) - 1 + s
			: y.slice(0, -2) + s;
	}
	filterDates(dates: Array<Date>): Array<Date> {
		if (!this.afMultipleDates && dates.length > 1) return dates.slice(-1);
		return dates.filter((date, index) => this.indexOf(date, dates) === index);
	}
	indexOf(date: Date, dates: Array<Date>): number {
		const dateString = this.getDateString(date);
		for (let i = 0; i < dates.length; i++) {
			if (dateString === this.getDateString(dates[i])) {
				return i;
			}
		}
		return -1;
	}
	areSameDates(a: Array<Date>, b: Array<Date>) {
		return this.getDatesString(a) === this.getDatesString(b);
	}
	getDatesString(dates: Array<Date>): string {
		return dates.map((date) => this.getDateString(date)).join('');
	}
	getDateString(date: Date): string {
		return `${date.getDate()}${date.getMonth()}${date.getFullYear()}`;
	}
	isDateSelectable(date: Date): boolean {
		if (this.afMinDate && this.isBefore(date, this.afMinDate)) return false;
		if (this.afMaxDate && this.isAfter(date, this.afMaxDate)) return false;
		return true;
	}
	isBefore(a: Date, b: Date): boolean {
		return isBefore(this.dateWithoutTime(a), this.dateWithoutTime(b));
	}
	isAfter(a: Date, b: Date): boolean {
		return isAfter(this.dateWithoutTime(a), this.dateWithoutTime(b));
	}
	dateWithoutTime(date: Date): Date {
		return new Date(date.getFullYear(), date.getMonth(), date.getDate());
	}

	getShowError(): boolean {
		return (
			this.afInvalid ||
			this.showWrongFormatError ||
			this.showDisabledDateError
		);
	}
	getErrorText(): string {
		if (this.afInvalid) return this.afValidationText ?? this.afValidationMessage;
		return this.showWrongFormatError
			? this.afValidationWrongFormat
			: this.afValidationDisabledDate;			
	}

	get inputValue(): string {
		if (this.afMultipleDates)
			return this.datesToInputString(
				this.afSelectedDates.sort((a: Date, b: Date) =>
					this.isBefore(a, b) ? -1 : 1
				)
			);
		if (this.afSelectedDates.length > 0)
			return this.afSelectedDates[
				this.afSelectedDates.length - 1
			].toLocaleDateString('sv-SE');
		return '';
	}
	datesToInputString(dates: Array<Date>): string {
		if (dates.length <= 2)
			return dates
				.map((date: Date) => date.toLocaleDateString('sv-SE'))
				.join(' och ');
		return dates
			.map((date: Date) => date.toLocaleDateString('sv-SE'))
			.join(', ')
			.replace(/, ([^,]*)$/, ' och $1');
	}

	render() {
		return (
			<Host>
				<div
					class={{
						'digi-calendar-datepicker': true,
						...this.cssModifiers
					}}
				>
					<div class="digi-calendar-datepicker__input-field">
						<digi-form-input
							class={'digi-calendar-datepicker__input-component'}
							afLabel={this.afLabel}
							afLabelDescription={this.afLabelDescription}
							afValue={this.inputValue}
							afValidation={
								this.getShowError()
									? FormInputValidation.ERROR
									: FormInputValidation.NEUTRAL
							}
							afValidationText={this.getErrorText()}
							afId={this.afId}
							afAriaDescribedby={`${this.afId}--validation-message`}
							afRequired={this.afRequired}
							afRequiredText={this.afRequiredText}
							onAfOnChange={(evt) => {
								evt.stopImmediatePropagation();
								this.parseInput(evt);
							}}
						>
							<button
								class="digi-calendar-datepicker__input-button"
								slot="button"
								aria-label="Visa eller dölj kalender"
								aria-expanded={this.showCalendar ? 'true' : 'false'}
								aria-haspopup={true}
								onClick={() => this.toggleCalendar()}
								type="button"
							>
								<digi-icon
									class="digi-calendar-datepicker__button-icon"
									afName={'calendar-alt'}
									slot="icon"
								/>
							</button>
						</digi-form-input>
					</div>
					<div class="digi-calendar-datepicker__calendar">
						<digi-calendar
							afActive={this.showCalendar}
							afShowWeekNumber={this.afShowWeekNumber}
							afMultipleDates={this.afMultipleDates}
							afSelectedDates={this.getShowError() ? [] : this.afSelectedDates}
							afMinDate={this.afMinDate}
							afMaxDate={this.afMaxDate}
							aria-hidden={!this.showCalendar}
							onAfOnDateSelectedChange={(evt) => {
								evt.stopImmediatePropagation();
								this.parseCalendar(evt);
							}}
						/>
					</div>
				</div>
			</Host>
		);
	}
}
