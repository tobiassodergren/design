# digi-footer-card

<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description                                                                                               | Type                                                                                 | Default                    |
| -------- | --------- | --------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------ | -------------------------- |
| `afType` | `af-type` | Sätter innehållstyp för footerkortet, så som länklista med, utan ikon eller om kortet ska ha en kantlinje | `FooterCardVariation.BORDER \| FooterCardVariation.ICON \| FooterCardVariation.NONE` | `FooterCardVariation.NONE` |


## Slots

| Slot       | Description              |
| ---------- | ------------------------ |
| `"mySlot"` | Slot description, if any |


## CSS Custom Properties

| Name                                     | Description                                                                      |
| ---------------------------------------- | -------------------------------------------------------------------------------- |
| `--digi--footer-card--border-bottom`     | solid var(--digi--border-width--primary) var(--digi--color--border--inverted-3); |
| `--digi--footer-card--link--font-weight` | var(--digi--typography--link-bold--font-weight--desktop);                        |
| `--digi--footer-card--text--color`       | var(--digi--color--text--inverted);                                              |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
