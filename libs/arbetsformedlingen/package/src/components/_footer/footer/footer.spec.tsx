import { newSpecPage } from '@stencil/core/testing';
import { FooterFooter } from './footer';

describe('footer', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [FooterFooter],
			html: '<footer></footer>'
		});
		expect(root).toEqualHtml(`
      <footer>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </footer>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [FooterFooter],
			html: `<footer first="Stencil" last="'Don't call me a framework' JS"></footer>`
		});
		expect(root).toEqualHtml(`
      <footer first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </footer>
    `);
	});
});
