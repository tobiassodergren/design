import {
	Component,
	h,
	Host,
	Element,
	Prop
} from '@stencil/core';
import { HTMLStencilElement } from '@stencil/core/internal';
import { FooterVariation } from './footer-variation.enum';
import { LayoutBlockVariation } from '../../../enums-core';

/**
 * @slot content-top - Ska innehålla en div per kolumn, i dessa används <digi-footer-card /> för informationen.
 *
 * @slot content-bottom-left - Ska innehålla länk och logotyp
 * 
 * @slot content-bottom-right - Ska innehålla länkar till sociala medier.
 * 
 * @enums FooterVariation - footer-variation.enum.ts
 *
 * @swedishName Sidfot
 */
@Component({
	tag: 'digi-footer',
	styleUrl: 'footer.scss',
	scoped: true
})
export class FooterFooter {
	@Element() hostElement: HTMLStencilElement;

	
  /**
   * Sätter variation. Kan vara stor eller liten
   * @en Set variation. Can be 'large' or 'small'
   */
  @Prop() afVariation: FooterVariation = FooterVariation.LARGE;

	get cssModifiers() {
		return {
			'digi-footer--small': this.afVariation == FooterVariation.SMALL,
		};
	}

	render() {
		return (
			<Host>
				<footer
					class={{
						'digi-footer': true,
						...this.cssModifiers
					}}
				>
          <digi-typography>
            <digi-layout-block afVariation={LayoutBlockVariation.PROFILE}>
              <digi-layout-container af-no-gutter>
                <div class="digi-footer__content-top">
                  <slot name="content-top" />
                </div>
                <div class="digi-footer__content-bottom">
                  <slot name="content-bottom-left" />
                  <slot name="content-bottom-right" />
                </div>
              </digi-layout-container>
            </digi-layout-block>
          </digi-typography>
				</footer>
			</Host>
		);
	}
}