# digi-footer

<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description                                 | Type                                             | Default                 |
| ------------- | -------------- | ------------------------------------------- | ------------------------------------------------ | ----------------------- |
| `afVariation` | `af-variation` | Sätter variation. Kan vara stor eller liten | `FooterVariation.LARGE \| FooterVariation.SMALL` | `FooterVariation.LARGE` |


## Slots

| Slot                     | Description                                                                              |
| ------------------------ | ---------------------------------------------------------------------------------------- |
| `"content-bottom-left"`  | Ska innehålla länk och logotyp                                                           |
| `"content-bottom-right"` | Ska innehålla länkar till sociala medier.                                                |
| `"content-top"`          | Ska innehålla en div per kolumn, i dessa används <digi-footer-card /> för informationen. |


## CSS Custom Properties

| Name                                          | Description                                                 |
| --------------------------------------------- | ----------------------------------------------------------- |
| `--digi--footer--link--border-color--default` | var(--digi--color--text--inverted);                         |
| `--digi--footer--link--border-color--hover`   | var(--digi--color--text--inverted);                         |
| `--digi--footer--link--color--default`        | var(--digi--color--text--inverted);                         |
| `--digi--footer--link--color--hover`          | var(--digi--color--text--inverted);                         |
| `--digi--footer--margin-top`                  | var(--digi--container-gutter--large);                       |
| `--digi--footer--padding-top`                 | var(--digi--container-gutter--largest);                     |
| `--digi--footer--text--color`                 | var(--digi--color--text--inverted);                         |
| `--digi--footer--text--font-family`           | var(--digi--global--typography--font-family--default);      |
| `--digi--footer--text--font-size`             | var(--digi--typography--body--font-size--desktop);          |
| `--digi--footer--text--font-weight`           | var(--digi--typography--description--font-weight--desktop); |


## Dependencies

### Depends on

- [digi-typography](../../../__core/_typography/typography)
- [digi-layout-block](../../../__core/_layout/layout-block)
- [digi-layout-container](../../../__core/_layout/layout-container)

### Graph
```mermaid
graph TD;
  digi-footer --> digi-typography
  digi-footer --> digi-layout-block
  digi-footer --> digi-layout-container
  digi-layout-block --> digi-layout-container
  style digi-footer fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
