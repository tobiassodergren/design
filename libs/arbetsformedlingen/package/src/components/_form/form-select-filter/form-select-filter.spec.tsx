import { newSpecPage } from '@stencil/core/testing';
import { FormSelectFilter } from './form-select-filter';

describe('form-select-filter', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [FormSelectFilter],
			html: '<form-select-filter></form-select-filter>'
		});
		expect(root).toEqualHtml(`
      <form-select-filter>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </form-select-filter>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [FormSelectFilter],
			html: `<form-select-filter first="Stencil" last="'Don't call me a framework' JS"></form-select-filter>`
		});
		expect(root).toEqualHtml(`
      <form-select-filter first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </form-select-filter>
    `);
	});
});
