# form-select-filter

Istället för slots skickar man in array med IListItem, som blandannat har label och value.
Endast label är obligatoriskt. Även stöd för att sätta något som förvalt (selected) finns.
Utöver det finns lang & dir, om man vill använda fälten för icke europeiskt alfabet.

När man börjar fylla i sökord filtreras datakällan på någon form av fuzzy search matchning. 


Events
afOnQueryUpdated    Skickar tillbaka event med sträng på vad användaren har skrivit in. 
                    I detta event kan man göra en request mot backend och uppdatera datakällan

afOnSelect          Event som skickar tillbaka IListItem objektet som är valt



<!-- Auto Generated Below -->


## Properties

| Property                               | Attribute                               | Description                                                                                                                                                                                                                              | Type                                                                                                                                                 | Default                                        |
| -------------------------------------- | --------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------- |
| `afAlignRight`                         | `af-align-right`                        | Justera dropdown från höger till vänster                                                                                                                                                                                                 | `boolean`                                                                                                                                            | `undefined`                                    |
| `afAnnounceIfOptional`                 | `af-announce-if-optional`               | Normalt betonas ett obligatoriskt fält visuellt och semantiskt. Detta vänder på det och betonar fältet om det inte krävs. Ställ in detta till sant om ditt formulär innehåller fler obligatoriska fält än inte.                          | `boolean`                                                                                                                                            | `false`                                        |
| `afAnnounceIfOptionalText`             | `af-announce-if-optional-text`          | Sätter text för afAnnounceIfOptional.                                                                                                                                                                                                    | `string`                                                                                                                                             | `undefined`                                    |
| `afDescription`                        | `af-description`                        | En valfri beskrivningstext.                                                                                                                                                                                                              | `string`                                                                                                                                             | `undefined`                                    |
| `afDisableValidation`                  | `af-disable-validation`                 | Inaktiverar valideringen                                                                                                                                                                                                                 | `boolean`                                                                                                                                            | `undefined`                                    |
| `afFilterButtonAriaDescribedby`        | `af-filter-button-aria-describedby`     | Sätter attributet 'aria-describedby' på filterknappen                                                                                                                                                                                    | `string`                                                                                                                                             | `undefined`                                    |
| `afFilterButtonText` _(required)_      | `af-filter-button-text`                 | Texten i filterknappen                                                                                                                                                                                                                   | `string`                                                                                                                                             | `undefined`                                    |
| `afFilterButtonTextLabel` _(required)_ | `af-filter-button-text-label`           | Texten till label för knappen                                                                                                                                                                                                            | `string`                                                                                                                                             | `undefined`                                    |
| `afFilterButtonTextMultipleValues`     | `af-filter-button-text-multiple-values` | Texten som visas när man gjort fler än ett val. Använd {count} för att läsa ut antalet val                                                                                                                                               | `string`                                                                                                                                             | `undefined`                                    |
| `afHideResetButton`                    | `af-hide-reset-button`                  | Döljer rensaknappen när värdet sätts till true                                                                                                                                                                                           | `boolean`                                                                                                                                            | `false`                                        |
| `afId`                                 | `af-id`                                 | Prefixet för alla komponentens olika interna 'id'-attribut på dropdowns, filterlistor etc. Ex. 'my-cool-id' genererar 'my-cool-id-filter-list', 'my-cool-id-dropdown-menu' och så vidare. Ges inget värde så genereras ett slumpmässigt. | `string`                                                                                                                                             | `randomIdGenerator('digi-form-select-filter')` |
| `afListItems`                          | `af-list-items`                         | Lista med val                                                                                                                                                                                                                            | `IListItem[] \| string`                                                                                                                              | `undefined`                                    |
| `afMultipleItems`                      | `af-multiple-items`                     |                                                                                                                                                                                                                                          | `boolean`                                                                                                                                            | `false`                                        |
| `afName`                               | `af-name`                               | Namnet på filterlistans valda filter vid submit. Används även av legendelementet inne i filterlistan.                                                                                                                                    | `string`                                                                                                                                             | `undefined`                                    |
| `afRequired`                           | `af-required`                           | Är det ihopkopplade fältet tvingat?                                                                                                                                                                                                      | `boolean`                                                                                                                                            | `undefined`                                    |
| `afRequiredText`                       | `af-required-text`                      | Sätter text för afRequired.                                                                                                                                                                                                              | `string`                                                                                                                                             | `undefined`                                    |
| `afResetButtonText`                    | `af-reset-button-text`                  | Texten i rensaknappen                                                                                                                                                                                                                    | `string`                                                                                                                                             | `_t.clear`                                     |
| `afResetButtonTextAriaLabel`           | `af-reset-button-text-aria-label`       | Skärmläsartext för resetknappen                                                                                                                                                                                                          | `string`                                                                                                                                             | `undefined`                                    |
| `afSubmitButtonText`                   | `af-submit-button-text`                 | Texten i submitknappen                                                                                                                                                                                                                   | `string`                                                                                                                                             | `undefined`                                    |
| `afSubmitButtonTextAriaLabel`          | `af-submit-button-text-aria-label`      | Skärmläsartext för submitknappen                                                                                                                                                                                                         | `string`                                                                                                                                             | `undefined`                                    |
| `afValidation`                         | `af-validation`                         | Sätter väljarens validering                                                                                                                                                                                                              | `FormSelectFilterValidation.ERROR \| FormSelectFilterValidation.NEUTRAL \| FormSelectFilterValidation.SUCCESS \| FormSelectFilterValidation.WARNING` | `FormSelectFilterValidation.NEUTRAL`           |
| `afValidationText`                     | `af-validation-text`                    | Sätter valideringstexten.                                                                                                                                                                                                                | `string`                                                                                                                                             | `undefined`                                    |
| `value`                                | --                                      |                                                                                                                                                                                                                                          | `IListItem[]`                                                                                                                                        | `[]`                                           |


## Events

| Event               | Description                                             | Type                  |
| ------------------- | ------------------------------------------------------- | --------------------- |
| `afOnFilterClosed`  | När filtret stängs utan att valda alternativ bekräftats | `CustomEvent<any>`    |
| `afOnFocusout`      |                                                         | `CustomEvent<any>`    |
| `afOnQueryChanged`  | När en söksträng ändras                                 | `CustomEvent<string>` |
| `afOnResetFilters`  | Vid reset av filtret                                    | `CustomEvent<any>`    |
| `afOnSelect`        | När ett eller flera filter väljs                        | `CustomEvent<any[]>`  |
| `afOnSubmitFilters` | Vid uppdatering av filtret                              | `CustomEvent<any>`    |


## CSS Custom Properties

| Name                                                                     | Description                                                                     |
| ------------------------------------------------------------------------ | ------------------------------------------------------------------------------- |
| `--digi--form-select-filter--dropdown--background`                       | var(--digi--color--background--primary);                                        |
| `--digi--form-select-filter--dropdown--border-radius`                    | var(--digi--border-radius--input);                                              |
| `--digi--form-select-filter--dropdown--box-shadow`                       | 0 0.125rem 0.375rem 0 rgba(0, 0, 0, 0.7);                                       |
| `--digi--form-select-filter--dropdown--header--background`               | var(--digi--color--border--neutral-2);                                          |
| `--digi--form-select-filter--dropdown--item--background-color--active`   | var(--digi--color--background--neutral-6);                                      |
| `--digi--form-select-filter--dropdown--item--background-color--default`  | var(--digi--color--background--primary);                                        |
| `--digi--form-select-filter--dropdown--item--background-color--focus`    | var(--digi--color--background--neutral-6);                                      |
| `--digi--form-select-filter--dropdown--item--background-color--hover`    | var(--digi--color--background--neutral-6);                                      |
| `--digi--form-select-filter--dropdown--item--background-color--selected` | var(--digi--color--background--neutral-6);                                      |
| `--digi--form-select-filter--dropdown--item--border-color`               | var(--digi--color--border--neutral-2); //#CACACA i design                       |
| `--digi--form-select-filter--dropdown--item--border-type`                | var(--digi--border-style--primary);                                             |
| `--digi--form-select-filter--dropdown--item--border-width`               | var(--digi--border-width--primary);                                             |
| `--digi--form-select-filter--dropdown--item--checkbox--background-color` | var(--digi--color--background--primary);                                        |
| `--digi--form-select-filter--dropdown--item--checkbox--border-color`     | var(--digi--color--border--primary);                                            |
| `--digi--form-select-filter--dropdown--item--checkbox--border-radius`    | var(--digi--border-radius--tertiary);                                           |
| `--digi--form-select-filter--dropdown--item--checkbox--border-style`     | var(--digi--border-style--primary);                                             |
| `--digi--form-select-filter--dropdown--item--checkbox--border-width`     | var(--digi--border-width--primary);                                             |
| `--digi--form-select-filter--dropdown--item--checkbox--height`           | 1.25rem;                                                                        |
| `--digi--form-select-filter--dropdown--item--checkbox--width`            | 1.25rem;                                                                        |
| `--digi--form-select-filter--dropdown--item--color`                      | var(--digi--color--text--primary);                                              |
| `--digi--form-select-filter--dropdown--item--cursor`                     | pointer;                                                                        |
| `--digi--form-select-filter--dropdown--item--font-family`                | var(--digi--global--typography--font-family--default);                          |
| `--digi--form-select-filter--dropdown--item--font-size`                  | var(--digi--typography--body--font-size--desktop);                              |
| `--digi--form-select-filter--dropdown--item--font-weight--default`       | var(--digi--typography--link--font-weight--desktop);                            |
| `--digi--form-select-filter--dropdown--item--font-weight--hover`         | var(--digi--typography--link-bold--font-weight--desktop);                       |
| `--digi--form-select-filter--dropdown--item--font-weight--selected`      | var(--digi--typography--link-bold--font-weight--desktop);                       |
| `--digi--form-select-filter--dropdown--item--height`                     | 1.5rem;                                                                         |
| `--digi--form-select-filter--dropdown--item--letter-spacing`             | 0px;                                                                            |
| `--digi--form-select-filter--dropdown--item--line-height`                | 1.5;                                                                            |
| `--digi--form-select-filter--dropdown--item--margin-bottom`              | 0px;                                                                            |
| `--digi--form-select-filter--dropdown--item--multiple--padding-x`        | var(--digi--padding--largest-2);                                                |
| `--digi--form-select-filter--dropdown--item--multiple--padding-y`        | var(--digi--margin--small);                                                     |
| `--digi--form-select-filter--dropdown--item--padding-x`                  | var(--digi--margin--largest);                                                   |
| `--digi--form-select-filter--dropdown--item--padding-y`                  | var(--digi--margin--small);                                                     |
| `--digi--form-select-filter--dropdown--margin-top`                       | var(--digi--gutter--small);                                                     |
| `--digi--form-select-filter--dropdown--min-width`                        | 18.75rem;                                                                       |
| `--digi--form-select-filter--dropdown--z-index`                          | 999;                                                                            |
| `--digi--form-select-filter--footer--background`                         | var(--digi--color--background--secondary);                                      |
| `--digi--form-select-filter--item--margin`                               | var(--digi--gutter--small) 0;                                                   |
| `--digi--form-select-filter--legend--font-family`                        | var(--digi--global--typography--font-family--default);                          |
| `--digi--form-select-filter--legend--font-size`                          | var(--digi--typography--preamble--font-size--desktop);                          |
| `--digi--form-select-filter--legend--font-weight`                        | var(--digi--typography--preamble--font-weight--desktop);                        |
| `--digi--form-select-filter--legend__border-bottom`                      | var(--digi--border-width--primary) solid var(--digi--color--border--neutral-2); |
| `--digi--form-select-filter--list--margin`                               | 0;                                                                              |
| `--digi--form-select-filter--list--padding`                              | var(--digi--gutter--small) 0 0 0;                                               |
| `--digi--form-select-filter--reset-button--margin-right`                 | var(--digi--padding--small);                                                    |
| `--digi--form-select-filter--scroll--max-height`                         | 10.625rem;                                                                      |
| `--digi--form-select-filter--scroll--padding`                            | var(--digi--padding--small);                                                    |
| `--digi--form-select-filter--search-input--border-color`                 | var(--digi--form-select-filter--toggle-button--border-width--neutral);          |
| `--digi--form-select-filter--search-input--border-radius`                | var(--digi--border-radius--input);                                              |
| `--digi--form-select-filter--search-input--border-style`                 | var(--digi--border-style--primary);                                             |
| `--digi--form-select-filter--search-input--border-width`                 | var(--digi--border-width--input-regular);                                       |
| `--digi--form-select-filter--search-input--height`                       | var(--digi--input-height--medium);                                              |
| `--digi--form-select-filter--search-input--width`                        | 100%;                                                                           |
| `--digi--form-select-filter--toggle-button--background--error`           | var(--digi--color--background--danger-2);                                       |
| `--digi--form-select-filter--toggle-button--background--neutral`         | var(--digi--color--background--input);                                          |
| `--digi--form-select-filter--toggle-button--background--success`         | var(--digi--color--background--success-2);                                      |
| `--digi--form-select-filter--toggle-button--background--warning`         | var(--digi--color--background--warning-2);                                      |
| `--digi--form-select-filter--toggle-button--border-color--error`         | var(--digi--color--border--danger);                                             |
| `--digi--form-select-filter--toggle-button--border-color--neutral`       | var(--digi--color--border--neutral-3);                                          |
| `--digi--form-select-filter--toggle-button--border-color--success`       | var(--digi--color--border--success);                                            |
| `--digi--form-select-filter--toggle-button--border-color--warning`       | var(--digi--color--border--neutral-3);                                          |
| `--digi--form-select-filter--toggle-button--border-width--error`         | var(--digi--border-width--input-validation);                                    |
| `--digi--form-select-filter--toggle-button--border-width--neutral`       | var(--digi--border-width--input-regular);                                       |
| `--digi--form-select-filter--toggle-button--border-width--success`       | var(--digi--border-width--input-validation);                                    |
| `--digi--form-select-filter--toggle-button--border-width--warning`       | var(--digi--border-width--input-validation);                                    |
| `--digi--form-select-filter--toggle-button--text--margin-right`          | var(--digi--margin--smaller);                                                   |
| `--digi--form-select-filter--toggle-button-indicator--color`             | var(--digi--color--background--success-1);                                      |
| `--digi--form-select-filter--toggle-button-indicator--color--active`     | var(--digi--color--border--success);                                            |
| `--digi--form-select-filter--toggle-button-indicator--color--default`    | transparent;                                                                    |
| `--digi--form-select-filter--toggle-button-indicator--height`            | 1rem;                                                                           |
| `--digi--form-select-filter--toggle-button-indicator--margin-left`       | calc(var(--digi--margin--smaller) * -1);                                        |
| `--digi--form-select-filter--toggle-button-indicator--margin-right`      | var(--digi--margin--small);                                                     |
| `--digi--form-select-filter--toggle-button-indicator--width`             | 1rem;                                                                           |
| `--digi--form-select-filter--toggle-icon--height`                        | 0.875rem;                                                                       |
| `--digi--form-select-filter--toggle-icon--transition`                    | ease-in-out 0.2s all;                                                           |
| `--digi--form-select-filter--toggle-icon--width`                         | 0.875rem;                                                                       |


## Dependencies

### Depends on

- [digi-form-label](../../../__core/_form/form-label)
- [digi-util-keydown-handler](../../../__core/_util/util-keydown-handler)
- [digi-icon](../../../__core/_icon/icon)
- [digi-form-input](../../../__core/_form/form-input)
- [digi-button](../../../__core/_button/button)
- [digi-form-validation-message](../../../__core/_form/form-validation-message)

### Graph
```mermaid
graph TD;
  digi-form-select-filter --> digi-form-label
  digi-form-select-filter --> digi-util-keydown-handler
  digi-form-select-filter --> digi-icon
  digi-form-select-filter --> digi-form-input
  digi-form-select-filter --> digi-button
  digi-form-select-filter --> digi-form-validation-message
  digi-form-input --> digi-form-label
  digi-form-input --> digi-form-validation-message
  digi-form-validation-message --> digi-icon
  style digi-form-select-filter fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
