import { Component, h, Prop, State } from '@stencil/core';
import { avatarPlaceholder, AvatarPlaceholderName } from './avatars/avatars';
/**
 * 
 *
 * @swedishName Sidhuvud-avatar 
 */
@Component({
	tag: 'digi-header-avatar',
	styleUrl: 'header-avatar.scss',
	scoped: true
})
export class NavigationAvatar {
	@State() currentAvatar;

	/**
	 * För att visa avataren om man är inloggad
	 * @en Shows the avatar if logged in.
	 */
	@Prop() afIsLoggedIn: boolean = true;

	/**
	 * Sätter attributet 'src'.
	 * @en Sets `src` attribute.
	 */
	@Prop() afSrc: string;

	/**
	 * Sätter attributet 'alt'.
	 * @en Sets `alt` attribute.
	 */
	@Prop() afAlt!: string;

	/**
	 * Visar placeholderbild om profilbild inte finns
	 * @en Shows placerholder image if profile image isn't available.
	 */
	@Prop() afShowPlaceholderImage: boolean;

	/**
	 * Döljer profilbilden.
	 * @en Hides profile image.
	 */
	@Prop() afHideImage: boolean = false;

	/**
	 * Sätter namnet på användaren
	 * @en Sets user name
	 */
	@Prop() afName: string;

	/**
	 * Döljer användarens namn
	 * @en Hides user name
	 */
	@Prop() afHideName: boolean = false;

	/**
	 * Sätter användarens signatur
	 * @en Set user signature
	 */
	@Prop() afSignature: string;

	/**
	 * Döljer användarens signatur
	 * @en Hide user signature
	 */
	@Prop() afHideSignature: boolean = false;

	/**
	 * Visar en placeholdertext om användaren är dold
	 * @en Shows a placeholder text if user is hidden
	 */
	@Prop() afUserPlacerholder: string = "Inloggad";

	/**
	 * Title för placeholderikonen
	 * @en Shows title for placeholder avatar
	 */
		@Prop() afPlacerholderTitle: string = "";

	/**
	 * Döljer användaren helt
	 * @en Hide user
	 */
	@Prop() afHideUser: boolean = false;

	setCurrentLoginName() {
		if (!this.afHideName && !this.afHideSignature) {
			return this.afName + ' (' + this.afSignature + ')';
		}
		if (!this.afHideSignature) {
			return this.afSignature;
		}
		if (!this.afHideName) {
			return this.afName;
		}
		if (this.afHideUser) {
			return this.afUserPlacerholder;
		}
	}

	setAvatarPlaceholder(name: AvatarPlaceholderName) {
		const { AvatarPlaceholderComponent } = avatarPlaceholder[name];
		this.currentAvatar = AvatarPlaceholderComponent;
	}

	componentWillLoad() {
		this.getAvatar();
	}

	private getAvatar(): void {
		if (this.afIsLoggedIn) {
			return this.setAvatarPlaceholder('avatar-logged-in');
		} else {
			return this.setAvatarPlaceholder('avatar-logged-out');
		}
	}

	render() {
		if (this.afShowPlaceholderImage) {
			this.afHideImage = true;
		}
		return (
			<div class="digi-header-avatar">
				{!this.afHideImage && this.afIsLoggedIn && (
					<digi-media-image
						class={'digi-header-avatar__image'}
						af-height="32"
						af-width="32"
						afUnlazy
						af-src={this.afSrc}
						afAlt={this.afAlt}
					></digi-media-image>
				)}

				{this.afShowPlaceholderImage && this.afIsLoggedIn &&(
					<this.currentAvatar>{<title>{this.afPlacerholderTitle}</title>}</this.currentAvatar>
				)}

				{this.afIsLoggedIn && (
					<span class="digi-header-avatar__name">{this.setCurrentLoginName()}</span>
				)}

				{!this.afIsLoggedIn && (
					<this.currentAvatar>{<title>{this.afPlacerholderTitle}</title>}</this.currentAvatar>
				)}

				{!this.afIsLoggedIn && <slot></slot>}
				
			</div>
		);
	}
}