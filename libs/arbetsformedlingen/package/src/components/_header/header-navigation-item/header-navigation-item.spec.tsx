import { newSpecPage } from '@stencil/core/testing';
import { HeaderNavigationItem } from './header-navigation-item';

describe('header-navigation-item', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [HeaderNavigationItem],
			html: '<header-navigation-item></header-navigation-item>'
		});
		expect(root).toEqualHtml(`
      <header-navigation-item>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </header-navigation-item>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [HeaderNavigationItem],
			html: `<header-navigation-item first="Stencil" last="'Don't call me a framework' JS"></header-navigation-item>`
		});
		expect(root).toEqualHtml(`
      <header-navigation-item first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </header-navigation-item>
    `);
	});
});
