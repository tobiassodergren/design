import { h } from '@stencil/core';
import { HeaderNavigationItem } from './header-navigation-item';

export default {
	title: 'HeaderNavigationItem',
	component: HeaderNavigationItem
};

const Template = (args) => (
	<header-navigation-item {...args}></header-navigation-item>
);

export const Primary = Template.bind({});
Primary.args = { first: 'Hello', last: 'World' };
