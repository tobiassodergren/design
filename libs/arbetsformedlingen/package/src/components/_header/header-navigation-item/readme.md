# header-navigation-item



<!-- Auto Generated Below -->


## Properties

| Property        | Attribute         | Description                      | Type      | Default     |
| --------------- | ----------------- | -------------------------------- | --------- | ----------- |
| `afCurrentPage` | `af-current-page` | Sätter vilken sida som är active | `boolean` | `undefined` |


## CSS Custom Properties

| Name                                           | Description                                            |
| ---------------------------------------------- | ------------------------------------------------------ |
| `--digi--header-navigation-item--color`        | var(--digi--color--text--primary);                     |
| `--digi--header-navigation-item--color-active` | var(--digi--color--text--link);                        |
| `--digi--header-navigation-item--color-hover`  | var(--digi--color--text--link);                        |
| `--digi--header-navigation-item--font-family`  | var(--digi--global--typography--font-family--default); |
| `--digi--header-navigation-item--font-size`    | var(--digi--typography--body--font-size--desktop);     |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
