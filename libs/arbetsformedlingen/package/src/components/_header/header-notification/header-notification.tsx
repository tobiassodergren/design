import { Component, h, Prop } from '@stencil/core';
/**
 * 
 *
 * @swedishName Sidhuvud-notifikationsindikator
 */
@Component({
	tag: 'digi-header-notification',
	styleUrl: 'header-notification.scss',
	scoped: true
})
export class HeaderNotification {
	/**
	 * Numret som ska skrivas ut som innehåll
	 * @en The number content inside the notification 
	 */
	@Prop() afNotificationAmount: number;

	render() {
		return (
			<div class="digi-header-notification">
				{this.afNotificationAmount > 0 && (
					<span class="digi-header-notification__number">
						{this.afNotificationAmount < 100 ? this.afNotificationAmount : '+99'}
					</span>
				)}
				<slot />
			</div>
		);
	}
}
