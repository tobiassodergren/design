import { newSpecPage } from '@stencil/core/testing';
import { Header } from './header';

describe('header', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [Header],
			html: '<header></header>'
		});
		expect(root).toEqualHtml(`
      <header>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </header>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [Header],
			html: `<header first="Stencil" last="'Don't call me a framework' JS"></header>`
		});
		expect(root).toEqualHtml(`
      <header first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </header>
    `);
	});
});
