import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-clock',
	styleUrls: ['icon-clock.scss'],
	scoped: true
})
export class IconClock {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-clock"
				width="25"
				height="25"
				viewBox="0 0 25 25"
				xmlns="http://www.w3.org/2000/svg"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-clock__shape"
					d="M12.5 0C5.595 0 0 5.595 0 12.5S5.595 25 12.5 25 25 19.405 25 12.5 19.405 0 12.5 0zm0 22.58c-5.57 0-10.08-4.51-10.08-10.08S6.93 2.42 12.5 2.42 22.58 6.93 22.58 12.5 18.07 22.58 12.5 22.58zm3.115-5.261l-4.28-3.11a.609.609 0 01-.246-.49V5.445c0-.333.272-.605.605-.605h1.612c.333 0 .605.272.605.605v7.142l3.367 2.45a.604.604 0 01.131.846l-.947 1.306a.609.609 0 01-.847.13z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
