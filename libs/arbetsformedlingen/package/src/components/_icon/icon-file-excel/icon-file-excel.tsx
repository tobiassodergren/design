import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-file-excel',
	styleUrl: 'icon-file-excel.scss',
	scoped: true
})
export class IconFileExcel {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-file-excel"
				width="48"
				height="48"
				viewBox="0 0 48 48"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
				xmlns="http://www.w3.org/2000/svg"
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-file-excel__shape"
					d="M27.16,12.75 L27.16,0 L9.16,0 C7.963,0 7,1.003125 7,2.25 L7,45.75 C7,46.996875 7.963,48 9.16,48 L39.4,48 C40.597,48 41.56,46.996875 41.56,45.75 L41.56,15 L29.32,15 C28.132,15 27.16,13.9875 27.16,12.75 Z M32.569,22.734375 L27.16,31.5 L32.569,40.265625 C33.028,41.015625 32.515,42 31.66,42 L28.519,42 C28.123,42 27.754,41.775 27.565,41.409375 C25.801,38.015625 24.28,34.96875 24.28,34.96875 C23.704,36.35625 23.38,36.84375 20.986,41.41875 C20.797,41.784375 20.437,42.009375 20.041,42.009375 L16.9,42.009375 C16.045,42.009375 15.532,41.025 15.991,40.275 L21.418,31.509375 L15.991,22.74375 C15.523,21.99375 16.045,21.0093085 16.9,21.0093085 L20.032,21.0093085 C20.428,21.0093085 20.797,21.234375 20.986,21.6 C23.335,26.175 22.786,24.75 24.28,28.021875 C24.28,28.021875 24.829,26.925 27.574,21.6 C27.763,21.234375 28.132,21.0093085 28.528,21.0093085 L31.66,21.0093085 C32.515,21 33.028,21.984375 32.569,22.734375 Z M41.56,11.428125 L41.56,12 L30.04,12 L30.04,0 L30.589,0 C31.165,0 31.714,0.234375 32.119,0.65625 L40.93,9.84375 C41.335,10.265625 41.56,10.8375 41.56,11.428125 Z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
