import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-image',
	styleUrl: 'icon-image.scss',
	scoped: true
})
export class IconImage {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-image"
				width="48"
				height="48"
				viewBox="0 0 48 48"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
				xmlns="http://www.w3.org/2000/svg"
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-image__shape"
					d="M43.5,5.645833 L4.5,5.645833 C2.0146875,5.645833 0,7.6605205 0,10.145833 L0,37.145833 C0,39.6311455 2.0146875,41.645833 4.5,41.645833 L43.5,41.645833 C45.9853125,41.645833 48,39.6311455 48,37.145833 L48,10.145833 C48,7.6605205 45.9853125,5.645833 43.5,5.645833 Z M42.9375,37.145833 L5.0625,37.145833 C4.75183983,37.145833 4.5,36.8939932 4.5,36.583333 L4.5,10.708333 C4.5,10.3976728 4.75183983,10.145833 5.0625,10.145833 L42.9375,10.145833 C43.2481602,10.145833 43.5,10.3976728 43.5,10.708333 L43.5,36.583333 C43.5,36.8939932 43.2481602,37.145833 42.9375,37.145833 L42.9375,37.145833 Z M12,13.895833 C9.92896875,13.895833 8.25,15.5748018 8.25,17.645833 C8.25,19.7168643 9.92896875,21.395833 12,21.395833 C14.0710312,21.395833 15.75,19.7168643 15.75,17.645833 C15.75,15.5748018 14.0710312,13.895833 12,13.895833 Z M9,32.645833 L39,32.645833 L39,25.145833 L30.7954687,16.9413018 C30.3561562,16.5019893 29.6438437,16.5019893 29.2044375,16.9413018 L18,28.145833 L14.2954687,24.4413018 C13.8561562,24.0019893 13.1438437,24.0019893 12.7044375,24.4413018 L9,28.145833 L9,32.645833 Z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
