import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-print',
	styleUrls: ['icon-print.scss'],
	scoped: true
})
export class IconPrint {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-print"
				width="26"
				height="26"
				viewBox="0 0 26 26"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
				xmlns="http://www.w3.org/2000/svg"
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-print__shape"
					d="M23.563 9.75h-.813V4.161c0-.323-.128-.633-.357-.862L19.451.357A1.219 1.219 0 0018.589 0H4.469C3.796 0 3.25.546 3.25 1.219V9.75h-.813A2.437 2.437 0 000 12.188v6.703c0 .336.273.609.61.609h2.64v5.281c0 .673.546 1.219 1.219 1.219H21.53c.673 0 1.219-.546 1.219-1.219V19.5h2.64a.61.61 0 00.61-.61v-6.703a2.437 2.437 0 00-2.438-2.437zm-4.063 13h-13v-4.875h13v4.875zm-13-11.375V3.25h9.75v2.031c0 .67.548 1.219 1.219 1.219H19.5v4.875h-13zm15.438 3.656a1.219 1.219 0 110-2.437 1.219 1.219 0 010 2.437z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
