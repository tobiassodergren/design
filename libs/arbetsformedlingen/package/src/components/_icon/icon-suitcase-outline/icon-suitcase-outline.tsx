import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-suitcase-outline',
	styleUrl: 'icon-suitcase-outline.scss',
	scoped: true
})
export class IconSuitcaseOutline {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-suitcase-outline"
				width="48"
				height="48"
				viewBox="0 0 48 48"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
				xmlns="http://www.w3.org/2000/svg"
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-suitcase-outline__shape"
					d="M43.34,12.7619048 L35.84,12.7619048 L35.84,8.38095238 C35.84,5.96230159 33.824375,4 31.34,4 L16.34,4 C13.855625,4 11.84,5.96230159 11.84,8.38095238 L11.84,12.7619048 L4.34,12.7619048 C1.855625,12.7619048 -0.16,14.7242063 -0.16,17.1428571 L-0.16,40.5079365 C-0.16,42.9265873 1.855625,44.8888889 4.34,44.8888889 L43.34,44.8888889 C45.824375,44.8888889 47.84,42.9265873 47.84,40.5079365 L47.84,17.1428571 C47.84,14.7242063 45.824375,12.7619048 43.34,12.7619048 Z M16.34,8.38095238 L31.34,8.38095238 L31.34,12.7619048 L16.34,12.7619048 L16.34,8.38095238 Z M4.34,40.5079365 L4.34,17.1428571 L11.84,17.1428571 L11.84,40.5079365 L4.34,40.5079365 Z M16.34,40.5079365 L16.34,17.1428571 L31.34,17.1428571 L31.34,40.5079365 L16.34,40.5079365 Z M43.34,40.5079365 L35.84,40.5079365 L35.84,17.1428571 L43.34,17.1428571 L43.34,40.5079365 Z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
