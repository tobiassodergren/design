import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-tyck-till',
	styleUrls: ['icon-tyck-till.scss'],
	scoped: true
})
export class IconTyckTill {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-tyck-till"
				width="32"
				height="27"
				viewBox="0 0 32 27"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
				xmlns="http://www.w3.org/2000/svg"
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<g
					class="digi-icon-tyck-till__shape"
					fill="currentColor"
					fill-rule="evenodd"
				>
					<path d="M9.053 3.455c2.074-.814 4.306-1.222 6.697-1.222 2.39 0 4.626.405 6.706 1.213 2.08.809 3.732 1.9 4.957 3.272 1.225 1.372 1.837 2.856 1.837 4.45 0 1.302-.419 2.544-1.257 3.725-.838 1.18-2.018 2.201-3.542 3.062l-1.53.872.475 1.675a12.46 12.46 0 001.231 3.002 17.357 17.357 0 01-4.834-2.984l-.756-.663-1.002.105c-.808.093-1.57.14-2.285.14-2.39 0-4.626-.405-6.706-1.214-2.08-.808-3.732-1.899-4.957-3.271-1.225-1.373-1.837-2.856-1.837-4.45 0-1.593.612-3.077 1.837-4.45 1.225-1.372 2.88-2.46 4.966-3.262zM0 11.167c0 2.024.703 3.894 2.11 5.61 1.406 1.716 3.316 3.071 5.73 4.066 2.414.994 5.05 1.492 7.91 1.492.82 0 1.67-.047 2.549-.14 2.32 2.036 5.015 3.443 8.086 4.223.574.162 1.242.29 2.004.383.263 0 .422-.06.562-.183a.888.888 0 00.281-.48c.036-.064.039-.133.01-.226a.464.464 0 01-.036-.175c.006-.023-.02-.078-.079-.166l-.106-.157a2.586 2.586 0 00-.123-.148 9.152 9.152 0 01-1.837-2.111 7.086 7.086 0 01-.57-.89c-.141-.268-.3-.61-.475-1.03-.176-.418-.328-.86-.457-1.326 1.84-1.035 3.29-2.315 4.35-3.839 1.06-1.523 1.591-3.158 1.591-4.903 0-1.512-.416-2.957-1.248-4.336-.832-1.378-1.951-2.568-3.357-3.568-1.407-1-3.082-1.794-5.028-2.382A21.046 21.046 0 0015.75 0c-2.133 0-4.172.294-6.117.881-1.946.588-3.621 1.382-5.028 2.382-1.406 1-2.525 2.19-3.357 3.568C.416 8.21 0 9.655 0 11.167z" />
					<path d="M17.398 12.475h-2.707l-.418-7.703h3.543l-.418 7.703zm-3.093 2.85c0-.494.148-.878.443-1.152.296-.275.725-.412 1.288-.412.558 0 .98.137 1.268.412.287.274.43.658.43 1.152 0 .488-.149.87-.447 1.144-.298.274-.715.411-1.25.411-.542 0-.966-.137-1.272-.411-.306-.274-.46-.656-.46-1.144z" />
				</g>
			</svg>
		);
	}
}
