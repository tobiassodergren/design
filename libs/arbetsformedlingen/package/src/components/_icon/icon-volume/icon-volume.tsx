import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-volume',
	styleUrl: 'icon-volume.scss',
	scoped: true
})
export class IconVolume {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-volume"
				width="48"
				height="48"
				viewBox="0 0 48 48"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
				xmlns="http://www.w3.org/2000/svg"
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-volume__shape"
					d="M21.503,6.67523344 L12.606,15.1996901 L2.4,15.1996901 C1.074,15.1996901 0,16.22895 0,17.4997121 L0,31.2998443 C0,32.5696482 1.074,33.5998664 2.4,33.5998664 L12.606,33.5998664 L21.503,42.124323 C23.006,43.5647118 25.6,42.5527021 25.6,40.4980158 L25.6,8.30154068 C25.6,6.24493765 23.004,5.23676132 21.503,6.67523344 Z M48,24.3997782 C48,18.3114282 44.794,12.7137496 39.423,9.4266348 C38.304,8.74237824 36.82,9.06054796 36.111,10.1415583 C35.402,11.2225687 35.733,12.653374 36.852,13.3385889 C40.827,15.7718206 43.2,19.9061102 43.2,24.3997782 C43.2,28.8934463 40.827,33.0277359 36.852,35.4609675 C35.733,36.1452241 35.402,37.5760295 36.111,38.6579982 C36.762,39.650841 38.223,40.1089287 39.423,39.3729217 C44.794,36.0858069 48,30.4881282 48,24.3997782 Z M33.823,17.0329993 C32.665,16.4263685 31.204,16.8259973 30.562,17.938633 C29.923,19.0512687 30.346,20.4494904 31.507,21.0637879 C32.798,21.7432528 33.6,23.02264 33.6,24.3997782 C33.6,25.7778748 32.798,27.0563037 31.508,27.7357685 C30.347,28.3500661 29.924,29.7482878 30.563,30.8609235 C31.206,31.9783508 32.668,32.3751046 33.824,31.7665571 C36.647,30.2763345 38.401,27.4540158 38.401,24.3988199 C38.401,21.343624 36.647,18.5222636 33.823,17.0329993 Z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
