import { newE2EPage } from '@stencil/core/testing';

describe('digi-info-card', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-info-card></digi-info-card>');

    const element = await page.find('digi-info-card');
    expect(element).toHaveClass('hydrated');
  });
});
