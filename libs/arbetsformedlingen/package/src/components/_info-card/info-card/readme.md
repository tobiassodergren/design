# digi-info-card

This is an info card. It exists in a number of different variations.

## Enums

If used in a Typescript environment, you will need to import a couple of enums:

```ts
import { InfoCardHeadingLevel, InfoCardType, InfoCardVariation } from '@digi/core';
```

<!-- Auto Generated Below -->


## Properties

| Property                      | Attribute            | Description                                                                                             | Type                                                                                                                                                             | Default                      |
| ----------------------------- | -------------------- | ------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------- |
| `afBorderPosition`            | `af-border-position` | Sätter storlek på kortet. Kan vara 'Standard'.                                                          | `InfoCardBorderPosition.LEFT \| InfoCardBorderPosition.TOP`                                                                                                      | `InfoCardBorderPosition.TOP` |
| `afHeading` _(required)_      | `af-heading`         | Rubrikens text                                                                                          | `string`                                                                                                                                                         | `undefined`                  |
| `afHeadingLevel` _(required)_ | `af-heading-level`   | Sätter rubrikens vikt (h1-h6)                                                                           | `InfoCardHeadingLevel.H1 \| InfoCardHeadingLevel.H2 \| InfoCardHeadingLevel.H3 \| InfoCardHeadingLevel.H4 \| InfoCardHeadingLevel.H5 \| InfoCardHeadingLevel.H6` | `undefined`                  |
| `afLinkHref`                  | `af-link-href`       | Sätter attributet 'href' på länken                                                                      | `string`                                                                                                                                                         | `undefined`                  |
| `afLinkText`                  | `af-link-text`       | Text till en valfri länk. NOTERA: Länken är obligatorisk om 'af-type' är 'single'.                      | `string`                                                                                                                                                         | `undefined`                  |
| `afSize`                      | `af-size`            | Sätter storlek på kortet. Kan vara 'Standar'.                                                           | `InfoCardSize`                                                                                                                                                   | `InfoCardSize.STANDARD`      |
| `afSvgAriaHidden`             | `af-svg-aria-hidden` | För att dölja ikon för skärmläsare.                                                                     | `boolean`                                                                                                                                                        | `undefined`                  |
| `afType`                      | `af-type`            | Sätter typ. Kan vara 'tip' or 'related'.                                                                | `InfoCardType.RELATED \| InfoCardType.TIP`                                                                                                                       | `InfoCardType.TIP`           |
| `afVariation`                 | `af-variation`       | Sätter variant. Kan vara 'primary' eller 'secondary'. Detta har endast genomslag om 'af-type' är 'tip'. | `InfoCardVariation.PRIMARY \| InfoCardVariation.SECONDARY`                                                                                                       | `InfoCardVariation.PRIMARY`  |


## Events

| Event           | Description                    | Type               |
| --------------- | ------------------------------ | ------------------ |
| `afOnClickLink` | Länkelementets 'onclick'-event | `CustomEvent<any>` |


## Slots

| Slot        | Description                 |
| ----------- | --------------------------- |
| `"default"` | kan innehålla vad som helst |


## CSS Custom Properties

| Name                                             | Description                                                                                                                  |
| ------------------------------------------------ | ---------------------------------------------------------------------------------------------------------------------------- |
| `--digi--info-card--background-color--primary`   | var(--digi--color--background--neutral-5);                                                                                   |
| `--digi--info-card--background-color--related`   | var(--digi--color--background--primary);                                                                                     |
| `--digi--info-card--background-color--secondary` | var(--digi--color--background--secondary);                                                                                   |
| `--digi--info-card--border`                      | var(--digi--border-width--complementary-2) solid var(--digi--color--border--secondary);                                      |
| `--digi--info-card--color--heading`              | var(--digi--color--text--primary);                                                                                           |
| `--digi--info-card--color--heading--multi`       | var(--digi--color--text--link);                                                                                              |
| `--digi--info-card--heading--font-size--desktop` | var(--digi--typography--heading-2--font-size--desktop);                                                                      |
| `--digi--info-card--heading--font-size--mobile`  | var(--digi--typography--heading-2--font-size--mobile);                                                                       |
| `--digi--info-card--heading--font-weight`        | var(--digi--typography--heading-3--font-weight--desktop);                                                                    |
| `--digi--info-card--padding`                     | var(--digi--padding--large) var(--digi--padding--medium) var(--digi--padding--largest) var(--digi--padding--medium);         |
| `--digi--info-card--padding--desktop`            | var(--digi--padding--largest-2) var(--digi--padding--largest) var(--digi--padding--largest-3) var(--digi--padding--largest); |


## Dependencies

### Depends on

- [digi-typography](../../../__core/_typography/typography)
- [digi-link-internal](../../../__core/_link/link-internal)

### Graph
```mermaid
graph TD;
  digi-info-card --> digi-typography
  digi-info-card --> digi-link-internal
  digi-link-internal --> digi-link
  digi-link-internal --> digi-icon
  style digi-info-card fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
