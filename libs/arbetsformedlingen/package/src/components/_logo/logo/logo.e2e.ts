import { newE2EPage } from '@stencil/core/testing';

describe('digi-logo', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-logo></digi-logo>');

    const element = await page.find('digi-logo');
    expect(element).toHaveClass('hydrated');
  });
});
