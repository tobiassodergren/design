# digi-navigation-breadcrumbs

This is a navigational breadcrumb component receiving links through a slot. The slot expects a-elements with a href attribute and text-content.

The component will manipulate and rerender the sloted elements wrapping them within a li-element. The `af-current-page` prop will be displayed as the current page.

When the breadcrumbs-list state changes the list will get updated through componentWillUpdate.

## INFO

In order to create a custom routing it is up to the application to prevent the click through the `afOnClick` event.

<!-- Auto Generated Below -->


## Properties

| Property                     | Attribute         | Description                                                           | Type     | Default                                            |
| ---------------------------- | ----------------- | --------------------------------------------------------------------- | -------- | -------------------------------------------------- |
| `afAriaLabel`                | `af-aria-label`   | Sätter attributet 'aria-label' på navelementet.                       | `string` | `_t.breadcrumbs`                                   |
| `afCurrentPage` _(required)_ | `af-current-page` | Sätter texten för den aktiva sidan                                    | `string` | `undefined`                                        |
| `afId`                       | `af-id`           | Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id. | `string` | `randomIdGenerator('digi-navigation-breadcrumbs')` |


## Events

| Event       | Description                     | Type               |
| ----------- | ------------------------------- | ------------------ |
| `afOnClick` | Länkelementens 'onclick'-event. | `CustomEvent<any>` |


## Slots

| Slot        | Description                                 |
| ----------- | ------------------------------------------- |
| `"default"` | Ska inehålla länkelement med href-attribut. |


## CSS Custom Properties

| Name                                                                  | Description                                                     |
| --------------------------------------------------------------------- | --------------------------------------------------------------- |
| `--digi--navigation-breadcrumbs--divider--padding`                    | var(--digi--gutter--icon);                                      |
| `--digi--navigation-breadcrumbs--item--color`                         | var(--digi--color--text--primary);                              |
| `--digi--navigation-breadcrumbs--item--font-family`                   | var(--digi--global--typography--font-family--default);          |
| `--digi--navigation-breadcrumbs--item--font-size`                     | var(--digi--typography--breadcrumbs--font-size--desktop);       |
| `--digi--navigation-breadcrumbs--item--text-decoration`               | var(--digi--typography--breadcrumbs--text-decoration--desktop); |
| `--digi--navigation-breadcrumbs--items--display`                      | flex;                                                           |
| `--digi--navigation-breadcrumbs--items--flex-wrap`                    | wrap;                                                           |
| `--digi--navigation-breadcrumbs--items--margin`                       | 0;                                                              |
| `--digi--navigation-breadcrumbs--items--padding`                      | 0;                                                              |
| `--digi--navigation-breadcrumbs--link--color`                         | var(--digi--color--text--link);                                 |
| `--digi--navigation-breadcrumbs--link--hover--focus--color`           | var(--digi--color--text--link-hover);                           |
| `--digi--navigation-breadcrumbs--link--hover--focus--text-decoration` | underline;                                                      |
| `--digi--navigation-breadcrumbs--link--visited--color`                | var(--digi--color--text--link-visited);                         |


## Dependencies

### Depends on

- [digi-util-mutation-observer](../../../__core/_util/util-mutation-observer)

### Graph
```mermaid
graph TD;
  digi-navigation-breadcrumbs --> digi-util-mutation-observer
  style digi-navigation-breadcrumbs fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
