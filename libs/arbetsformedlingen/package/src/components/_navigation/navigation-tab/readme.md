# digi-navigation-tab

This component should only be used inside of `digi-navigation-tabs`. It creates a tab panel inside the tabs component.

<!-- Auto Generated Below -->


## Properties

| Property                   | Attribute       | Description                                                                             | Type      | Default                                    |
| -------------------------- | --------------- | --------------------------------------------------------------------------------------- | --------- | ------------------------------------------ |
| `afActive`                 | `af-active`     | Sätter aktiv tabb. Detta sköts av digi-navigation-tabs som ska omsluta denna komponent. | `boolean` | `undefined`                                |
| `afAriaLabel` _(required)_ | `af-aria-label` | Sätter attributet 'aria-label'                                                          | `string`  | `undefined`                                |
| `afId`                     | `af-id`         | Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.                   | `string`  | `randomIdGenerator('digi-navigation-tab')` |


## Events

| Event        | Description                                | Type                   |
| ------------ | ------------------------------------------ | ---------------------- |
| `afOnToggle` | När tabben växlar mellan aktiv och inaktiv | `CustomEvent<boolean>` |


## Slots

| Slot        | Description                 |
| ----------- | --------------------------- |
| `"default"` | Kan innehålla vad som helst |


## CSS Custom Properties

| Name                                        | Description                                      |
| ------------------------------------------- | ------------------------------------------------ |
| `--digi--navigation-tab--box-shadow--focus` | solid 2px var(--digi--color--border--secondary); |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
