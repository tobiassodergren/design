import { newE2EPage } from '@stencil/core/testing';

describe('digi-notification-alert', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-notification-alert></digi-notification-alert>');

    const element = await page.find('digi-notification-alert');
    expect(element).toHaveClass('hydrated');
  });
});
