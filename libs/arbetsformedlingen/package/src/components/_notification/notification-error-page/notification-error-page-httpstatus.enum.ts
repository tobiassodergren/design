export enum ErrorPageStatusCodes {
  UNAUTHORIZED = '401',
  FORBIDDEN = '403',
  NOT_FOUND = '404',
  GONE = '410',
  INTERNAL_SERVER_ERRROR = '500',
  GATEWAY_TIMEOUT = '504'
}
