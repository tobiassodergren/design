# notification-error-page



<!-- Auto Generated Below -->


## Properties

| Property           | Attribute             | Description                                                           | Type                                                                                                                                                                                                                        | Default                                             |
| ------------------ | --------------------- | --------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------- |
| `afCustomHeading`  | `af-custom-heading`   | Använd för egen rubrik                                                | `string`                                                                                                                                                                                                                    | `undefined`                                         |
| `afHttpStatusCode` | `af-http-status-code` | Http status kod som supportas av komponenten                          | `ErrorPageStatusCodes.FORBIDDEN \| ErrorPageStatusCodes.GATEWAY_TIMEOUT \| ErrorPageStatusCodes.GONE \| ErrorPageStatusCodes.INTERNAL_SERVER_ERRROR \| ErrorPageStatusCodes.NOT_FOUND \| ErrorPageStatusCodes.UNAUTHORIZED` | `undefined`                                         |
| `afId`             | `af-id`               | Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id. | `string`                                                                                                                                                                                                                    | `randomIdGenerator('digi-notification-error-page')` |


## Slots

| Slot        | Description                                                                                    |
| ----------- | ---------------------------------------------------------------------------------------------- |
| `"default"` | Html-text, använd för att skriva över komponentens default brödtext                            |
| `"links"`   | Ska vara en lista (<ul> eller <ol>) med listobjekt med länkar (<li><a href="/">Start</a></li>) |
| `"search"`  | Skicka in en komponent eller kod som har stöd för att skicka vidare besökaren till en söksida  |


## CSS Custom Properties

| Name                                                    | Description                                         |
| ------------------------------------------------------- | --------------------------------------------------- |
| `--digi--notification-error-page--gutter`               | calc(var(--digi--container-gutter--larger) + 3rem); |
| `--digi--notification-error-page--max-width`            | var(--digi--container-width--larger);               |
| `--digi--notification-error-page--padding-top--desktop` | 144px;                                              |
| `--digi--notification-error-page--padding-top--mobile`  | 48px;                                               |
| `--digi--notification-error-page--padding-top--tablet`  | 238px;                                              |


## Dependencies

### Depends on

- [digi-layout-container](../../../__core/_layout/layout-container)
- [digi-typography](../../../__core/_typography/typography)
- [digi-util-breakpoint-observer](../../../__core/_util/util-breakpoint-observer)

### Graph
```mermaid
graph TD;
  digi-notification-error-page --> digi-layout-container
  digi-notification-error-page --> digi-typography
  digi-notification-error-page --> digi-util-breakpoint-observer
  style digi-notification-error-page fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
