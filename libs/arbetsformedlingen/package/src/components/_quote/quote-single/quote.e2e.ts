import { newE2EPage } from '@stencil/core/testing';

describe('digi-quote', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-quote></digi-quote>');

    const element = await page.find('digi-quote');
    expect(element).toHaveClass('hydrated');
  });
});
