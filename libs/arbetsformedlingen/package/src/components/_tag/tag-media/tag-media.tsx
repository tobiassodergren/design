import {
	Component,
	h,
	Host,
	Prop,
	Element,
} from '@stencil/core';
import { HTMLStencilElement } from '@stencil/core/internal';
import { TagMediaIcon } from './tag-media-icon.enum';

/**
 * @slot mySlot - Slot description, if any
 *
 * @enums myEnum - tag-media-myEnum.enum.ts
 *
 * @swedishName Mediatagg
 */
@Component({
	tag: 'digi-tag-media',
	styleUrl: 'tag-media.scss',
	scoped: true
})
export class TagMedia {
	@Element() hostElement: HTMLStencilElement;

	/**
	 * Sätter taggens text.
	 * @en Set the tag text.
	 */
	@Prop() afText!: string;

	/**
	 * Sätter en specifik icon för taggen.
	 * @en Sets a specific icon for the tag.
	 */
	@Prop() afIcon: TagMediaIcon = TagMediaIcon.NEWS;

	get tagIcon() {
		if (this.afIcon === TagMediaIcon.NEWS) {
			return <digi-icon-news class="digi-tag-media__icon"></digi-icon-news>
		}

		if (this.afIcon === TagMediaIcon.PLAYLIST) {
			return <digi-icon-list-ul class="digi-tag-media__icon"></digi-icon-list-ul>
		}

		if (this.afIcon === TagMediaIcon.PODCAST) {
			return <digi-icon-media-podcast class="digi-tag-media__icon"></digi-icon-media-podcast>
		}

		if (this.afIcon === TagMediaIcon.FILM) {
			return <digi-icon-film class="digi-tag-media__icon"></digi-icon-film>
		}

		if (this.afIcon === TagMediaIcon.WEBTV) {
			return <digi-icon-web-tv class="digi-tag-media__icon"></digi-icon-web-tv>
		}

		if (this.afIcon === TagMediaIcon.WEBINAR) {
			return <digi-icon-webinar class="digi-tag-media__icon"></digi-icon-webinar>
		}

		return <digi-icon-news class="digi-tag-media__icon"></digi-icon-news>;
	}

	render() {
		return (
			<Host>
				<div
					class={{
						'digi-tag-media': true,
					}}
				>
					{this.tagIcon}
					<span class="digi-tag-media__text">
						{this.afText}
					</span>
				</div>
			</Host>
		);
	}
}
