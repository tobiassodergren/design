import { newSpecPage } from '@stencil/core/testing';
import { FeedbackBanner } from './tools-feedback-banner';

describe('tools-feedback-banner', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [FeedbackBanner],
			html: '<tools-feedback-banner></tools-feedback-banner>'
		});
		expect(root).toEqualHtml(`
      <tools-feedback-banner>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </tools-feedback-banner>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [FeedbackBanner],
			html: `<tools-feedback-banner first="Stencil" last="'Don't call me a framework' JS"></tools-feedback-banner>`
		});
		expect(root).toEqualHtml(`
      <tools-feedback-banner first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </tools-feedback-banner>
    `);
	});
});
