import { render } from '@testing-library/react';

import ArbetsformedlingenReact from './arbetsformedlingen-react';

describe('ArbetsformedlingenReact', () => {
	it('should render successfully', () => {
		const { baseElement } = render(<ArbetsformedlingenReact />);
		expect(baseElement).toBeTruthy();
	});
});
