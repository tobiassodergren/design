## [12.3.1] - 2022-02-09

- `digi-button, digi-link-(internal|external)`
  - Justerat positionering av ikoner och dess storlek för knappar och länkar.

## [12.3.0] - 2022-02-09

- `digi-navigation-tabs`
  - Lagt till en publik metod för att ändra aktiv flik.

## [12.1.1-beta.5] - 2022-01-20

- `value`
  - Value och afValue sätts vid afOnInput så attributet korrekt speglas vid target.

## [11.2.1-beta.0] - 2022-01-19

- `digi-form-radiobutton`
  - Fixat bugg där radiogroup inte stöds. Fungerar nu på det sätt du önskar!  

## [10.2.1] - 2022-01-19

### Ändrat

- `digi-media-image`
  - Fixat bugg med hur platshållaren fungerar
  - Ändrat bakgrunsfärg på platshållaren
  - Lagt till så attributet loading används på bilden. Sätts till `lazy` eller `eager` beroende på om man använder `afUnLazy` eller inte
  - Bilden går att ändra dynamiskt

## [12.0.0] - 2022-01-18

- `version`
  - Ökade versionsnumret till 12 för att undvika problem med paket i npm.


## [10.2.0] - 2022-01-18

- `digi-layout-block`
  - Lade till attributet af-vertical-padding för att kunna addera padding inuti container-elementet.
  - Lade till attributen af-margin-top och af-margin-bottom för att kunna addera marginal uppe och/eller nere på container-elementet.

- `digi-layout-container`
  - Lade till attributet af-vertical-padding för att kunna addera padding inuti elementet.
  - Lade till attributen af-margin-top och af-margin-bottom för att kunna addera marginal uppe och/eller nere på elementet.


## [11.1.0] - 2022-01-10

- `enums`
  - Enums finns numera under `@digi/core` och fungerar med bl.a. Angular.

- `types`
  -  Ändrat types från `components.d.ts` till `index.d.ts` som både exporterar components samt enums. På components finns namespace Components som kan användas t.ex. som `Components.DigiButton`. Förberett för interfaces om/när det ska användas och exponeras.

- `output-target`
  - Använder nu `dist-custom-elements` istället för `dist-custom-elements-bundle` som blivit deprecated.

## [10.1.3] - 2022-01-17

### Ändrat

- `digi-form-input`, `digi-form-textarea`
  - Tagit bort kontroll av 'dirty' och 'touched' av formulärelement vilket gör det mer flexibelt att välja när felmeddelanden ska visas.
- `digi-form-select`
  - Använder sig av `digi-util-mutation-observer` för att se om options-lista ändras programmatiskt.
- `digi-form-fieldset`
  - Lagt in möjlighet att sätta id på komponenten. Väljer man inget sätts en slumpmässig id.
- `digi-media-image`
  - Löst bugg som gjorde att komponenten inte använde den bredd och höjd man angett.

## [11.0.0] - 2021-12-23

- `digi-form-*`
  - Justerat formulärkomponenterna så att de kan använda `value` istället för `afValue` (som nu är markerat deprecated). Även `checked` ersätter `afChecked`. Detta är gjort för att bättre fungera med Angular samt det nya biblioteket Digi Core Angular som är påväg ut.

- `digi-form-select`
  - Ny logik för att hitta valt värde och skicka rätt event enligt samma practice som de andra formulärelementen.

- `digi-form-radiobutton`
  - Tog bort `afChecked` och använder nu `afValue`för att initialt sätta ett värde. När `value` är samma som `afValue` så är radioknappen icheckad (följer bättre radiogroup).

- `övrigt`
  - Digi Core är nu förberett för att kunna exportera filer via output-target till Angular. Ett nytt bibliotek `@digi/core-angular` kommer snart som är till för angular-appar. Det här biblioteket wrappar Digi Core och gör det enklare att använda t.ex. Reactive Forms.

## [10.1.2] - 2021-12-15

### Ändrat

- `digi-typography`
  - Lagt till så ul- och dl-listor får samma textstorlek som stycken och även samma hantering för att inte få för långa textrader
- `digi-navigation-pagination`
  - Justerat så att primär och sekundär variant på knapparna används korrekt. För att ändra utseende på knapparna så behöver knapparnas original-variabler ändras direkt, t.ex. `--digi-button--background`, dock finns variabler för t.ex. width, padding, m.m.

## [10.1.1] - 2021-12-10

### Ändrat

- `digi-form-filter`
  - Löst en bugg där komponenten tidigare fungerade felaktigt om någon kryssruta var aktiv vid sidladdning.
- `digi-navigation-tabs`
  - Lagt in stöd så komponenten känner av om man lägger till eller tar bort `digi-navigation-tab`-komponenter för att kunna dynamiskt ändra antal flikar.
- `digi-info-card`
  - Tagit bort felaktig marginal på rubriken.
- `digi-navigation-tab`
  - Ändrat så fokusram på en panel endast markeras när man navigerar dit med skärmläsare.
- `digi-form-filter`
  - Fixat så att komponenten fungerar korrekt även om man använder den inuti `digi-navigation-tab`. Tidigare stängdes listan när man försökte klicka på en kryssruta.

## [10.1.0] - 2021-11-23

### Nytt

- `digi-icon-exclamation-triangle-warning`
  - Lagt till en ny ikon. Denna ikon används bl.a. för varningsmeddelanden i formulär. Tidigare behövdes den ikonen läsas in som en asset i projektet, nu kommer inte detta behövas.

### Ändrat

- `digi-link-button`
  - Uppdaterat komponenten så man kan välja mellan olika storlekar.
  - Uppdaterat så alla layouter som finns i UI-kit också går att använda i kod.
- `digi-form-input`, `digi-form-select`, `digi-form-textarea`
  - Korrigerat så layout av formulärelement när de indikerar "fel", "varning" och "korrekt" följer UI-kit.
- `digi-icon-arrow-down`
  - Korrigerat layout så den följer UI-kit
- `digi-navigation-breadcrumbs`
  - Ändrat uppdelaren mellan länkar till '/', istället för '>'.


## [10.0.0] - 2021-10-19

### Ändrat

- `digi-navigation-menu`
  - Allt innehåll i Navigation Menu ska ligga i en `<ul>`-tagg och listas med respektive `<li>`-taggar.
- `digi-navigation-menu-item`
  - En Navigation Menu Item som ska agera som expanderbart menyalternativ ska följas av en `<ul>`-tagg, t.ex.
  ```
    <digi-navigation-menu-item></digi-navigation-menu-item>
    <ul>...</ul>
  ```
  För att göra raden expanderbar måste attributet `af-active-subnav` användas med antingen `true` för för-expanderad eller `false` för stängd.
- `digi-layout-media`
  - Lagt till token `--digi-layout-media-object--flex-wrap`
- `digi-progressbar`
  - Lagt till möjlighet att tända valfria steg i komponenten, som följer ordningen på vilka formulär-element som är gjorda.
- `digi-navigation-pagination`
  - Lagt till en publik metod för att ändra aktivt steg i komponenten.
- `digi-form-error-list`
  - Lade till default värde på linkItems för att undvika JS fel vid tom lista

## [9.4.6] - 2021-09-30

### Ändrat

- `digi-code-example`
  - Ändrat bakgrundsfärg på vertygslistan

## [9.4.5] - 2021-09-28

### Ändrat

- `digi-form-input-search`
  - Lagt till möjlighet att dölja knappen
  - Buggfix, knappens text gick inte att ändra

## [9.4.4] - 2021-09-24

### Ändrat

- `digi-form-error-list`
  - Ändrat färgen på notifikationen från "info" till "danger"
- `digi-notification-alert`
  - Justerat padding för medium storlek
- `digi-form-validation-message`
  - Ändrat default varningsmeddelande till att vara tomt

## [9.4.3] - 2021-09-16

### Ändrat

- `design-tokens`
  - Ändrat färgen `$digi--ui--color--green`
  - Ändrat färgen `$digi--ui--color--pink`

## [9.4.2] - 2021-08-27

### Ändrat

- `digi-form-filter`
  - Lagt till afOnChange EventEmitter

## [9.4.1] - 2021-08-18

### Ändrat

- Lagt till en ny outputTarget `dist-custom-elements-bundle`

## [9.4.0] - 2021-07-02

### Nytt

- `digi-progressbar`
  - Ny komponent

### Ändrat

- `digi-form-radiobutton`
  - Korrigeringar av layout

## [9.3.0] - 2021-06-18

### Ändrat

- `digi-navigation-pagination`
  - Skriv inte ut pagineringen om det bara är en sida. Däremot visas fortfarande texten "Visar 1-15 av XXX".

## [9.2.1] - 2021-06-15

### Ändrat

- Lagt till alla enums under `@digi/core/dist/enum/`
- Korrigerat felaktigheter i alla readmefiler. Främst gällande felaktiga paths till enum-importer.
- Lagt till enum-mappen till disten.

## [9.2.0] - 2021-06-15

### Nytt

- `digi-calendar`
  - Ny komponent
- `digi-typography-meta`
  - Ny komponent
- `digi-logo`
  - Ny komponent
- `digi-util-mutation-observer`
  - Ny komponent
- `digi-form-radiobutton`
  - Ny komponent

### Ändrat

- `digi-form-fieldset`
  - Ändrade padding i fieldset till 0 för att linjera med komponenter utanför fieldset
- `digi-form-error-list`
  - Lagt till mutation observer för att se ändringar i komponenten

## [9.1.0] - 2021-05-25

### Nytt

- `digi-util-breakpoint-observer`
  - Ny komponent
- `digi-form-select`
  - Ny komponent
- `digi-form-fieldset`
  - Ny komponent
- `digi-tag`
  - Ny komponent

### Ändrat

- `digi-navigation-pagination`
  - Felplacering av ikon i knapparna föregående/nästa vid navigering av siffer-knapparna.
  - Centrering av text i siffer-knapparna efter css-reset tagits bort

## [9.0.0] - 2021-05-04

### Ändrat

- `digi-core`
  - Lagt till ny docs tag `@enums` i alla komponenter
- `digi-media-figure`
  - Ändrat enum `MediaFigureAlign` till `MediaFigureAlignment`
  - Ändrat prop `afAlign` till `afAlignment`
- `digi-form-error-list`
  - Ändrat enum `ErrorListHeadingLevel` till `FormErrorListHeadingLevel`

## [8.1.0] - 2021-05-03

### Nytt

- `digi-core`
  - Lagt till alla `enums` till paketet. Dessa hittas under `@digi/core/enum/mitt-enum-namn.emum.ts`

## [8.0.0] - 2021-04-30

### Changed

- `digi-core`
  - Ändrat all dokumentation inne i komponenterna till svenska
- `digi-form-filter`
  - Ändrat event `afOnSubmittedFilter` till `afOnSubmitFilter`
- `digi-form-input-search`
  - Ändrat prop `afButtonLabel` till `afButtonText`
- `digi-navigation-tabs`
  - Ändrat prop `afTablistAriaLabel` till `afAriaLabel`
  - Ändrat prop `afInitActiveTabIndex` till `afInitActiveTab`
- `digi-navigation-pagination`
  - Ändrat prop `afStartPage` till `afInitActivePage`
- `dgi-notification-alert`
  - Ändrat event `afOnCloseAlert` till `afOnClose`

## [7.2.0] - 2021-04-23

### Added

- `digi-notification-alert`
  - Created component
- `digi-form-error-list`
  - Created component
- `digi-link`
  - Created component

### Changed

- `digi-layout-media-object`
  - Included Stretch alignment

## [7.1.0] - 2021-04-19

### Added

- `digi-typography-time`
  - Created component

### Changed

- `Navigation-pagination` `Navigation-tabs` `Form-filter` `Form-textarea` `Form-input` `Form-input-search`
  - Solved style issues after removal of base-reset

## [7.0.0] - 2021-04-19

### Added

- `digi-navigation-context-menu`
  - Created component
- `digi-navigation-breadcrumbs`
  - Created component

### Breaking Changes

- **Removed reset css from all components**
  - Previously, all components included a reset css to prevent global styles from leaking in. This created a massive duplication of css and is now removed. Every consuming app is now responsible for handling css leakage. We are continuously correcting css problems that this may have caused inside the components themselves. If you encounter any problems, please report them to us.
- **Removed `global` folder**
  - All uncompiled scss is being moved to a new library called `@digi/styles`, which will very soon be available from the package manager. In the meantime, you can use the old `@af/digi-core`-package.

### Changed

- `digi-button`
  - Included hasIconSecondary within handleErrors function
  - Added outline and text-align variables
