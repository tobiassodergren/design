import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { ButtonVariation } from './button-variation.enum';
import { ButtonType } from './button-type.enum';
import { ButtonSize } from './button-size.enum';

export default {
	title: 'button/digi-button',
	parameters: {
		actions: {
			handles: ['afOnClick', 'afOnFocus', 'afOnBlur']
		}
	},
	argTypes: {
		'af-variation': enumSelect(ButtonVariation),
		'af-type': enumSelect(ButtonType),
		'af-size': enumSelect(ButtonSize)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-button',
	'af-variation': ButtonVariation.PRIMARY,
	'af-type': ButtonType.BUTTON,
	'af-size': ButtonSize.MEDIUM,
	'af-tabindex': '',
	'af-aria-label': 'An aria label',
	'af-aria-labelledby': 'An aria labelledby',
	'af-aria-controls': 'An aria controls',
	'af-aria-pressed': false,
	'af-aria-expanded': false,
	'af-dir': 'rtl',
	'af-lang': 'ar',
	children: 'I am a button'
};

export const WithIcon = Template.bind({});
WithIcon.args = {
	component: 'digi-button',
	'af-variation': ButtonVariation.FUNCTION,
	'af-type': ButtonType.BUTTON,
	'af-size': ButtonSize.MEDIUM,
	'af-tabindex': '',
	'af-aria-label': 'An aria label',
	'af-aria-labelledby': 'An aria labelledby',
	'af-aria-controls': 'An aria controls',
	'af-aria-pressed': false,
	'af-aria-expanded': false,
	children:
		'<digi-icon-chevron-left slot="icon"></digi-icon-chevron-left>I am a button'
};

export const WithIconSecondary = Template.bind({});
WithIconSecondary.args = {
	component: 'digi-button',
	'af-variation': ButtonVariation.FUNCTION,
	'af-type': ButtonType.BUTTON,
	'af-size': ButtonSize.MEDIUM,
	'af-tabindex': '',
	'af-aria-label': 'An aria label',
	'af-aria-labelledby': 'An aria labelledby',
	'af-aria-controls': 'An aria controls',
	'af-aria-pressed': false,
	'af-aria-expanded': false,
	children:
		'<digi-icon-chevron-right slot="icon-secondary"></digi-icon-chevron-right>I am a button'
};
