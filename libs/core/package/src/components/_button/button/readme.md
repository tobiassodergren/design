# digi-button

This is a general button component. It can be used to create most button variations.

## Notes

1) Tertiary buttons must also have an icon, or it will not work.
2) This was the first component created, and it does not really follow the coding/naming conventions of later components. It will very soon get a much needed do-over.

## Enums

If used in a Typescript environment, you will need to import a couple of enums:

```ts
import { ButtonVariation, ButtonType, ButtonSize } from '@digi/core';
```

<!-- Auto Generated Below -->


## Properties

| Property           | Attribute            | Description                                                                                                              | Type                                     | Default                            |
| ------------------ | -------------------- | ------------------------------------------------------------------------------------------------------------------------ | ---------------------------------------- | ---------------------------------- |
| `afAriaChecked`    | `af-aria-checked`    | Sätter attributet 'aria-checked'. Använd till exempel tillsammans med role="menuitemradio" eller role="menuitemcheckbox" | `boolean`                                | `undefined`                        |
| `afAriaControls`   | `af-aria-controls`   | Sätter attributet 'aria-controls'.                                                                                       | `string`                                 | `undefined`                        |
| `afAriaCurrent`    | `af-aria-current`    | Sätter attributet 'aria-current'.                                                                                        | `string`                                 | `undefined`                        |
| `afAriaExpanded`   | `af-aria-expanded`   | Sätter attributet 'aria-expanded'.                                                                                       | `boolean`                                | `undefined`                        |
| `afAriaHaspopup`   | `af-aria-haspopup`   | Sätter attributet 'aria-haspopup'.                                                                                       | `boolean`                                | `undefined`                        |
| `afAriaLabel`      | `af-aria-label`      | Sätt attributet 'aria-label'.                                                                                            | `string`                                 | `undefined`                        |
| `afAriaLabelledby` | `af-aria-labelledby` | Sätt attributet 'aria-labelledby'.                                                                                       | `string`                                 | `undefined`                        |
| `afAriaPressed`    | `af-aria-pressed`    | Sätter attributet 'aria-pressed'.                                                                                        | `boolean`                                | `undefined`                        |
| `afAutofocus`      | `af-autofocus`       | Sätter attributet 'autofocus'.                                                                                           | `boolean`                                | `undefined`                        |
| `afDir`            | `af-dir`             | Sätter attributet 'dir'.                                                                                                 | `string`                                 | `undefined`                        |
| `afForm`           | `af-form`            | Sätter attributet 'form'.                                                                                                | `string`                                 | `undefined`                        |
| `afFullWidth`      | `af-full-width`      | Sätter knappens bredd till 100%.                                                                                         | `boolean`                                | `false`                            |
| `afId`             | `af-id`              | Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.                                                    | `string`                                 | `randomIdGenerator('digi-button')` |
| `afLang`           | `af-lang`            | Sätter attributet 'lang'.                                                                                                | `string`                                 | `undefined`                        |
| `afRole`           | `af-role`            | Sätter attributet 'role'.                                                                                                | `string`                                 | `undefined`                        |
| `afSize`           | `af-size`            | Sätter knappens storlek. 'medium' är förvalt.                                                                            | `"large" \| "medium" \| "small"`         | `ButtonSize.MEDIUM`                |
| `afTabindex`       | `af-tabindex`        | Sätter attributet 'tabindex'.                                                                                            | `string`                                 | `undefined`                        |
| `afType`           | `af-type`            | Sätt attributet 'type'.                                                                                                  | `"button" \| "reset" \| "submit"`        | `ButtonType.BUTTON`                |
| `afVariation`      | `af-variation`       | Sätt variant. Om varianten är 'tertiary' så krävs även en ikon.                                                          | `"function" \| "primary" \| "secondary"` | `ButtonVariation.PRIMARY`          |


## Events

| Event       | Description                                                                     | Type                      |
| ----------- | ------------------------------------------------------------------------------- | ------------------------- |
| `afOnBlur`  | Buttonelementets 'onblur'-event.                                                | `CustomEvent<any>`        |
| `afOnClick` | Buttonelementets 'onclick'-event.                                               | `CustomEvent<MouseEvent>` |
| `afOnFocus` | Buttonelementets 'onfocus'-event.                                               | `CustomEvent<FocusEvent>` |
| `afOnReady` | När komponenten och slotsen är laddade och initierade så skickas detta eventet. | `CustomEvent<any>`        |


## Methods

### `afMGetButtonElement() => Promise<HTMLButtonElement>`

Hämta en referens till buttonelementet. Bra för att t.ex. sätta fokus programmatiskt.

#### Returns

Type: `Promise<HTMLButtonElement>`




## Slots

| Slot               | Description                                   |
| ------------------ | --------------------------------------------- |
| `"default"`        | Ska vara en textnod                           |
| `"icon"`           | Ska vara en ikon (eller en ikonlik) komponent |
| `"icon-secondary"` | Ska vara en ikon (eller en ikonlik) komponent |


## CSS Custom Properties

| Name                                                    | Description                                                                                                |
| ------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------- |
| `--digi--button--align-items`                           | center;                                                                                                    |
| `--digi--button--border-radius--function--active`       | initial;                                                                                                   |
| `--digi--button--border-radius--function--default`      | initial;                                                                                                   |
| `--digi--button--border-radius--function--focus`        | initial;                                                                                                   |
| `--digi--button--border-radius--function--hover`        | initial;                                                                                                   |
| `--digi--button--border-radius--primary--active`        | var(--digi--border-radius--button);                                                                        |
| `--digi--button--border-radius--primary--default`       | var(--digi--border-radius--button);                                                                        |
| `--digi--button--border-radius--primary--focus`         | var(--digi--border-radius--button);                                                                        |
| `--digi--button--border-radius--primary--hover`         | var(--digi--border-radius--button);                                                                        |
| `--digi--button--border-radius--secondary--active`      | var(--digi--border-radius--button);                                                                        |
| `--digi--button--border-radius--secondary--default`     | var(--digi--border-radius--button);                                                                        |
| `--digi--button--border-radius--secondary--focus`       | var(--digi--border-radius--button);                                                                        |
| `--digi--button--border-radius--secondary--hover`       | var(--digi--border-radius--button);                                                                        |
| `--digi--button--border-style--function--active`        | var(--digi--border-style--secondary);                                                                      |
| `--digi--button--border-style--function--default`       | var(--digi--border-style--secondary);                                                                      |
| `--digi--button--border-style--function--focus`         | var(--digi--border-style--secondary);                                                                      |
| `--digi--button--border-style--function--hover`         | var(--digi--border-style--secondary);                                                                      |
| `--digi--button--border-style--primary--active`         | var(--digi--border-style--primary);                                                                        |
| `--digi--button--border-style--primary--default`        | var(--digi--border-style--primary);                                                                        |
| `--digi--button--border-style--primary--focus`          | var(--digi--border-style--primary);                                                                        |
| `--digi--button--border-style--primary--hover`          | var(--digi--border-style--primary);                                                                        |
| `--digi--button--border-style--secondary--active`       | var(--digi--border-style--primary);                                                                        |
| `--digi--button--border-style--secondary--default`      | var(--digi--border-style--primary);                                                                        |
| `--digi--button--border-style--secondary--focus`        | var(--digi--border-style--primary);                                                                        |
| `--digi--button--border-style--secondary--hover`        | var(--digi--border-style--primary);                                                                        |
| `--digi--button--border-width--function--default`       | initial;                                                                                                   |
| `--digi--button--border-width--function--hover`         | initial;                                                                                                   |
| `--digi--button--border-width--primary--default`        | var(--digi--border-width--button);                                                                         |
| `--digi--button--border-width--primary--hover`          | var(--digi--border-width--button);                                                                         |
| `--digi--button--border-width--secondary--default`      | var(--digi--border-width--button);                                                                         |
| `--digi--button--border-width--secondary--hover`        | var(--digi--border-width--button);                                                                         |
| `--digi--button--color--background--function--active`   | transparent;                                                                                               |
| `--digi--button--color--background--function--default`  | transparent;                                                                                               |
| `--digi--button--color--background--function--focus`    | transparent;                                                                                               |
| `--digi--button--color--background--function--hover`    | transparent;                                                                                               |
| `--digi--button--color--background--primary--active`    | var(--digi--color--background--inverted-3);                                                                |
| `--digi--button--color--background--primary--default`   | var(--digi--color--background--inverted-1);                                                                |
| `--digi--button--color--background--primary--focus`     | var(--digi--color--background--inverted-2);                                                                |
| `--digi--button--color--background--primary--hover`     | var(--digi--color--background--inverted-2);                                                                |
| `--digi--button--color--background--secondary--active`  | var(--digi--color--background--tertiary);                                                                  |
| `--digi--button--color--background--secondary--default` | var(--digi--color--background--primary);                                                                   |
| `--digi--button--color--background--secondary--focus`   | var(--digi--color--background--secondary);                                                                 |
| `--digi--button--color--background--secondary--hover`   | var(--digi--color--background--neutral-1);                                                                 |
| `--digi--button--color--border--function--active`       | initial;                                                                                                   |
| `--digi--button--color--border--function--default`      | initial;                                                                                                   |
| `--digi--button--color--border--function--focus`        | initial;                                                                                                   |
| `--digi--button--color--border--function--hover`        | initial;                                                                                                   |
| `--digi--button--color--border--primary--active`        | var(--digi--color--border--quaternary);                                                                    |
| `--digi--button--color--border--primary--default`       | var(--digi--color--border--secondary);                                                                     |
| `--digi--button--color--border--primary--focus`         | var(--digi--color--border--tertiary);                                                                      |
| `--digi--button--color--border--primary--hover`         | var(--digi--color--border--informative);                                                                   |
| `--digi--button--color--border--secondary--active`      | var(--digi--color--border--primary);                                                                       |
| `--digi--button--color--border--secondary--default`     | var(--digi--color--border--secondary);                                                                     |
| `--digi--button--color--border--secondary--focus`       | var(--digi--color--border--primary);                                                                       |
| `--digi--button--color--border--secondary--hover`       | var(--digi--color--border--informative);                                                                   |
| `--digi--button--color--icon--function--active`         | var(--digi--color--text--link);                                                                            |
| `--digi--button--color--icon--function--default`        | var(--digi--color--text--link);                                                                            |
| `--digi--button--color--icon--function--focus`          | var(--digi--color--text--link);                                                                            |
| `--digi--button--color--icon--function--hover`          | var(--digi--color--text--link);                                                                            |
| `--digi--button--color--icon--primary--active`          | var(--digi--color--text--inverted);                                                                        |
| `--digi--button--color--icon--primary--default`         | var(--digi--color--text--inverted);                                                                        |
| `--digi--button--color--icon--primary--focus`           | var(--digi--color--text--inverted);                                                                        |
| `--digi--button--color--icon--primary--hover`           | var(--digi--color--text--inverted);                                                                        |
| `--digi--button--color--icon--secondary--active`        | var(--digi--color--text--secondary);                                                                       |
| `--digi--button--color--icon--secondary--default`       | var(--digi--color--text--secondary);                                                                       |
| `--digi--button--color--icon--secondary--focus`         | var(--digi--color--text--secondary);                                                                       |
| `--digi--button--color--icon--secondary--hover`         | var(--digi--color--text--secondary);                                                                       |
| `--digi--button--color--text--function--active`         | var(--digi--color--text--secondary);                                                                       |
| `--digi--button--color--text--function--default`        | var(--digi--color--text--link);                                                                            |
| `--digi--button--color--text--function--focus`          | var(--digi--color--text--secondary);                                                                       |
| `--digi--button--color--text--function--hover`          | var(--digi--color--text--link-hover);                                                                      |
| `--digi--button--color--text--primary--active`          | var(--digi--color--text--inverted);                                                                        |
| `--digi--button--color--text--primary--default`         | var(--digi--color--text--inverted);                                                                        |
| `--digi--button--color--text--primary--focus`           | var(--digi--color--text--inverted);                                                                        |
| `--digi--button--color--text--primary--hover`           | var(--digi--color--text--inverted);                                                                        |
| `--digi--button--color--text--secondary--active`        | var(--digi--color--text--secondary);                                                                       |
| `--digi--button--color--text--secondary--default`       | var(--digi--color--text--secondary);                                                                       |
| `--digi--button--color--text--secondary--focus`         | var(--digi--color--text--secondary);                                                                       |
| `--digi--button--color--text--secondary--hover`         | var(--digi--color--text--secondary);                                                                       |
| `--digi--button--cursor`                                | pointer;                                                                                                   |
| `--digi--button--display`                               | initial;                                                                                                   |
| `--digi--button--font-family`                           | var(--digi--global--typography--font-family--default);                                                     |
| `--digi--button--font-size--large`                      | var(--digi--global--typography--font-size--interaction-large);                                             |
| `--digi--button--font-size--medium`                     | var(--digi--global--typography--font-size--interaction-medium);                                            |
| `--digi--button--font-size--small`                      | var(--digi--global--typography--font-size--interaction-small);                                             |
| `--digi--button--font-weight--default`                  | var(--digi--typography--button--font-weight--desktop);                                                     |
| `--digi--button--font-weight--function`                 | var(--digi--typography--label--font-weight--desktop);                                                      |
| `--digi--button--icon--spacing`                         | var(--digi--gutter--large);                                                                                |
| `--digi--button--justify-content--default`              | initial;                                                                                                   |
| `--digi--button--justify-content--full-width`           | center;                                                                                                    |
| `--digi--button--min-height--large`                     | unset;                                                                                                     |
| `--digi--button--min-height--small`                     | unset;                                                                                                     |
| `--digi--button--outline--default`                      | initial;                                                                                                   |
| `--digi--button--outline--focus`                        | var(--digi--color--border--focus) var(--digi--border-style--primary) var(--digi--border-width--secondary); |
| `--digi--button--padding--large`                        | var(--digi--gutter--button-block-large) var(--digi--gutter--button-inline-large);                          |
| `--digi--button--padding--medium`                       | var(--digi--gutter--button-block-medium) var(--digi--gutter--button-inline-medium);                        |
| `--digi--button--padding--small`                        | var(--digi--gutter--button-block-small) var(--digi--gutter--button-inline-small);                          |
| `--digi--button--text-align--default`                   | initial;                                                                                                   |
| `--digi--button--text-align--full-width`                | center;                                                                                                    |
| `--digi--button--width`                                 | initial;                                                                                                   |


## Dependencies

### Used by

 - [digi-bar-chart](../../_chart/bar-chart)
 - [digi-calendar-week-view](../../_calendar/week-view)
 - [digi-chart-line](../../_chart/chart-line)
 - [digi-code-block](../../_code/code-block)
 - [digi-code-example](../../_code/code-example)
 - [digi-expandable-accordion](../../_expandable/expandable-accordion)
 - [digi-form-filter](../../_form/form-filter)
 - [digi-form-input-search](../../_form/form-input-search)
 - [digi-navigation-context-menu](../../_navigation/navigation-context-menu)
 - [digi-navigation-sidebar](../../_navigation/navigation-sidebar)
 - [digi-navigation-sidebar-button](../../_navigation/navigation-sidebar-button)
 - [digi-tag](../../_tag/tag)

### Graph
```mermaid
graph TD;
  digi-bar-chart --> digi-button
  digi-calendar-week-view --> digi-button
  digi-chart-line --> digi-button
  digi-code-block --> digi-button
  digi-code-example --> digi-button
  digi-expandable-accordion --> digi-button
  digi-form-filter --> digi-button
  digi-form-input-search --> digi-button
  digi-navigation-context-menu --> digi-button
  digi-navigation-sidebar --> digi-button
  digi-navigation-sidebar-button --> digi-button
  digi-tag --> digi-button
  style digi-button fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
