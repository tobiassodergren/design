# digi-code-block

This creates a syntax highlighted code block element. It supports dark- and light theme and a number of different languages. The component has a toolbar with buttons for copying the code directly to the clipboard.

## Enums

If used in a Typescript environment, you will need to import a couple of enums:

```ts
import { CodeBlockVariation, CodeBlockLanguage } from '@digi/core';
```

<!-- Auto Generated Below -->


## Properties

| Property        | Attribute         | Description                                                  | Type                                                                                                                                                                                                                                                                       | Default                   |
| --------------- | ----------------- | ------------------------------------------------------------ | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------- |
| `afCode`        | `af-code`         | En sträng med den kod som du vill visa upp.                  | `string`                                                                                                                                                                                                                                                                   | `undefined`               |
| `afHideToolbar` | `af-hide-toolbar` | Ta bort knappar för att till exempel kopiera kod             | `boolean`                                                                                                                                                                                                                                                                  | `false`                   |
| `afLang`        | `af-lang`         | Sätt språk inuti code-elementet. 'en' (engelska) är förvalt. | `string`                                                                                                                                                                                                                                                                   | `'en'`                    |
| `afLanguage`    | `af-language`     | Sätt programmeringsspråk. 'html' är förvalt.                 | `CodeBlockLanguage.BASH \| CodeBlockLanguage.CSS \| CodeBlockLanguage.GIT \| CodeBlockLanguage.HTML \| CodeBlockLanguage.JAVASCRIPT \| CodeBlockLanguage.JSON \| CodeBlockLanguage.JSX \| CodeBlockLanguage.SCSS \| CodeBlockLanguage.TSX \| CodeBlockLanguage.TYPESCRIPT` | `CodeBlockLanguage.HTML`  |
| `afVariation`   | `af-variation`    | Sätt färgtemat. Kan vara ljust eller mörkt.                  | `CodeBlockVariation.DARK \| CodeBlockVariation.LIGHT`                                                                                                                                                                                                                      | `CodeBlockVariation.DARK` |


## CSS Custom Properties

| Name                                | Description                                      |
| ----------------------------------- | ------------------------------------------------ |
| `--digi--code-block--background`    | var(--digi--color--background--primary);         |
| `--digi--code-block--border`        | 1px solid var(--digi--color--border--neutral-3); |
| `--digi--code-block--border-radius` | 0.3rem;                                          |


## Dependencies

### Used by

 - [digi-code-example](../code-example)

### Depends on

- [digi-button](../../_button/button)
- [digi-icon](../../_icon/icon)

### Graph
```mermaid
graph TD;
  digi-code-block --> digi-button
  digi-code-block --> digi-icon
  digi-code-example --> digi-code-block
  style digi-code-block fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
