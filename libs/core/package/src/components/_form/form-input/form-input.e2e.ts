import { newE2EPage } from '@stencil/core/testing';

describe('digi-form-input', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-form-input></digi-form-input>');

    const element = await page.find('digi-form-input');
    expect(element).toHaveClass('hydrated');
  });
});
