import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { FormInputType } from './form-input-type.enum';
import { FormInputValidation } from './form-input-validation.enum';
import { FormInputVariation } from './form-input-variation.enum';
import { FormInputButtonVariation } from './form-input-button-variation.enum';

export default {
	title: 'form/digi-form-input',
	parameters: {
		actions: {
			handles: [
				'afOnChange',
				'afOnBlur',
				'afOnKeyup',
				'afOnFocus',
				'afOnFocusout',
				'afOnInput',
				'afOnDirty',
				'afOnTouched'
			]
		}
	},
	argTypes: {
		'af-type': enumSelect(FormInputType),
		'af-variation': enumSelect(FormInputVariation),
		'af-validation': enumSelect(FormInputValidation),
		'af-button-variation': enumSelect(FormInputButtonVariation)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-form-input',
	'af-label': 'A label is required',
	'af-label-description': '',
	'af-type': FormInputType.TEXT,
	'af-variation': FormInputVariation.MEDIUM,
	'af-button-variation': FormInputButtonVariation.PRIMARY,
	'af-name': '',
	'af-id': null,
	'af-maxlength': null,
	'af-minlength': null,
	'af-required': false,
	'af-announce-if-optional': false,
	value: null,
	'af-value': null,
	'af-validation': FormInputValidation.NEUTRAL,
	'af-validation-text': '',
	'af-role': '',
	'af-autocomplete': '',
	'af-min': '',
	'af-max': '',
	'af-list': '',
	'af-step': '',
	'af-dirname': '',
	'af-aria-autocomplete': '',
	'af-aria-activedescendant': '',
	'af-aria-labelledby': '',
	'af-aria-describedby': '',
	children: ''
};

export const WithButton = Template.bind({});
WithButton.args = {
	...Standard.args,
	children: '<digi-button slot="button">Button</digi-button>'
};
