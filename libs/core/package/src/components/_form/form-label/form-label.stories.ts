import { enumSelect, Template } from '../../../../../../shared/utils/src';

export default {
	title: 'form/digi-form-label'
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-form-label',
	'af-label': 'A label is required',
	'af-id': null,
	'af-for': 'For attribute required',
	'af-description': '',
	'af-required': false,
	'af-announce-if-optional': false,
	/* html */
	children: `
    <digi-icon af-name="exclamation-circle-filled" slot="actions"></digi-icon>`
};
