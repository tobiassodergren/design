import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { FormTextareaVariation } from './form-textarea-variation.enum';
import { FormTextareaValidation } from './form-textarea-validation.enum';

export default {
	title: 'form/digi-form-textarea',
	parameters: {
		actions: {
			handles: [
				'afOnChange',
				'afOnBlur',
				'afOnKeyup',
				'afOnFocus',
				'afOnFocusout',
				'afOnInput',
				'afOnDirty',
				'afOnTouched'
			]
		}
	},
	argTypes: {
		'af-variation': enumSelect(FormTextareaVariation),
		'af-validation': enumSelect(FormTextareaValidation)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-form-textarea',
	'af-label': 'A label is required',
	'af-label-description': '',
	'af-variation': FormTextareaVariation.MEDIUM,
	'af-name': '',
	'af-id': null,
	'af-maxlength': null,
	'af-minlength': null,
	'af-required': false,
	'af-announce-if-optional': false,
	value: '',
	'af-value': '',
	'af-validation': FormTextareaValidation.NEUTRAL,
	'af-validation-text': '',
	'af-role': '',
	'af-aria-activedescendant': '',
	'af-aria-labelledby': '',
	'af-aria-describedby': ''
};
