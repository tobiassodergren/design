import { Template } from '../../../../../../shared/utils/src';

export default {
	title: 'icon/digi-icon'
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-icon',
	'af-title': '',
	'af-desc': '',
	'af-name': 'validation-success',
	children: ''
};
