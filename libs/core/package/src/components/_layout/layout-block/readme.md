# digi-layout-block

This is a layout component meant for creating full-width blocks with an inner container. It is very useful as a basic building block for pages, which can be easily built with a few of these.

The inner container uses `digi-layout-container` and accepts all the same variations (by using `af-container`). It can also have a few different standard background colors (using `af-variation`).

## Enums

If used in a Typescript environment, you will need to import a couple of enums:

```ts
import { LayoutBlockVariation, LayoutBlockContainer } from '@digi/core';
```

<!-- Auto Generated Below -->


## Properties

| Property            | Attribute             | Description                                                                                           | Type                                                                                                                                                                                                 | Default                        |
| ------------------- | --------------------- | ----------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------ |
| `afContainer`       | `af-container`        | Sätter den inre containervarianten, eller tar bort den helt. Kan vara 'static', 'fluid' eller 'none'. | `LayoutBlockContainer.FLUID \| LayoutBlockContainer.NONE \| LayoutBlockContainer.STATIC`                                                                                                             | `LayoutBlockContainer.STATIC`  |
| `afMarginBottom`    | `af-margin-bottom`    | Lägger till marginal i botten på containern                                                           | `boolean`                                                                                                                                                                                            | `undefined`                    |
| `afMarginTop`       | `af-margin-top`       | Lägger till marginal i toppen på containern                                                           | `boolean`                                                                                                                                                                                            | `undefined`                    |
| `afVariation`       | `af-variation`        | Sätter variant. Kontrollerar bakgrundsfärgen.                                                         | `LayoutBlockVariation.PRIMARY \| LayoutBlockVariation.PROFILE \| LayoutBlockVariation.SECONDARY \| LayoutBlockVariation.SYMBOL \| LayoutBlockVariation.TERTIARY \| LayoutBlockVariation.TRANSPARENT` | `LayoutBlockVariation.PRIMARY` |
| `afVerticalPadding` | `af-vertical-padding` | Lägger till vertikal padding inuti containern                                                         | `boolean`                                                                                                                                                                                            | `undefined`                    |


## Slots

| Slot        | Description                 |
| ----------- | --------------------------- |
| `"default"` | Kan innehålla vad som helst |


## CSS Custom Properties

| Name                                          | Description                                 |
| --------------------------------------------- | ------------------------------------------- |
| `--digi--layout-block--background--primary`   | var(--digi--color--background--primary);    |
| `--digi--layout-block--background--profile`   | var(--digi--color--background--inverted-1); |
| `--digi--layout-block--background--secondary` | var(--digi--color--background--secondary);  |
| `--digi--layout-block--background--symbol`    | var(--digi--color--background--neutral-5);  |
| `--digi--layout-block--background--tertiary`  | var(--digi--color--background--neutral-4);  |


## Dependencies

### Depends on

- [digi-layout-container](../layout-container)

### Graph
```mermaid
graph TD;
  digi-layout-block --> digi-layout-container
  style digi-layout-block fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
