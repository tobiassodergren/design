import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { LayoutColumnsElement } from './layout-columns-element.enum';
import { LayoutColumnsVariation } from './layout-columns-variation.enum';

export default {
	title: 'layout/digi-layout-columns',
	argTypes: {
		'af-element': enumSelect(LayoutColumnsElement),
		'af-variation': enumSelect(LayoutColumnsVariation)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-layout-columns',
	'af-element': LayoutColumnsElement.DIV,
	'af-variation': null,
	/* html */
	children: `
    <digi-media-image
        af-src="https://picsum.photos/id/237/1280/400"
        af-alt="En bild"
        af-width="1280"
        af-height="400"
    ></digi-media-image>
    <digi-media-image
        af-src="https://picsum.photos/id/237/1280/400"
        af-alt="En bild"
        af-width="1280"
        af-height="400"
    ></digi-media-image>
    <digi-media-image
        af-src="https://picsum.photos/id/237/1280/400"
        af-alt="En bild"
        af-width="1280"
        af-height="400"
    ></digi-media-image>
    <digi-media-image
        af-src="https://picsum.photos/id/237/1280/400"
        af-alt="En bild"
        af-width="1280"
        af-height="400"
    ></digi-media-image>`
};
