# digi-layout-columns

This is a versatile layout component for creating even-spaced columns. By default it will place all its children on the same row, but by setting `--digi--layout-columns--min-width` to a desired value it will start wrapping as soon as its children becomes smaller. It also accepts a few defaults, which you can provide using `af-variation`.

It is also possible to change the wrapper's element type (default is `<div>`). See documentation below for how this works.

## Enums

If used in a Typescript environment, you will need to import a couple of enums:

```ts
import { LayoutColumnsVariation, LayoutColumnsElement } from '@digi/core';
```

<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description                                                                       | Type                                                                                       | Default                    |
| ------------- | -------------- | --------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------ | -------------------------- |
| `afElement`   | `af-element`   | Sätter elementtypen på det omslutande elementet. Kan vara 'div', 'ul' eller 'ol'. | `LayoutColumnsElement.DIV \| LayoutColumnsElement.OL \| LayoutColumnsElement.UL`           | `LayoutColumnsElement.DIV` |
| `afVariation` | `af-variation` | Sätter förvalda kolumnvarianter. Kan vara 'two' eller 'three'.                    | `LayoutColumnsVariation.ONE \| LayoutColumnsVariation.THREE \| LayoutColumnsVariation.TWO` | `undefined`                |


## Slots

| Slot        | Description                 |
| ----------- | --------------------------- |
| `"default"` | Kan innehålla vad som helst |


## CSS Custom Properties

| Name                                  | Description                   |
| ------------------------------------- | ----------------------------- |
| `--digi--layout-columns--gap--column` | var(--digi--gutter--largest); |
| `--digi--layout-columns--gap--row`    | var(--digi--gutter--largest); |
| `--digi--layout-columns--min-width`   | 0;                            |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
