# digi-layout-media-object

This is the classic media object ([see here](https://css-tricks.com/media-object-bunch-ways/) for more information). It is most often used for images with text on the side, but can really be used for positioning anything next to each other.

## Enums

If used in a Typescript environment, you will need to import a couple of enums:

```ts
import { LayoutMediaObjectAlignment } from '@digi/core';
```

<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description                                                                          | Type                                        | Default                            |
| ------------- | -------------- | ------------------------------------------------------------------------------------ | ------------------------------------------- | ---------------------------------- |
| `afAlignment` | `af-alignment` | Sätter justeringen av innehållet. Kan vara 'center', 'start', 'end' eller 'stretch'. | `"center" \| "end" \| "start" \| "stretch"` | `LayoutMediaObjectAlignment.START` |


## Slots

| Slot        | Description                                                              |
| ----------- | ------------------------------------------------------------------------ |
| `"default"` | kan innehålla vad som helst. Vanligen är det textinnehåll.               |
| `"media"`   | Bör innehålla någon sorts mediakompinent (t.ex. en bild eller en video). |


## CSS Custom Properties

| Name                                     | Description                   |
| ---------------------------------------- | ----------------------------- |
| `--digi--layout-media-object--alignment` | flex-start;                   |
| `--digi--layout-media-object--flex-wrap` | wrap;                         |
| `--digi--layout-media-object--gutter`    | var(--digi--gutter--largest); |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
