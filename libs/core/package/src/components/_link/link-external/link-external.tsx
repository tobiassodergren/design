import { Component, Event, EventEmitter, Prop, h } from '@stencil/core';
import { LinkVariation } from '../link/link-variation.enum';
import { LinkExternalVariation } from './link-external-variation.enum';
import { Element, Host, HTMLStencilElement } from '@stencil/core/internal';

/**
 * @slot default - Ska vara en textnod
 *
 * @enums LinkExternalVariation - link-external-variation.enum.ts
 *@swedishName Extern länk
 */
@Component({
	tag: 'digi-link-external',
	styleUrls: ['link-external.scss'],
	scoped: true
})
export class LinkExternal {
	@Element() hostElement: HTMLStencilElement;

	/**
	 * Sätter attributet 'href'.
	 * @en Set `href` attribute.
	 */
	@Prop() afHref!: string;

		/**
	 * Sätter attributet 'aria-describedby'.
	 * @en Sets `aria-describedby` attribute.
	 */
		@Prop() afDescribedby: string;

	/**
	 * Sätter variant. Kan vara 'small' eller 'large'.
	 * @en Sets the variation of the link.
	 */
	@Prop() afVariation: `${LinkExternalVariation}` = LinkExternalVariation.SMALL;

	/**
	 * Sätter attributet 'target'. Om värdet är '_blank' så lägger komponenten automatikt till ett ´ref´-attribut med 'noopener noreferrer'.
	 * @en Set `target` attribute. If value is '_blank', the component automatically adds a 'ref' attribute with 'noopener noreferrer'.
	 */
	@Prop() afTarget: string;



	/**
	 * Länkelementets 'onclick'-event.
	 * @en The link element's 'onclick' event.
	 */
	@Event() afOnClick: EventEmitter<MouseEvent>;

	clickLinkHandler(e: any) {
		(e as CustomEvent).stopImmediatePropagation();
		this.afOnClick.emit(e.detail);
	}

	get cssModifiers() {
		return {
			[`digi-link-external--variation-${this.afVariation}`]: !!this.afVariation
		  };
	}

	render() {
		return (
      <Host>
          <digi-link
            class={{
              'digi-link-external': true,
              ...this.cssModifiers
            }}
            afVariation={this.afVariation as LinkVariation}
            afHref={this.afHref}
            onAfOnClick={(e) => this.clickLinkHandler(e)}
            afTarget={this.afTarget}
						afDescribedby={this.afDescribedby}
          >
            <digi-icon
              class="digi-link-external__icon"
              aria-hidden="true"
              slot="icon"
              afName={`external-link-alt`}
            ></digi-icon>
            <slot></slot>
          </digi-link>
      </Host>
		);
	}
}
