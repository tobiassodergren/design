import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { LinkInternalVariation } from './link-internal-variation.enum';

export default {
	title: 'link/digi-link-internal',
	parameters: {
		actions: {
			handles: ['afOnClick']
		}
	},
	argTypes: {
		'af-variation': enumSelect(LinkInternalVariation)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-link-internal',
	'af-href': 'A link is required',
	'af-variation': LinkInternalVariation.SMALL,
	/* html */
	children: 'I am an internal link'
};
