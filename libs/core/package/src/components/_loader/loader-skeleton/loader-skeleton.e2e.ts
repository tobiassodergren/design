import { newE2EPage } from '@stencil/core/testing';

describe('loader-skeleton', () => {
	it('renders', async () => {
		const page = await newE2EPage();

		await page.setContent('<loader-skeleton></loader-skeleton>');
		const element = await page.find('loader-skeleton');
		expect(element).toHaveClass('hydrated');
	});

	it('renders changes to the name data', async () => {
		const page = await newE2EPage();

		await page.setContent('<loader-skeleton></loader-skeleton>');
		const component = await page.find('loader-skeleton');
		const element = await page.find('loader-skeleton >>> div');
		expect(element.textContent).toEqual(`Hello, World! I'm `);

		component.setProperty('first', 'James');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James`);

		component.setProperty('last', 'Quincy');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James Quincy`);

		component.setProperty('middle', 'Earl');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James Earl Quincy`);
	});
});
