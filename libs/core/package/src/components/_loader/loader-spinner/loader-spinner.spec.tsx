import { newSpecPage } from '@stencil/core/testing';
import { LoaderSpinner } from './loader-spinner';

describe('loader-spinner', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [LoaderSpinner],
			html: '<loader-spinner></loader-spinner>'
		});
		expect(root).toEqualHtml(`
      <loader-spinner>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </loader-spinner>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [LoaderSpinner],
			html: `<loader-spinner first="Stencil" last="'Don't call me a framework' JS"></loader-spinner>`
		});
		expect(root).toEqualHtml(`
      <loader-spinner first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </loader-spinner>
    `);
	});
});
