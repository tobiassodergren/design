import { Component, Prop, h } from '@stencil/core';
import { MediaFigureAlignment } from './media-figure-alignment.enum';

/**
 * @slot default - Ska vara en bild (eller bildlik) komponent. Helst en `digi-media-image`.
 *
 * @enums MediaFigureAlignment - media-figure-alignment.enum.ts
 * @swedishName Mediefigur
 */
@Component({
  tag: 'digi-media-figure',
  styleUrls: ['media-figure.scss'],
  scoped: true,
})
export class MediaFigure {
  /**
   * Lägger till ett valfritt figcaptionelement
   * @en Adds an optional figcaption element
   */
  @Prop() afFigcaption: string;

  /**
   * Justering av bild och text. Kan vara 'start', 'center' eller 'end'
   * @en Alignment of image and caption. Value can be 'start', 'center' or 'end'.
   */
  @Prop() afAlignment: MediaFigureAlignment = MediaFigureAlignment.START;

  get cssModifiers() {
    return {
      'digi-media-figure--align-start': this.afAlignment === 'start',
      'digi-media-figure--align-center': this.afAlignment === 'center',
      'digi-media-figure--align-end': this.afAlignment === 'end',
    };
  }

  render() {
    return (
      <figure
        class={{
          'digi-media-figure': true,
          ...this.cssModifiers,
        }}
      >
        <div class="digi-media-figure__image">
          <slot></slot>
        </div>
        {this.afFigcaption && (
          <figcaption class="digi-media-figure__figcaption">
            {this.afFigcaption}
          </figcaption>
        )}
      </figure>
    );
  }
}
