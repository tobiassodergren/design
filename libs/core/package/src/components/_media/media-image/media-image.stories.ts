import { enumSelect, Template } from '../../../../../../shared/utils/src';

export default {
	title: 'media/digi-media-image',
	parameters: {
		actions: {
			handles: ['afOnLoad']
		}
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-media-image',
	'af-observer-options': '',
	'af-src': '',
	'af-srcset': '',
	'af-alt': 'Alt text is required',
	'af-title': '',
	'af-aria-label': '',
	'af-width': '',
	'af-height': '',
	'af-fullwidth': false,
	'af-unlazy': false,
	children: ''
};
