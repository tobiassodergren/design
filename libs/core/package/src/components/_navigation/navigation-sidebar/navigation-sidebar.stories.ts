import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { NavigationSidebarPosition } from './navigation-sidebar-position.enum';
import { NavigationSidebarVariation } from './navigation-sidebar-variation.enum';
import { NavigationSidebarMobilePosition } from './navigation-sidebar-mobile-position.enum';
import { NavigationSidebarMobileVariation } from './navigation-sidebar-mobile-variation.enum';
import { NavigationSidebarHeadingLevel } from './navigation-sidebar-heading-level.enum';
import { NavigationSidebarCloseButtonPosition } from './navigation-sidebar-close-button-position.enum';

export default {
	title: 'navigation/digi-navigation-sidebar',
	parameters: {
		actions: {
			handles: ['afOnClose', 'afOnEsc', 'afOnBackdropClick', 'afOnToggle']
		}
	},
	argTypes: {
		'af-position': enumSelect(NavigationSidebarPosition),
		'af-variation': enumSelect(NavigationSidebarVariation),
		'af-mobile-position': enumSelect(NavigationSidebarMobilePosition),
		'af-mobile-variation': enumSelect(NavigationSidebarMobileVariation),
		'af-heading-level': enumSelect(NavigationSidebarHeadingLevel),
		'af-close-button-position': enumSelect(NavigationSidebarCloseButtonPosition)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-navigation-sidebar',
	'af-position': NavigationSidebarPosition.START,
	'af-variation': NavigationSidebarVariation.OVER,
	'af-mobile-position': null,
	'af-mobile-variation': NavigationSidebarMobileVariation.DEFAULT,
	'af-heading': '',
	'af-heading-level': NavigationSidebarHeadingLevel.H2,
	'af-close-button-position': NavigationSidebarCloseButtonPosition.START,
	'af-sticky-header': false,
	'af-close-button-text': '',
	'af-close-button-aria-label': '',
	'af-hide-header': false,
	'af-focusable-element': '',
	'af-close-focusable-element': '',
	'af-active': true,
	'af-backdrop': false,
	'af-id': null,
	/* html */
	children: `
    <digi-navigation-vertical-menu>
        <ul>
            <li>
            <digi-navigation-vertical-menu-item af-text="About" af-active-subnav="false"></digi-navigation-vertical-menu-item>
            <ul>
                <li>
                <digi-navigation-vertical-menu-item af-text="About 1" af-href="/about/about-1"></digi-navigation-vertical-menu-item></digi-navigation-vertical-menu-item>
                </li>
                <li>
                <digi-navigation-vertical-menu-item af-text="About 2" af-href="/about/about-2"></digi-navigation-vertical-menu-item></digi-navigation-vertical-menu-item>
                </li>
            </ul>
            </li>
            <li>
            <digi-navigation-vertical-menu-item af-text="Pages" af-active-subnav="false"></digi-navigation-vertical-menu-item>
            <ul>
                <li>
                <digi-navigation-vertical-menu-item af-text="Pages 1" af-href="/pages/pages-1"></digi-navigation-vertical-menu-item>
                </li>
                <li>
                <digi-navigation-vertical-menu-item af-text="Pages 2" af-href="/pages/pages-2"></digi-navigation-vertical-menu-item>
                </li>
                <li>
                <digi-navigation-vertical-menu-item af-text="Pages 3" af-href="/grafisk-profil/pages-3"></digi-navigation-vertical-menu-item>
                </li>
            </ul>
            </li>
            <li>
            <digi-navigation-vertical-menu-item af-text="Contact" af-href="/contact"></digi-navigation-vertical-menu-item>
            </li>
        </ul>
    </digi-navigation-vertical-menu>`
};
