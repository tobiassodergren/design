import { enumSelect, Template } from '../../../../../../shared/utils/src';

export default {
	title: 'navigation/digi-navigation-vertical-menu-item',
	parameters: {
		actions: {
			handles: ['afOnClick']
		}
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-navigation-vertical-menu-item',
	'af-text': '',
	'af-href': '',
	'af-active-subnav': false,
	'af-active': false,
	'af-id': null,
	'af-override-link': false,
	'af-has-subnav': false,
	children: ''
};
