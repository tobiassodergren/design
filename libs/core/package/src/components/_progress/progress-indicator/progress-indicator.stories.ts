import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { ProgressIndicatorRole } from './progress-indicator-role.enum';
import { ProgressIndicatorVariation } from './progress-indicator-variation.enum';

export default {
	title: 'progress/digi-progress-indicator',
	argTypes: {
		'af-role': enumSelect(ProgressIndicatorRole),
		'af-variation': enumSelect(ProgressIndicatorVariation)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-progress-indicator',
	'af-completed-steps': null,
	'af-total-steps': null,
	'af-steps-separator': 'av',
	'af-steps-label': 'frågor besvarade',
	'af-role': ProgressIndicatorRole.STATUS,
	'af-variation': ProgressIndicatorVariation.PRIMARY,
	'af-id': null,
	children: ''
};
