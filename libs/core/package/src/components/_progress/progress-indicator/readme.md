# digi-progressbar



<!-- Auto Generated Below -->


## Properties

| Property            | Attribute             | Description                                                                                                                                                                                           | Type                                                                         | Default                                        |
| ------------------- | --------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------- | ---------------------------------------------- |
| `afCompletedSteps`  | `af-completed-steps`  | Sätter antal färdiga steg                                                                                                                                                                             | `number`                                                                     | `undefined`                                    |
| `afErrorStep`       | `af-error-step`       | Sätter ett steg där något gick fel                                                                                                                                                                    | `number`                                                                     | `undefined`                                    |
| `afId`              | `af-id`               | Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.                                                                                                                                 | `string`                                                                     | `randomIdGenerator('digi-progress-indicator')` |
| `afRole`            | `af-role`             | Sätter attributet 'role'. Förvalt är 'status'. Använder du komponenten både före och efter ett formulär så ange bara den ena som 'status' för att undvika att skärmläsare läser upp texten två gånger | `ProgressIndicatorRole.NONE \| ProgressIndicatorRole.STATUS`                 | `ProgressIndicatorRole.STATUS`                 |
| `afStepHeading`     | `af-step-heading`     | Sätter en beskrivande rubrik för förloppsmätaren                                                                                                                                                      | `string`                                                                     | `undefined`                                    |
| `afStepNextLabel`   | `af-step-next-label`  | Sätter en beskrivande rubrik för förloppsmätaren                                                                                                                                                      | `string`                                                                     | `undefined`                                    |
| `afStepNextText`    | `af-step-next-text`   | Beskrivande text om vad som kommer härnäst                                                                                                                                                            | `string`                                                                     | `_t.progress.questions_next`                   |
| `afStepText`        | `af-step-text`        | Text som visar "Steg" innan det antalet steg visas Som standard används "Steg", men kan ändras för exempelvis annat språk.                                                                            | `string`                                                                     | `_t.step`                                      |
| `afStepsSeparator`  | `af-steps-separator`  | Text emellan antal färdiga och totala antalet steg. Som standard används "av", men kan ändras för exempelvis annat språk.                                                                             | `string`                                                                     | `_t.of`                                        |
| `afTotalSteps`      | `af-total-steps`      | Sätter totala antalet steg                                                                                                                                                                            | `number`                                                                     | `undefined`                                    |
| `afValidationLabel` | `af-validation-label` | Beskrivande text om något går fel i processen                                                                                                                                                         | `string`                                                                     | `undefined`                                    |
| `afVariation`       | `af-variation`        | Sätter variant. Kan vara 'primary' och 'secondary'.                                                                                                                                                   | `ProgressIndicatorVariation.PRIMARY \| ProgressIndicatorVariation.SECONDARY` | `ProgressIndicatorVariation.PRIMARY`           |
| `afWarningStep`     | `af-warning-step`     | Sätter ett steg där det krävs ytterligare åtgärder                                                                                                                                                    | `number`                                                                     | `undefined`                                    |
| `radius`            | `radius`              |                                                                                                                                                                                                       | `number`                                                                     | `42`                                           |
| `stroke`            | `stroke`              |                                                                                                                                                                                                       | `number`                                                                     | `6`                                            |


## CSS Custom Properties

| Name                                                               | Description                                               |
| ------------------------------------------------------------------ | --------------------------------------------------------- |
| `--digi--progress-indicator--border-color`                         | var(--digi--color--border--neutral-5);                    |
| `--digi--progress-indicator--border-color--active`                 | var(--digi--color--border--success);                      |
| `--digi--progress-indicator--border-color--secondary--active`      | var(--digi--color--border--secondary);                    |
| `--digi--progress-indicator--border-width`                         | var(--digi--border-width--primary);                       |
| `--digi--progress-indicator--gap`                                  | var(--digi--padding--largest);                            |
| `--digi--progress-indicator--gap--large`                           | var(--digi--padding--largest-3);                          |
| `--digi--progress-indicator--gap--medium`                          | var(--digi--padding--largest);                            |
| `--digi--progress-indicator--gap--x-large`                         | var(--digi--padding--largest-3);                          |
| `--digi--progress-indicator--heading--font-size`                   | var(--digi--typography--heading-2--font-size--desktop);   |
| `--digi--progress-indicator--heading--font-weight`                 | var(--digi--typography--heading-2--font-weight--desktop); |
| `--digi--progress-indicator--heading--mobile--font-size`           | var(--digi--typography--heading-2--font-size--mobile);    |
| `--digi--progress-indicator--icon--color`                          | var(--digi--color--text--inverted);                       |
| `--digi--progress-indicator--icon--error--background-color`        | var(--digi--color--background--danger-2);                 |
| `--digi--progress-indicator--icon--error--color`                   | var(--digi--color--background--danger-1);                 |
| `--digi--progress-indicator--icon--height`                         | 1rem;                                                     |
| `--digi--progress-indicator--icon--warning--color`                 | var( --digi--color--background--warning-1);               |
| `--digi--progress-indicator--icon--width`                          | 1rem;                                                     |
| `--digi--progress-indicator--icon-status--height`                  | 1.75rem;                                                  |
| `--digi--progress-indicator--icon-status--width`                   | 1.75rem;                                                  |
| `--digi--progress-indicator--label--font-size`                     | var(--digi--typography--label--font-size--desktop);       |
| `--digi--progress-indicator--label--margin`                        | var(--digi--gutter--medium) 0;                            |
| `--digi--progress-indicator--margin`                               | 0 0 var(--digi--margin--medium);                          |
| `--digi--progress-indicator--step--background`                     | var(--digi--color--background--primary);                  |
| `--digi--progress-indicator--step--background--active`             | var(--digi--color--background--complementary-1);          |
| `--digi--progress-indicator--step--background--secondary--active`  | var(--digi--color--background--inverted-1);               |
| `--digi--progress-indicator--step--line--color`                    | var(--digi--color--border--neutral-5);                    |
| `--digi--progress-indicator--step--line--color--active`            | var(--digi--color--border--success);                      |
| `--digi--progress-indicator--step--line--color--current`           | var(--digi--color--border--neutral-5);                    |
| `--digi--progress-indicator--step--line--color--secondary--active` | var(--digi--color--border--secondary);                    |
| `--digi--progress-indicator--step--line--style`                    | dashed;                                                   |
| `--digi--progress-indicator--step--line--width`                    | var(--digi--border-width--secondary);                     |
| `--digi--progress-indicator--step--size`                           | var(--digi--global--spacing--base);                       |
| `--digi--progress-indicator--step--size--active`                   | 1.625rem;                                                 |
| `--digi--progress-indicator--step--size--active--large`            | 1.625rem;                                                 |
| `--digi--progress-indicator--step--size--large`                    | 1.625rem;                                                 |


## Dependencies

### Depends on

- [digi-util-breakpoint-observer](../../_util/util-breakpoint-observer)
- [digi-icon-check](../../_icon/icon-check)
- [digi-icon-exclamation-triangle-warning](../../_icon/icon-exclamation-triangle-warning)
- [digi-util-mutation-observer](../../_util/util-mutation-observer)

### Graph
```mermaid
graph TD;
  digi-progress-indicator --> digi-util-breakpoint-observer
  digi-progress-indicator --> digi-icon-check
  digi-progress-indicator --> digi-icon-exclamation-triangle-warning
  digi-progress-indicator --> digi-util-mutation-observer
  style digi-progress-indicator fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
