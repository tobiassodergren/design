import { newE2EPage } from '@stencil/core/testing';

describe('digi-progressbar', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-progressbar></digi-progressbar>');

    const element = await page.find('digi-progressbar');
    expect(element).toHaveClass('hydrated');
  });
});
