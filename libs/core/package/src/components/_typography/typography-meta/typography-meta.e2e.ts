import { newE2EPage } from '@stencil/core/testing';

describe('digi-typography-meta', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-typography-meta></digi-typography-meta>');

    const element = await page.find('digi-typography-meta');
    expect(element).toHaveClass('hydrated');
  });
});
