import { newE2EPage } from '@stencil/core/testing';

describe('digi-typography-preamble', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-typography-preamble></digi-typography-preamble>');

    const element = await page.find('digi-typography-preamble');
    expect(element).toHaveClass('hydrated');
  });
});
