# digi-util-keydown-handler

Detect keydown events on the most common keys, and also provides a catch-all `afOnKeyDown`-event.

<!-- Auto Generated Below -->


## Events

| Event          | Description                   | Type                         |
| -------------- | ----------------------------- | ---------------------------- |
| `afOnDown`     | Vid keydown på pil ner        | `CustomEvent<KeyboardEvent>` |
| `afOnEnd`      | Vid keydown på end            | `CustomEvent<KeyboardEvent>` |
| `afOnEnter`    | Vid keydown på enter          | `CustomEvent<KeyboardEvent>` |
| `afOnEsc`      | Vid keydown på escape         | `CustomEvent<KeyboardEvent>` |
| `afOnHome`     | Vid keydown på home           | `CustomEvent<KeyboardEvent>` |
| `afOnKeyDown`  | Vid keydown på alla tangenter | `CustomEvent<KeyboardEvent>` |
| `afOnLeft`     | Vid keydown på pil vänster    | `CustomEvent<KeyboardEvent>` |
| `afOnRight`    | Vid keydown på pil höger      | `CustomEvent<KeyboardEvent>` |
| `afOnShiftTab` | Vid keydown på skift och tabb | `CustomEvent<KeyboardEvent>` |
| `afOnSpace`    | Vid keydown på space          | `CustomEvent<KeyboardEvent>` |
| `afOnTab`      | Vid keydown på tab            | `CustomEvent<KeyboardEvent>` |
| `afOnUp`       | Vid keydown på pil upp        | `CustomEvent<KeyboardEvent>` |


## Slots

| Slot        | Description                 |
| ----------- | --------------------------- |
| `"default"` | Kan innehålla vad som helst |


## Dependencies

### Used by

 - [digi-calendar](../../_calendar/calendar)
 - [digi-form-filter](../../_form/form-filter)
 - [digi-navigation-context-menu](../../_navigation/navigation-context-menu)
 - [digi-navigation-sidebar](../../_navigation/navigation-sidebar)

### Graph
```mermaid
graph TD;
  digi-calendar --> digi-util-keydown-handler
  digi-form-filter --> digi-util-keydown-handler
  digi-navigation-context-menu --> digi-util-keydown-handler
  digi-navigation-sidebar --> digi-util-keydown-handler
  style digi-util-keydown-handler fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
