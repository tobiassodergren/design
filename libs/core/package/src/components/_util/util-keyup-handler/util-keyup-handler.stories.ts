import { enumSelect, Template } from '../../../../../../shared/utils/src';

export default {
	title: 'util/digi-util-keyup-handler',
	parameters: {
		actions: {
			handles: [
				'afOnKeyUp',
				'afOnEsc',
				'afOnEnter',
				'afOnTab',
				'afOnSpace',
				'afOnShiftTab',
				'afOnUp',
				'afOnDown',
				'afOnLeft',
				'afOnRight',
				'afOnHome',
				'afOnEnd'
			]
		}
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-util-keyup-handler',
	/* html */
	children: `
    <label>
        <digi-typography>
        Open the actions panel, focus the input, and press different keys
        </digi-typography>
        <input placeholder="Try me" style="display: block;" />
    </label>`
};
