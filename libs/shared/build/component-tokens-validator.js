/**
 * Detta är ett node skript som validerar komponent-tokens 
 * i Style Dictionary format mot en JSON Schema för att säkerställa att 
 * man har med alla variabler som behövs för en komponent
 * @en This is a node script that validates component-tokens 
 * in Style Dictionary format against a JSON Schema, to guarantee that 
 * all variables needed for the component is included
 */

'use strict';
(async () => {
  const Ajv = require('ajv')
  const glob = require('glob')
  const path = require('path')
  const fs = require('fs-extra')
  const process = require('process');

  const getFlags = () => {
    const args = process.argv.map((val) => val.replace(new RegExp('-', 'g'), ''))
    return args
  }
  const args = getFlags()
  
  const packageName = args.filter((val) => val.includes('packagename='))[0].replace('packagename=', '')
  const packagePath = `../../${packageName}`

  console.log(`----- Validate component token files for ${packageName} \n`);
  
  const schemasPath = path.resolve(
    __dirname,
    '../schemas/components/'
  )
  
  const componentsPath = path.resolve(
    __dirname,
    packagePath + '/design-tokens/src/components'
  )

  const themeFiles = glob.sync(componentsPath + '/**/*.json')

  const errors = [];

  themeFiles.forEach((file) => {
    const schemaName = path.basename(file).replace('.json', '.schema.json')
    const schemaPath = path.resolve(__dirname, schemasPath + `/${schemaName}`)

    if (!fs.existsSync(schemaPath)) return

    const schemaFile = require(schemaPath)
    const data = require(file)
  
    const ajv = new Ajv({ allErrors: true, verbose: true, async: true})
    const validate = ajv.compile(schemaFile)
    const valid = validate(data)
    
    !valid && errors.push(validate.errors)
  })

  if(errors.length > 0) {
    console.log('Validation of component tokens shows these errors: \n', errors)
    process.exit(1)
  }

  console.log('Validated all component tokens')

})()