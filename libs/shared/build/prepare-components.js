/**
 * Detta är ett node skript som kopierar över Core-komponenterna till respektive paket 
 * samt uppdaterar export-filerna för enums.
 * @en This is a node script that copies Core components to the other libraries 
 * and updates export files for enums
 */

'use strict';
(function () {
	const glob = require('glob');
	const fs = require('fs-extra');
	const path = require('path');
  const { readdir } = require('fs').promises;


	const getFlags = function () {
		const args = process.argv.map((val) => val.replace(new RegExp('-', 'g'), ''));
		return args;
	};
	const args = getFlags();

  const packageName = args.filter((val) => val.includes('packagename='))[0].replace('packagename=', '');
	const packagePath = `../../${packageName}/package/src`;

	console.log(`----- Prepare components for ${packageName} \n`);

  // Project component tokens
  const __localComponentTokens = path.resolve(
    __dirname,
    packagePath + '/design-tokens/components/'
  )

  // Project components
	const __localComponents = path.resolve(
		__dirname,
		packagePath + '/components'
	);

  // Path to local Core-components
  const __localCoreComponents = path.resolve(
		__dirname,
		packagePath + '/__core'
	);

  // Path to Core-components
	const __coreComponents = path.resolve(
		__dirname,
		'../../core/package/src/components'
	);

	const emptyDir = async (dirPath) => {
		if (fs.existsSync(dirPath)) {
			const dirContents = fs.readdirSync(dirPath); // List dir content

			for (const fileOrDirPath of dirContents) {
				try {
					// Get Full path
					const fullPath = path.join(dirPath, fileOrDirPath);
					const stat = fs.statSync(fullPath);

					if (stat.isDirectory()) {
						// It's a sub directory
						if (fs.readdirSync(fullPath).length) emptyDir(fullPath);
						// If the dir is not empty then remove it's contents too(recursively)
						fs.rmdirSync(fullPath);
					} else fs.unlinkSync(fullPath); // It's a file
				} catch (ex) {
					console.error(ex.message);
				}
			}
		}
	}

	const copyCore = async () => {
    try {
      await fs.copy(__coreComponents, __localCoreComponents, (err) => {
        if (err) {
          console.error(err);
        } else {
          console.log('2. Copied Core components\n');
          console.log('3. Generating enums\n')
          generateExports(__localCoreComponents, '__core', 'core');
          generateExports(__localComponents, 'components', packageName);
        }
      });
    } catch(err) {
      console.error(err);
    }
	}

  const generateExports = async (componentsPath, srcFolder, fileSuffix) => {
		glob.glob(`${componentsPath}/**/*.enum.ts`, {}, function (er, files) {
			console.log(`Exporting enums for ${fileSuffix} components\n`);
			const exportsString = files.map((file) => {
				const parsed = path.parse(file.split(`src/${srcFolder}`)[1]);
				return `export * from './${srcFolder}${path
					.join(parsed.dir, parsed.name)
					.replace(/\\/g, '/')}';`;
			});
			getFlags().includes('verbose') && console.log(exportsString);
			const file = fs.createWriteStream(
				packagePath + `/enums-${fileSuffix}.ts`
			);
			file.on('error', function (err) {
				console.error(err);
			});
			exportsString.forEach(function (v) {
				file.write(v + '\n');
			});
			file.end();
			console.log(
				`Enums successfully exported to enums-${fileSuffix}.ts\n`
			);
		});
	};

  const prepareCore = () => {
    console.log('1. Generating enums\n')
    generateExports(__localComponents, 'components', packageName);
  }

  const prepareTheme = async () => {
    await emptyDir(__localCoreComponents)
    console.log('1. Cleaned __core folder\n')

    await copyCore()
    console.log('2. Copied Core components\n')

  }

  if(packageName === 'core') {
    prepareCore()
  } else {
    prepareTheme()
  }
})();
