import { Directive, ElementRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

import { ValueAccessor } from './value-accessor';

@Directive({
  /* tslint:disable-next-line:directive-selector */
  selector: 'digi-form-input, digi-form-input[afType=text], digi-form-input[afType=email], digi-form-input[afType=color], digi-form-input[afType=date], digi-form-input[afType=datetime-local], digi-form-input[afType=month], digi-form-input[afType=password], digi-form-input[afType=search], digi-form-input[afType=tel], digi-form-input[afType=time], digi-form-input[afType=url], digi-form-input[afType=week], digi-form-textarea, digi-form-input-search, digi-calendar, digi-form-radiogroup',
  host: {
    '(afOnInput)': 'handleChangeEvent($event.target.value)',
    '(afOnDateSelectedChange)': 'handleChangeEvent($event.target.afSelectedDate)',
    '(afOnGroupChange)': 'handleChangeEvent($event.target.value)'
  },
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: TextValueAccessor,
      multi: true
    }
  ]
})
export class TextValueAccessor extends ValueAccessor {
  constructor(el: ElementRef) {
    super(el);
  }
}
