import '@storybook/addon-console';

import { setStencilDocJson } from '@pxtrn/storybook-addon-docs-stencil';
import docJson from '../../../../dist/libs/skolverket/docs.json';

// @ts-ignore
if (docJson) setStencilDocJson(docJson);

export const parameters = {
	controls: { hideNoControlsWarning: true }
};
