import { newE2EPage } from '@stencil/core/testing';

describe('digi-form-process-step', () => {
	it('renders', async () => {
		const page = await newE2EPage();
		await page.setContent('<digi-form-process-step></digi-form-process-step>');

		const element = await page.find('digi-form-process-step');
		expect(element).toHaveClass('hydrated');
	});
});
