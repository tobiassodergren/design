import { Component, h, Prop, Event, EventEmitter } from '@stencil/core';

/**
 * @slot default - Ska innehålla flera <form-process-step>
 *
 * @swedishName Processteg enskilt steg
 */
@Component({
	tag: 'digi-form-process-step',
	styleUrls: ['form-process-step.scss'],
	scoped: true
})
export class FormProcessStep {
	// Based on the step-by-step indicator here: https://www.w3.org/WAI/tutorials/forms/multi-page/

	// get cssModifiers() {
	// 	return {
	// 		[`digi-form-process-step--vertical-spacing-${this.afVerticalSpacing}`]:
	// 			!!this.afVerticalSpacing
	// 	};
	// }

	@Prop() afHref: string;

	@Prop({ attribute: null }) afType: 'current' | 'completed' | 'upcoming' =
		'upcoming';
	@Prop({ attribute: null }) afContext: 'regular' | 'fallback' = 'regular';
	@Prop() afLabel!: string;
	@Event() afClick: EventEmitter<MouseEvent>;

	clickHandler(e: MouseEvent) {
		this.afClick.emit(e);
	}

	get cssModifiers() {
		return {
			[`digi-form-process-step--type-${this.afType}`]: true,
			[`digi-form-process-step--context-${this.afContext}`]: true
		};
	}

	render() {
		return (
			<div class={{ 'digi-form-process-step': true, ...this.cssModifiers }}>
				{this.afType !== 'completed' ? (
					<p
						class="digi-form-process-step__control"
						aria-current={this.afType === 'current' ? 'step' : null}
						data-label={this.afLabel}
					>
						<span>{this.afLabel}</span>
					</p>
				) : this.afHref ? (
					<a
						class="digi-form-process-step__control"
						href={this.afHref}
						onClick={(e) => this.clickHandler(e)}
					>
						<span>{this.afLabel}</span>
					</a>
				) : (
					<button
						class="digi-form-process-step__control"
						type="button"
						onClick={(e) => this.clickHandler(e)}
					>
						<span>{this.afLabel}</span>
					</button>
				)}
			</div>
		);
	}
}
