import { Component, h } from '@stencil/core';

/**
 * @slot default - kan innehålla vad som helst
 *
 * @swedishName Rader
 */
@Component({
	tag: 'digi-layout-rows',
	styleUrls: ['layout-rows.scss'],
	scoped: true
})
export class LayoutRows {
	render() {
		return <slot></slot>;
	}
}
