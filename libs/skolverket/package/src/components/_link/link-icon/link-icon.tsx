import { Component, h } from '@stencil/core';

/**
 * @slot default - Ska vara en länk med en ikon och text inuti
 *
 * @swedishName Ikonlänk
 */
@Component({
	tag: 'digi-link-icon',
	styleUrls: ['link-icon.scss'],
	scoped: true
})
export class LinkIcon {
	render() {
		return (
			<span>
				<slot></slot>
			</span>
		);
	}
}
