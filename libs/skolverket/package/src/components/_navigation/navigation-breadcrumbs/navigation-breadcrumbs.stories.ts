import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { NavigationBreadcrumbsVariation } from './navigation-breadcrumbs-variation.enum';

export default {
	title: 'navigation/digi-navigation-breadcrumbs',
	argTypes: {
		'af-variation': enumSelect(NavigationBreadcrumbsVariation)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-navigation-breadcrumbs',
	'af-aria-label': 'breadcrumbs',
	'af-variation': NavigationBreadcrumbsVariation.REGULAR,
	'af-current-page': 'Current page is required',
	/* html */
	children: `
    <li><a href="/">Start</a></li>
    <li><a href="/contact-us">Sub-page 1</a></li>
    <li><a href="#">Sub-page 2</a></li>
		`
};
