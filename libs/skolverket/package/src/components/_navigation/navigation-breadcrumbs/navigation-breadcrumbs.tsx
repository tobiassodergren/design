import { _t } from '@digi/skolverket/text';
import { Component, Prop, h } from '@stencil/core';
import { NavigationBreadcrumbsVariation } from './navigation-breadcrumbs-variation.enum';

/**
 * @slot default - Ska inehålla li-element med a-element inuti.
 * @swedishName Brödsmulor
 */
@Component({
	tag: 'digi-navigation-breadcrumbs',
	styleUrls: ['navigation-breadcrumbs.scss'],
	scoped: true
})
export class NavigationBreadcrumbs {
	/**
	 * Sätter attributet 'aria-label' på navelementet.
	 * @en Set `aria-label` attribute on the nav element.
	 */
	@Prop() afAriaLabel = _t.breadcrumbs;

	/**
	 * Sätter texten för den aktiva sidan
	 * @en Set text for the active page
	 */
	@Prop() afCurrentPage!: string;

	/**
	 * Sätter texten för den aktiva sidan
	 * @en Set text for the active page
	 */
	@Prop() afVariation: NavigationBreadcrumbsVariation =
		NavigationBreadcrumbsVariation.REGULAR;

	get cssModifiers() {
		return {
			[`digi-navigation-breadcrumbs--variation-${this.afVariation}`]:
				!!this.afVariation
		};
	}

	render() {
		return (
			<nav
				class={{
					'digi-navigation-breadcrumbs': true,
					...this.cssModifiers
				}}
				aria-label={this.afAriaLabel}
			>
				<ol class="digi-navigation-breadcrumbs__items">
					<slot></slot>
					<li aria-current="page">{this.afCurrentPage}</li>
				</ol>
			</nav>
		);
	}
}
