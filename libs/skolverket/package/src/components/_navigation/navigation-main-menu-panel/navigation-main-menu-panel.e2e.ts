import { newE2EPage } from '@stencil/core/testing';

describe('digi-navigation-main-menu-panel', () => {
	it('renders', async () => {
		const page = await newE2EPage();
		await page.setContent(
			'<digi-navigation-main-menu-panel></digi-navigation-main-menu-panel>'
		);

		const element = await page.find('digi-navigation-main-menu-panel');
		expect(element).toHaveClass('hydrated');
	});
});
