import { enumSelect, Template } from '../../../../../../shared/utils/src';

export default {
	title: 'navigation/digi-navigation-pagination',
	parameters: {
		actions: {
			handles: ['afOnPageChange']
		}
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-navigation-pagination',
	'af-init-active-page': 1,
	'af-total-pages': 5,
	'af-current-result-start': 1,
	'af-id': 'pagination',
	'af-current-result-end': 3,
	'af-total-results': 50,
	'af-result-name': 'grejer'
};
