import { newE2EPage } from '@stencil/core/testing';

describe('digi-navigation-tab-in-a-box', () => {
	it('renders', async () => {
		const page = await newE2EPage();
		await page.setContent(
			'<digi-navigation-tab-in-a-box></digi-navigation-tab-in-a-box>'
		);

		const element = await page.find('digi-navigation-tab-in-a-box');
		expect(element).toHaveClass('hydrated');
	});
});
