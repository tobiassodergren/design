import { enumSelect, Template } from '../../../../../../shared/utils/src';

export default {
	title: 'navigation/digi-navigation-tab-in-a-box',
	parameters: {
		actions: {
			handles: ['afOnToggle']
		}
	}
};

export const Standard = () => /* html */ `
<digi-layout-container>
	<digi-navigation-tabs>
		<digi-navigation-tab-in-a-box af-aria-label="Översikt">
			<h2>Översikt</h2>
			<p>Jag accepterar samma props som digi-navigation-tab</p>
			<p>Om du struntar i af-id så kommer af-aria-label att "sluggifieras" och användas istället.</p>
		</digi-navigation-tab-in-a-box>
		<digi-navigation-tab-in-a-box af-aria-label="Pågående ärende">
			<h2>Pågående ärende</h2>
			<p>Lorem ipsum doler siit amet</p>
		</digi-navigation-tab-in-a-box>
		<digi-navigation-tab-in-a-box af-aria-label="Beslut">
			<h2>Beslut</h2>
			<p>Lorem ipsum doler siit amet</p>
		</digi-navigation-tab-in-a-box>
	</digi-navigation-tabs>
</digi-layout-container>
`;
