import { Component, Event, EventEmitter, Prop, Watch, h } from '@stencil/core';
import { CardBoxWidth } from '../../../enums-skolverket';
import { slugify } from '../../../global/utils/string';

/**
 * @slot default - Kan innehålla vad som helst
 * @swedishName Flik i en box
 */
@Component({
	tag: 'digi-navigation-tab-in-a-box',
	styleUrls: ['navigation-tab-in-a-box.scss'],
	scoped: true
})
export class NavigationTabInABox {
	/**
	 * Sätter attributet 'aria-label'
	 * @en Set aria-label attribute
	 */
	@Prop() afAriaLabel!: string;

	/**
	 * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
	 * @en Set id attribute. Defaults to random string.
	 */
	@Prop() afId: string;

	/**
	 * Sätter aktiv tabb. Detta sköts av digi-navigation-tab-in-a-boxs som ska omsluta denna komponent.
	 * @en Sets active tab (this is handled by digi-navigation-tab-in-a-boxs which should wrap this component)
	 */
	@Prop() afActive: boolean;

	/**
	 * När tabben växlar mellan aktiv och inaktiv
	 * @en When the tab toggles between active and inactive
	 */
	@Event() afOnToggle: EventEmitter<boolean>;

	@Watch('afActive')
	toggleHandler(activeTab: boolean) {
		if (activeTab || null) {
			this.afOnToggle.emit(activeTab);
		}
	}

	render() {
		return (
			<digi-navigation-tab
				afAriaLabel={this.afAriaLabel}
				afId={this.afId || slugify(this.afAriaLabel)}
				afActive={this.afActive}
				onAfOnToggle={(e) => this.afOnToggle.emit(e.detail)}
			>
				<digi-card-box afWidth={CardBoxWidth.FULL}>
					<digi-typography>
						<slot />
					</digi-typography>
				</digi-card-box>
			</digi-navigation-tab>
		);
	}
}
