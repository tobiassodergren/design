import { enumSelect, Template } from '../../../../../../shared/utils/src';

export default {
	title: 'navigation/digi-navigation-tabs',
	parameters: {
		actions: {
			handles: ['afOnChange', 'afOnClick', 'afOnFocus']
		}
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-navigation-tabs',
	'af-aria-label': '',
	'af-init-active-tab': 0,
	'af-id': null,
	/* html */
	children: `
    <digi-navigation-tab af-aria-label="Översikt" af-id="oversikt">
			<digi-card-box><p>I am the first tabpanel</p></digi-card-box>
		</digi-navigation-tab>
		<digi-navigation-tab af-aria-label="pågående ärende" af-id="pagaende-arende">
			<digi-card-box><p>I am the second tabpanel</p></digi-card-box>
		</digi-navigation-tab>
		<digi-navigation-tab af-aria-label="Beslut" af-id="beslut">
			<digi-card-box><p>I am the third tabpanel</p></digi-card-box>
    </digi-navigation-tab>`
};

export const InALayout = () => /* html */ `
	<digi-layout-container>
		<digi-navigation-tabs>
			<digi-navigation-tab af-aria-label="Översikt" af-id="oversikt">
				<digi-card-box af-width="full">
					<digi-typography>
						<h2>Översikt</h2>
						<p>Lorem ipsum doler siit amet</p>
					</digi-typography>
				</digi-card-box>
			</digi-navigation-tab>
			<digi-navigation-tab af-aria-label="Pågående ärende" af-id="pagaende-arende">
				<digi-card-box af-width="full">
					<digi-typography>
						<h2>Pågående ärende</h2>
						<p>Lorem ipsum doler siit amet</p>
					</digi-typography>
				</digi-card-box>
			</digi-navigation-tab>
			<digi-navigation-tab af-aria-label="Beslut" af-id="beslut">
				<digi-card-box af-width="full">
					<digi-typography>
						<h2>Beslut</h2>
						<p>Lorem ipsum doler siit amet</p>
					</digi-typography>
				</digi-card-box>
			</digi-navigation-tab>
		</digi-navigation-tabs>
	</digi-layout-container>
`;
