import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { NotificationAlertVariation } from './notification-alert-variation.enum';

export default {
	title: 'notification/digi-notification-alert',
	parameters: {
		actions: {
			handles: ['afOnClose']
		}
	},
	argTypes: {
		'af-variation': enumSelect(NotificationAlertVariation)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-notification-alert',
	'af-variation': NotificationAlertVariation.INFO,
	'af-closeable': false,
	/* html */
	children: `
		<h2 slot="heading">Driftstörning</h2>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In consequat tortor id ultricies convallis. Aenean ultrices tortor eget varius auctor. In ac est eu turpis porttitor rutrum.</p>
		<a href="#" slot="link">Läs mer om störningen</a> 
		`
};
