import { _t } from '@digi/skolverket/text';
import {
	Component,
	Event,
	EventEmitter,
	Prop,
	h,
	Host,
	State
} from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

/**
 * @slot default - Kan innehålla vad som helst
 * @slot link - Länk tiill exempel till läs-mer-om-cookies-sidan
 * @slot settings - Formulärselement för olika cookieinställningar
 *
 * @swedishName Cookiemeddelande
 */
@Component({
	tag: 'digi-notification-cookie',
	styleUrls: ['notification-cookie.scss'],
	scoped: true
})
export class NotificationCookie {
	@State() modalIsOpen = false;
	@Prop() afBannerText = _t.notification.cookie_banner_text;
	@Prop() afBannerHeadingText = _t.notification.cookie_banner_heading;
	@Prop() afModalHeadingText = _t.notification.cookie_modal_heading;
	@Prop() afRequiredCookiesText = _t.notification.cookie_modal_heading;

	/**
	 * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
	 * @en Component id attribute. Defaults to random string.
	 */
	@Prop() afId: string = randomIdGenerator('digi-notification-cookie');

	/**
	 * När användaren godkänner alla kakor
	 * @en When the user accepts all cookies
	 */
	@Event() afOnAcceptAllCookies: EventEmitter<MouseEvent>;

	/**
	 * När användaren godkänner alla kakor
	 * @en When the user accepts all cookies
	 */
	@Event() afOnSubmitSettings: EventEmitter;

	clickPrimaryButtonHandler() {
		this.afOnAcceptAllCookies.emit();
	}

	formSubmitHandler(e) {
		this.afOnSubmitSettings.emit(e);
	}

	openModal() {
		this.modalIsOpen = true;
	}

	closeModal() {
		this.modalIsOpen = false;
	}

	render() {
		return (
			<Host>
				<digi-notification-alert class="digi-notification-cookie__alert">
					<h2 slot="heading">{this.afBannerHeadingText}</h2>
					<div class="digi-notification-cookie__content">
						<p>{this.afBannerText}</p>
					</div>
					<div class="digi-notification-cookie__link" slot="link">
						<slot name="link"></slot>
					</div>
					<div class="digi-notification-cookie__actions" slot="actions">
						<digi-button
							afType="button"
							afFullWidth
							onAfOnClick={() => this.clickPrimaryButtonHandler()}
						>
							{_t.notification.cookie_accept_all}
						</digi-button>
						<digi-button
							afType="button"
							afFullWidth
							afVariation="secondary"
							onAfOnClick={() => this.openModal()}
						>
							{_t.settings}
						</digi-button>
					</div>
				</digi-notification-alert>
				<digi-dialog
					afOpen={this.modalIsOpen}
					onAfOnClose={() => this.closeModal()}
				>
					<h2 slot="heading">{this.afModalHeadingText}</h2>
					<p>{this.afRequiredCookiesText}</p>
					<form
						method="dialog"
						id={`${this.afId}-form`}
						onSubmit={(e) => this.formSubmitHandler(e)}
					>
						<digi-form-fieldset
							class="digi-notification-cookie__form-controls"
							afName="cookie-settings"
							afId={`${this.afId}-cookie-settings`}
						>
							<slot name="settings" />
						</digi-form-fieldset>
					</form>
					<digi-button
						afType="button"
						afVariation="secondary"
						onAfOnClick={() => this.closeModal()}
						slot="actions"
					>
						{_t.cancel}
					</digi-button>
					<digi-button afType="submit" afForm={`${this.afId}-form`} slot="actions">
						{_t.notification.cookie_save_and_approve}
					</digi-button>
				</digi-dialog>
			</Host>
		);
	}
}
