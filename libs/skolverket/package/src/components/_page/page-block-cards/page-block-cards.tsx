import { Component, h, Prop } from '@stencil/core';
import { CardBoxWidth } from '../../../enums-skolverket';
import { PageBlockCardsVariation } from './page-block-cards-variation.enum';

/**
 * @slot default
 * @slot heading - Rubrik
 *
 * @swedishName Sidblock med kort
 */
@Component({
	tag: 'digi-page-block-cards',
	styleUrls: ['page-block-cards.scss'],
	scoped: true,
	assetsDirs: ['public']
})
export class PageBlockCards {
	@Prop() afVariation: `${PageBlockCardsVariation}`;

	get cssModifiers() {
		return {
			[`digi-page-block-cards--variation-${this.afVariation}`]: !!this.afVariation
		};
	}

	render() {
		return (
			<digi-layout-container
				class={{ 'digi-page-block-cards': true, ...this.cssModifiers }}
			>
				<digi-card-box afWidth={CardBoxWidth.FULL}>
					<div class="digi-page-block-cards__inner">
						<digi-typography-heading-section>
							<slot name="heading"></slot>
						</digi-typography-heading-section>
						<digi-layout-stacked-blocks>
							<slot></slot>
						</digi-layout-stacked-blocks>
					</div>
				</digi-card-box>
			</digi-layout-container>
		);
	}
}
