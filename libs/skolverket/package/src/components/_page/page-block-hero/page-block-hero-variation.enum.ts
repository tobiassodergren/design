export enum PageBlockHeroVariation {
	START = 'start',
	SUB = 'sub',
	SECTION = 'section',
	PROCESS = 'process'
}
