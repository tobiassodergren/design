# digi-page-block-hero

<!-- Auto Generated Below -->


## Properties

| Property            | Attribute             | Description | Type                                         | Default     |
| ------------------- | --------------------- | ----------- | -------------------------------------------- | ----------- |
| `afBackground`      | `af-background`       |             | `"auto" \| "transparent"`                    | `undefined` |
| `afBackgroundImage` | `af-background-image` |             | `string`                                     | `undefined` |
| `afVariation`       | `af-variation`        |             | `"process" \| "section" \| "start" \| "sub"` | `undefined` |


## Slots

| Slot        | Description |
| ----------- | ----------- |
| `"default"` |             |
| `"heading"` | Rubrik      |


## Dependencies

### Depends on

- [digi-layout-container](../../../__core/_layout/layout-container)
- [digi-typography](../../../__core/_typography/typography)

### Graph
```mermaid
graph TD;
  digi-page-block-hero --> digi-layout-container
  digi-page-block-hero --> digi-typography
  style digi-page-block-hero fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
