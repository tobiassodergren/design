import { newE2EPage } from '@stencil/core/testing';

describe('digi-page-block-lists', () => {
	it('renders', async () => {
		const page = await newE2EPage();
		await page.setContent('<digi-page-block-lists></digi-page-block-lists>');

		const element = await page.find('digi-page-block-lists');
		expect(element).toHaveClass('hydrated');
	});
});
