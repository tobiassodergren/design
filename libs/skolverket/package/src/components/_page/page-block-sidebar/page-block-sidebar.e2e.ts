import { newE2EPage } from '@stencil/core/testing';

describe('digi-page-block-sidebar', () => {
	it('renders', async () => {
		const page = await newE2EPage();
		await page.setContent('<digi-page-block-sidebar></digi-page-block-sidebar>');

		const element = await page.find('digi-page-block-sidebar');
		expect(element).toHaveClass('hydrated');
	});
});
