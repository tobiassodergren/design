import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { PageBlockSidebarVariation } from './page-block-sidebar-variation.enum';

export default {
	title: 'page/digi-page-block-sidebar',
	argTypes: {
		'af-variation': enumSelect(PageBlockSidebarVariation)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-page-block-sidebar',
	'af-variation': 'start',
	/* html */
	children: `
    <digi-card-box slot="sidebar"><digi-typography>Sidebar</digi-typography></digi-card-box>
    <digi-card-box><digi-typography>Huvudyta</digi-typography></digi-card-box>
  `
};

export const WithMoreLayout = Template.bind({});
WithMoreLayout.args = {
	component: 'digi-page-block-sidebar',
	'af-variation': 'section',
	/* html */
	children: `
    <digi-layout-rows slot="sidebar">
      <digi-card-box>
        <digi-typography>Sidebar 1</digi-typography>
      </digi-card-box>
      <digi-card-box>
        <digi-typography>Sidebar 2</digi-typography>
      </digi-card-box>
      <digi-card-box>
        <digi-typography>Sidebar 3</digi-typography>
      </digi-card-box>
      <digi-card-box>
        <digi-typography>Sidebar 4</digi-typography>
      </digi-card-box>
    </digi-layout-rows>
    <digi-layout-rows>
      <digi-typography>
        <h1>Huvudyta</h1>
        <digi-typography-preamble>
        Curabitur blandit tempus porttitor. Maecenas sed diam eget risus varius blandit sit amet non magna. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.
        </digi-typography-preamble>
        <p>Aenean lacinia bibendum nulla sed consectetur. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
        <p>Donec ullamcorper nulla non metus auctor fringilla. Sed posuere consectetur est at lobortis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.</p>
      </digi-typography>
      <digi-typography>
      <digi-layout-rows>
        <h2>Vidare läsning</h2>
        <digi-layout-stacked-blocks>
          <digi-card-link>
            <img slot="image" src="https://www.skolverket.se/images/18.3770ea921807432e6c726e7/1656400441366/s%C3%A4kerhet-kris-bild-webb.png" width="392" height="220" style="width: 100%; height: auto; display: block;" />
            <h2 slot="link-heading"><a href="#">Beredskap vid ett förändrat omvärldsläge</a></h2>
            <p>Förskola och skola är samhällsviktig verksamhet som behöver fungera även vid olika typer av kriser. Här finns stöd för hur du kan förbereda din verksamhet vid olika händelser av kris.</p>
          </digi-card-link>
          <digi-card-link>
            <img slot="image" src="https://www.skolverket.se/images/18.3e89579d17f69780fbcceb/1648714549765/Mottagandet%20fr%C3%A5n%20Ukraina%20-puffbild.png" width="392" height="220" style="width: 100%; height: auto; display: block;" />
            <h2 slot="link-heading"><a href="#" slot="link-heading">Nyanlända barn och elever från Ukraina</a></h2>
            <p>Här hittar du som tar emot barn och elever från Ukraina information vad som gäller, det stöd vi erbjuder för mottagande och kartläggning och om skolsystemet och de olika skolformerna på engelska.</p>
          </digi-card-link>
        </digi-layout-stacked-blocks>
      </digi-layout-rows>
      </digi-typography>
    </digi-layout-rows>
    
  `
};
