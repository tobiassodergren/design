import { newE2EPage } from '@stencil/core/testing';

describe('digi-page-footer', () => {
	it('renders', async () => {
		const page = await newE2EPage();
		await page.setContent('<digi-page-footer></digi-page-footer>');

		const element = await page.find('digi-page-footer');
		expect(element).toHaveClass('hydrated');
	});
});
