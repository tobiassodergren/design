import { newE2EPage } from '@stencil/core/testing';

describe('digi-page', () => {
	it('renders', async () => {
		const page = await newE2EPage();
		await page.setContent('<digi-page></digi-page>');

		const element = await page.find('digi-page');
		expect(element).toHaveClass('hydrated');
	});
});
