# digi-typography-heading-section

<!-- Auto Generated Below -->


## Slots

| Slot        | Description                     |
| ----------- | ------------------------------- |
| `"default"` | ska innehålla ett rubrikelement |


## Dependencies

### Used by

 - [digi-page-block-cards](../../_page/page-block-cards)
 - [digi-page-block-lists](../../_page/page-block-lists)

### Graph
```mermaid
graph TD;
  digi-page-block-cards --> digi-typography-heading-section
  digi-page-block-lists --> digi-typography-heading-section
  style digi-typography-heading-section fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
