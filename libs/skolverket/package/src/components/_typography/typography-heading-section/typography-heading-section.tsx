import { Component, h } from '@stencil/core';

/**
 * @slot default - ska innehålla ett rubrikelement
 *
 * @swedishName Avsnittsrubrik
 */
@Component({
	tag: 'digi-typography-heading-section',
	styleUrls: ['typography-heading-section.scss'],
	scoped: true
})
export class TypographyHeadingSection {
	render() {
		return (
			<span>
				<slot></slot>
			</span>
		);
	}
}
