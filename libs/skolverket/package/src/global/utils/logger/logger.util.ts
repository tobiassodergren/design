import { LoggerType } from './LoggerType.enum';
import { LoggerSource } from './LoggerSource.enum';

const colors = {
	log: {
		bg: '#1616b2',
		text: '#fff'
	},
	info: {
		bg: '#058470',
		text: '#fff'
	},
	warn: {
		bg: '#fddc41',
		text: '#333'
	},
	error: {
		bg: '#e21c50',
		text: '#fff'
	}
};

export function outputLog(
	message: string,
	host: Element,
	type: LoggerType = LoggerType.LOG,
	source: LoggerSource = LoggerSource.CORE
) {
	console[type](
		`%c${source}: ${message || ''}`,
		`color: ${colors[type].text}; background-color: ${colors[type].bg};`,
		host || null
	);
}

export const logger = {
	log: (
		message: string,
		host: Element,
		source: LoggerSource = LoggerSource.CORE
	) => {
		outputLog(message, host, LoggerType.LOG, source);
	},
	info: (
		message: string,
		host: Element,
		source: LoggerSource = LoggerSource.CORE
	) => {
		outputLog(message, host, LoggerType.INFO, source);
	},
	warn: (
		message: string,
		host: Element,
		source: LoggerSource = LoggerSource.CORE
	) => {
		outputLog(message, host, LoggerType.WARN, source);
	},
	error: (
		message: string,
		host: Element,
		source: LoggerSource = LoggerSource.CORE
	) => {
		outputLog(message, host, LoggerType.ERROR, source);
	}
};
