import { render } from '@testing-library/react';

import SkolverketReact from './skolverket-react';

describe('SkolverketReact', () => {
	it('should render successfully', () => {
		const { baseElement } = render(<SkolverketReact />);
		expect(baseElement).toBeTruthy();
	});
});
